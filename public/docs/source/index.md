---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->



















# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost:8000/docs/collection.json)
<!-- END_INFO -->

#Calendar
Get the dates when order(s) was/were placed.
Get the positions opened and closed on dates
<!-- START_ebaee48743d7910a235fcf6d7bd6ce05 -->
## Get positions on dates
Get activity with positions on a particualar date.

If something was opened or closed.
Send `date` in `YYYY-MM-DD` format a.k.a UTC format

> Example request:

```bash
curl -X GET "http://localhost:8000/api/v1/calendar/positions" \
-H "Accept: application/json" \
    -d "date"="2018-04-19" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/v1/calendar/positions",
    "method": "GET",
    "data": {
        "date": "2018-04-19"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
"{\n \"success\": true,\n \"msg\": \"Retrieved positions\",\n \"data\" : [\n     \"openedPositions\": [],\n     \"closedPositions: []\n  ]\n}"
```

### HTTP Request
`GET api/v1/calendar/positions`

`HEAD api/v1/calendar/positions`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    date | date |  required  | Date format: `Y-m-d`

<!-- END_ebaee48743d7910a235fcf6d7bd6ce05 -->

#Close Position
Close a position on various exchanges
<!-- START_7a405a25a3dffc6c85e84f45e9596a34 -->
## Close a position

> Example request:

```bash
curl -X POST "http://localhost:8000/api/v1/position/close" \
-H "Accept: application/json" \
    -d "user_position_id"="98234" \
    -d "exit_size"="98234" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/v1/position/close",
    "method": "POST",
    "data": {
        "user_position_id": 98234,
        "exit_size": 98234
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/position/close`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    user_position_id | numeric |  required  | 
    exit_size | numeric |  optional  | 

<!-- END_7a405a25a3dffc6c85e84f45e9596a34 -->

#Dashboard
Get currently open positions, orders, position categories
Add position into category, add category and delete
<!-- START_7670fd929b91d8d6b59140396bc24e3d -->
## Get currently open positions
Send `category_name` as `all` or `null` to get all the positions,
`category_name` as `uncategorized` to get positions that are not in
any category or a category name to get positions of
that category

> Example request:

```bash
curl -X GET "http://localhost:8000/api/v1/dashboard/get-open-positions" \
-H "Accept: application/json" \
    -d "category_name"="et" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/v1/dashboard/get-open-positions",
    "method": "GET",
    "data": {
        "category_name": "et"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
"{\n{\n \"success\": true,\n \"msg\": \"Retrieved positions\",\n \"data\": [\n    {\n     \"user_position_id\": \"9\",\n     \"asset_type\": \"crypto\",\n     \"symbol\": \"ETHBTC\",\n     \"quantity\": \"0.01\",\n     \"entry\": \"0\",\n     \"price_currency\": \"ETH\",\n     \"side\": null,\n     \"exchange_name\": \"binance\"\n    }\n ]\n}\n}"
```

### HTTP Request
`GET api/v1/dashboard/get-open-positions`

`HEAD api/v1/dashboard/get-open-positions`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    category_name | string |  optional  | 

<!-- END_7670fd929b91d8d6b59140396bc24e3d -->

<!-- START_fda9ba0a4ea0fbcf3f83c62d8af97c16 -->
## Get position categories names

> Example request:

```bash
curl -X GET "http://localhost:8000/api/v1/dashboard/get-categories-names" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/v1/dashboard/get-categories-names",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "success": true,
    "msg": "Retrieved categories names",
    "data": [
        {
            "name": "Another One",
            "id": 2
        },
        {
            "name": "New Category Here",
            "id": 1
        }
    ]
}
```

### HTTP Request
`GET api/v1/dashboard/get-categories-names`

`HEAD api/v1/dashboard/get-categories-names`


<!-- END_fda9ba0a4ea0fbcf3f83c62d8af97c16 -->

<!-- START_ae919e9afa1b734f3f2284722e6366eb -->
## Create a position category

> Example request:

```bash
curl -X POST "http://localhost:8000/api/v1/dashboard/create-position-category" \
-H "Accept: application/json" \
    -d "name"="pariatur" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/v1/dashboard/create-position-category",
    "method": "POST",
    "data": {
        "name": "pariatur"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/dashboard/create-position-category`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  required  | Maximum: `50`

<!-- END_ae919e9afa1b734f3f2284722e6366eb -->

<!-- START_0d9ea1a90c683d5882929a5259b63233 -->
## Edit position category name

> Example request:

```bash
curl -X POST "http://localhost:8000/api/v1/dashboard/edit-position-category" \
-H "Accept: application/json" \
    -d "position_category_id"="4653" \
    -d "name"="qui" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/v1/dashboard/edit-position-category",
    "method": "POST",
    "data": {
        "position_category_id": 4653,
        "name": "qui"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/dashboard/edit-position-category`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    position_category_id | numeric |  required  | 
    name | string |  required  | Maximum: `50`

<!-- END_0d9ea1a90c683d5882929a5259b63233 -->

<!-- START_e1897c3e16417aed415cdb823943ef31 -->
## Delete a position category name

> Example request:

```bash
curl -X POST "http://localhost:8000/api/v1/dashboard/delete-position-category" \
-H "Accept: application/json" \
    -d "position_category_id"="9068" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/v1/dashboard/delete-position-category",
    "method": "POST",
    "data": {
        "position_category_id": 9068
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/dashboard/delete-position-category`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    position_category_id | numeric |  required  | 

<!-- END_e1897c3e16417aed415cdb823943ef31 -->

<!-- START_5f965b916f43c227704824294cd34298 -->
## Add a position into a category

> Example request:

```bash
curl -X POST "http://localhost:8000/api/v1/dashboard/add-position-into-category" \
-H "Accept: application/json" \
    -d "position_id"="286155" \
    -d "position_category_id"="286155" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/v1/dashboard/add-position-into-category",
    "method": "POST",
    "data": {
        "position_id": 286155,
        "position_category_id": 286155
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/dashboard/add-position-into-category`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    position_id | numeric |  required  | 
    position_category_id | numeric |  required  | 

<!-- END_5f965b916f43c227704824294cd34298 -->

#Exchange
Connect to an exchange, disconnect,
get list of connected exchanges for a user,
get currently supported exchanges by RemoraHub
<!-- START_41bab41edb865561670ea56990225168 -->
## Connect to an exchange
`extra_1 param` is supplied with `uid` when exchange is Bitstamp
and with `password` when exchange is Gdax.

When editing or revalidating a conn, send `revalidate` as `1`
and send all the params sent as usual.

> Example request:

```bash
curl -X POST "http://localhost:8000/api/v1/exchange/connect" \
-H "Accept: application/json" \
    -d "exchange_name"="ipsam" \
    -d "api_key"="ipsam" \
    -d "api_secret"="ipsam" \
    -d "extra_1"="ipsam" \
    -d "revalidate"="1" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/v1/exchange/connect",
    "method": "POST",
    "data": {
        "exchange_name": "ipsam",
        "api_key": "ipsam",
        "api_secret": "ipsam",
        "extra_1": "ipsam",
        "revalidate": true
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/exchange/connect`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    exchange_name | string |  required  | 
    api_key | string |  required  | 
    api_secret | string |  required  | 
    extra_1 | string |  optional  | Required if `exchange_name` is `bitstamp` Required if `exchange_name` is `gdax`
    revalidate | boolean |  optional  | 

<!-- END_41bab41edb865561670ea56990225168 -->

<!-- START_19759bf601b4a6ed32588c3cd0e4037d -->
## Connected exchanges
Exchanges where the user is currently connected to using Sharpremora

> Example request:

```bash
curl -X GET "http://localhost:8000/api/v1/exchange/connected" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/v1/exchange/connected",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "success": true,
    "msg": "Retrieved exchanges",
    "data": [
        {
            "name": "binance",
            "id": 7,
            "exchange_supported_order_types": null
        },
        {
            "name": "bitmex",
            "id": 17,
            "exchange_supported_order_types": null
        },
        {
            "name": "bittrex",
            "id": 21,
            "exchange_supported_order_types": null
        },
        {
            "name": "gdax",
            "id": 59,
            "exchange_supported_order_types": null
        },
        {
            "name": "kucoin",
            "id": 71,
            "exchange_supported_order_types": null
        }
    ]
}
```

### HTTP Request
`GET api/v1/exchange/connected`

`HEAD api/v1/exchange/connected`


<!-- END_19759bf601b4a6ed32588c3cd0e4037d -->

<!-- START_23195ed3244005528fa6c72d78ff0d64 -->
## Disconnect an exchange

> Example request:

```bash
curl -X POST "http://localhost:8000/api/v1/exchange/disconnect" \
-H "Accept: application/json" \
    -d "exchange_id"="sint" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/v1/exchange/disconnect",
    "method": "POST",
    "data": {
        "exchange_id": "sint"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/exchange/disconnect`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    exchange_id | string |  required  | 

<!-- END_23195ed3244005528fa6c72d78ff0d64 -->

<!-- START_2c4902855a73375d68eaee3aa0b36658 -->
## Supported exchanges
The list of exchanges that are currently supported by Sharpremora

> Example request:

```bash
curl -X GET "http://localhost:8000/api/v1/exchange/supported" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/v1/exchange/supported",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "success": true,
    "msg": "Retrieved supported exchanges",
    "data": [
        {
            "id": 7,
            "name": "binance"
        },
        {
            "id": 11,
            "name": "bitfinex"
        },
        {
            "id": 17,
            "name": "bitmex"
        },
        {
            "id": 21,
            "name": "bittrex"
        },
        {
            "id": 59,
            "name": "gdax"
        },
        {
            "id": 71,
            "name": "kucoin"
        },
        {
            "id": 19,
            "name": "bitstamp"
        },
        {
            "id": 50,
            "name": "cryptopia"
        }
    ]
}
```

### HTTP Request
`GET api/v1/exchange/supported`

`HEAD api/v1/exchange/supported`


<!-- END_2c4902855a73375d68eaee3aa0b36658 -->

<!-- START_d1bc25367049863fdd33ea8436f6ea63 -->
## Get all the markets from an exchange

> Example request:

```bash
curl -X GET "http://localhost:8000/api/v1/exchange/markets" \
-H "Accept: application/json" \
    -d "exchange_name"="nihil" \
    -d "market_name"="nihil" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/v1/exchange/markets",
    "method": "GET",
    "data": {
        "exchange_name": "nihil",
        "market_name": "nihil"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "success": false,
    "msg": "Validation Error",
    "errors": {
        "exchange_name": [
            "The exchange name field is required."
        ]
    }
}
```

### HTTP Request
`GET api/v1/exchange/markets`

`HEAD api/v1/exchange/markets`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    exchange_name | string |  required  | 
    market_name | string |  optional  | 

<!-- END_d1bc25367049863fdd33ea8436f6ea63 -->

<!-- START_e7c9cd6260053b149d22623d6e85dbf8 -->
## Exchange capabilities
Returns what types of orders supported by an exchange

> Example request:

```bash
curl -X GET "http://localhost:8000/api/v1/exchange/capabilities" \
-H "Accept: application/json" \
    -d "exchange_name"="iusto" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/v1/exchange/capabilities",
    "method": "GET",
    "data": {
        "exchange_name": "iusto"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "success": false,
    "msg": "Validation Error",
    "errors": {
        "exchange_name": [
            "The exchange name field is required."
        ]
    }
}
```

### HTTP Request
`GET api/v1/exchange/capabilities`

`HEAD api/v1/exchange/capabilities`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    exchange_name | string |  required  | 

<!-- END_e7c9cd6260053b149d22623d6e85dbf8 -->

<!-- START_8afd8b3de2a0ed8ff30b63ca9c57e7ae -->
## Get balance.

Send `asset` as an array.
E.g send `asset` as `['btc', 'eth']` to get balance for BTC and ETH for that exchange.
If you don't specify any asset, it will send balance for all the assets of that
exchange.

> Example request:

```bash
curl -X GET "http://localhost:8000/api/v1/exchange/get-balance" \
-H "Accept: application/json" \
    -d "exchange_name"="dicta" \
    -d "asset"="dicta" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/v1/exchange/get-balance",
    "method": "GET",
    "data": {
        "exchange_name": "dicta",
        "asset": "dicta"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
"{\n \"success\": true,\n \"msg\": \"Fetched Balance\",\n \"data\" : [\n     {\"asset\": \"BTC\", \"free\": \"0.1\", \"used\" : \"0.02\", \"total\": \"0.12\"},\n     {\"asset\": \"ETH\", \"free\": \"0.1\", \"used\" : \"0.02\", \"total\": \"0.12\"}\n ]\n}"
```

### HTTP Request
`GET api/v1/exchange/get-balance`

`HEAD api/v1/exchange/get-balance`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    exchange_name | string |  required  | 
    asset | array |  optional  | 

<!-- END_8afd8b3de2a0ed8ff30b63ca9c57e7ae -->

#Fetch Orders
Fetch Open orders on various exchanges, fetch open orders
of various exchanges from our database
<!-- START_8e318220348980e0021a4449bf1a282e -->
## Fetch open orders
Fetch open orders on various exchanges
and insert that into our database

> Example request:

```bash
curl -X POST "http://localhost:8000/api/v1/exchange/fetch-open-orders" \
-H "Accept: application/json" \
    -d "exchange_name"="enim" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/v1/exchange/fetch-open-orders",
    "method": "POST",
    "data": {
        "exchange_name": "enim"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/exchange/fetch-open-orders`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    exchange_name | string |  required  | 

<!-- END_8e318220348980e0021a4449bf1a282e -->

<!-- START_985d87fa04a157f2d8b59ef306bf6f06 -->
## Get orders
Send `status` as `all` to get orders of any status.

Get `open`, `cancelled`, `filled` or
all kinds of orders on all exchange or a
particular exchange from our database.
To filter orders in ascending or descending order
of order time, send `order_time_flow` as `asc` or `desc`.
Send `from_date` and `to_date` in `YYYY-MM-DD` format from front end
a.k.a `UTC` format

> Example request:

```bash
curl -X GET "http://localhost:8000/api/v1/orders" \
-H "Accept: application/json" \
    -d "order_status"="open" \
    -d "exchange_name"="doloremque" \
    -d "order_time_flow"="asc" \
    -d "page"="6" \
    -d "from_date"="2018-04-19" \
    -d "to_date"="2018-04-19" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/v1/orders",
    "method": "GET",
    "data": {
        "order_status": "open",
        "exchange_name": "doloremque",
        "order_time_flow": "asc",
        "page": 6,
        "from_date": "2018-04-19",
        "to_date": "2018-04-19"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "success": true,
    "msg": "Retrieved orders",
    "data": [],
    "pageCount": 0
}
```

### HTTP Request
`GET api/v1/orders`

`HEAD api/v1/orders`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    order_status | string |  optional  | `open`, `cancelled` or `filled`
    exchange_name | string |  optional  | 
    order_time_flow | string |  optional  | `asc` or `desc`
    page | numeric |  optional  | 
    from_date | date |  optional  | Date format: `Y-m-d`
    to_date | date |  optional  | Date format: `Y-m-d`

<!-- END_985d87fa04a157f2d8b59ef306bf6f06 -->

<!-- START_c18cd6b2940b4dfa59efebdc0dae7c2e -->
## Cancel an order
Cancel an order on any exchange

> Example request:

```bash
curl -X POST "http://localhost:8000/api/v1/orders/cancel-order" \
-H "Accept: application/json" \
    -d "order_id"="12993" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/v1/orders/cancel-order",
    "method": "POST",
    "data": {
        "order_id": 12993
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/orders/cancel-order`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    order_id | numeric |  required  | 

<!-- END_c18cd6b2940b4dfa59efebdc0dae7c2e -->

<!-- START_a5a6190500e6199d7b0d1f8faddd014f -->
## Fetch orders dates list
Dates list use for calendar page
to highlight the date on which user place orders

> Example request:

```bash
curl -X GET "http://localhost:8000/api/v1/orders/calendar-dates-list" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/v1/orders/calendar-dates-list",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "success": true,
    "msg": "Retrieved orders dates list",
    "data": [
        {
            "date": "2018-04-11"
        },
        {
            "date": "2018-04-11"
        },
        {
            "date": "2018-04-11"
        },
        {
            "date": "2018-04-11"
        },
        {
            "date": "2018-04-11"
        },
        {
            "date": "2018-04-11"
        },
        {
            "date": "2018-04-11"
        },
        {
            "date": "2018-04-11"
        },
        {
            "date": "2018-04-11"
        },
        {
            "date": "2018-04-16"
        },
        {
            "date": "2018-04-16"
        },
        {
            "date": "2018-04-16"
        },
        {
            "date": "2018-04-17"
        },
        {
            "date": "2018-04-19"
        },
        {
            "date": "2018-04-19"
        },
        {
            "date": "2018-04-19"
        },
        {
            "date": "2018-04-19"
        },
        {
            "date": "2018-04-19"
        },
        {
            "date": "2018-04-19"
        },
        {
            "date": "2018-04-19"
        },
        {
            "date": "2018-04-19"
        },
        {
            "date": "2018-04-19"
        },
        {
            "date": "2018-04-19"
        },
        {
            "date": "2018-04-19"
        },
        {
            "date": "2018-04-19"
        },
        {
            "date": "2018-04-19"
        },
        {
            "date": "2018-04-19"
        }
    ]
}
```

### HTTP Request
`GET api/v1/orders/calendar-dates-list`

`HEAD api/v1/orders/calendar-dates-list`


<!-- END_a5a6190500e6199d7b0d1f8faddd014f -->

#Open Position
Open a position on various exchanges
<!-- START_9ea57a6c96f4fb7117b4454d0be6a3ef -->
## Open a position
Opening a position means creating or placing an order

> Example request:

```bash
curl -X POST "http://localhost:8000/api/v1/position/open" \
-H "Accept: application/json" \
    -d "exchange_name"="qui" \
    -d "symbol"="qui" \
    -d "type"="take_profit_limit" \
    -d "side"="Buy" \
    -d "quantity"="589314" \
    -d "price"="589314" \
    -d "stop_price"="qui" \
    -d "time_in_force"="qui" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/v1/position/open",
    "method": "POST",
    "data": {
        "exchange_name": "qui",
        "symbol": "qui",
        "type": "take_profit_limit",
        "side": "Buy",
        "quantity": 589314,
        "price": 589314,
        "stop_price": "qui",
        "time_in_force": "qui"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/position/open`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    exchange_name | string |  required  | 
    symbol | string |  required  | 
    type | string |  required  | `stop_loss`, `limit`, `market`, `stop_loss_limit`, `take_profit`, `take_profit_limit`, `limit_maker`, `stop_entry`, `market_limit` or `stop_market`
    side | string |  required  | `buy`, `sell`, `Buy` or `Sell`
    quantity | numeric |  required  | 
    price | numeric |  optional  | Required unless `type` is `market`
    stop_price | string |  optional  | 
    time_in_force | string |  optional  | 

<!-- END_9ea57a6c96f4fb7117b4454d0be6a3ef -->

<!-- START_2fe3a712cc1ec50a5966b66f7739732e -->
## api/v1/position/openpositions

> Example request:

```bash
curl -X GET "http://localhost:8000/api/v1/position/openpositions" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/v1/position/openpositions",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "success": true,
    "msg": [
        {
            "symbol": "BTCUSDT",
            "base": "BTC",
            "size": "0.02676328",
            "exchange": "binance"
        },
        {
            "symbol": "LTCBTC",
            "base": "LTC",
            "size": "0.00883332",
            "exchange": "binance"
        },
        {
            "symbol": "ETHBTC",
            "base": "ETH",
            "size": "0.04068856",
            "exchange": "binance"
        },
        {
            "symbol": "EOSBTC",
            "base": "EOS",
            "size": "0.21900000",
            "exchange": "binance"
        },
        {
            "symbol": "WTCBTC",
            "base": "WTC",
            "size": "0.01998000",
            "exchange": "binance"
        },
        {
            "symbol": "XVGBTC",
            "base": "XVG",
            "size": "0.92000000",
            "exchange": "binance"
        },
        {
            "symbol": "MTLBTC",
            "base": "MTL",
            "size": "0.00015000",
            "exchange": "binance"
        },
        {
            "symbol": "NULSBTC",
            "base": "NULS",
            "size": "0.10000000",
            "exchange": "binance"
        },
        {
            "symbol": "ASTBTC",
            "base": "AST",
            "size": "0.67600000",
            "exchange": "binance"
        },
        {
            "symbol": "POWRBTC",
            "base": "POWR",
            "size": "0.52500000",
            "exchange": "binance"
        },
        {
            "symbol": "BTGBTC",
            "base": "BTG",
            "size": "0.00826000",
            "exchange": "binance"
        },
        {
            "symbol": "REQBTC",
            "base": "REQ",
            "size": "0.99900000",
            "exchange": "binance"
        },
        {
            "symbol": "XMRBTC",
            "base": "XMR",
            "size": "0.00095900",
            "exchange": "binance"
        },
        {
            "symbol": "XRPBTC",
            "base": "XRP",
            "size": "0.21100000",
            "exchange": "binance"
        },
        {
            "symbol": "MANABTC",
            "base": "MANA",
            "size": "0.76200000",
            "exchange": "binance"
        },
        {
            "symbol": "GVTBTC",
            "base": "GVT",
            "size": "0.00527000",
            "exchange": "binance"
        },
        {
            "symbol": "BCDBTC",
            "base": "BCD",
            "size": "0.50036920",
            "exchange": "binance"
        },
        {
            "symbol": "ADABTC",
            "base": "ADA",
            "size": "0.80800000",
            "exchange": "binance"
        },
        {
            "symbol": "XLMBTC",
            "base": "XLM",
            "size": "0.30000000",
            "exchange": "binance"
        },
        {
            "symbol": "LUNBTC",
            "base": "LUN",
            "size": "0.00410000",
            "exchange": "binance"
        },
        {
            "symbol": "RPXBTC",
            "base": "RPX",
            "size": "2597.40000000",
            "exchange": "binance"
        },
        {
            "symbol": "XEMBTC",
            "base": "XEM",
            "size": "2282.71500000",
            "exchange": "binance"
        },
        {
            "symbol": "BTC-ABY",
            "base": "ABY",
            "size": 800,
            "exchange": "bittrex"
        },
        {
            "symbol": "BTC-ADA",
            "base": "ADA",
            "size": 25,
            "exchange": "bittrex"
        },
        {
            "symbol": "BTC-ADT",
            "base": "ADT",
            "size": 3281.92534861,
            "exchange": "bittrex"
        },
        {
            "symbol": "USDT-BTC",
            "base": "BTC",
            "size": 0.00750125,
            "exchange": "bittrex"
        },
        {
            "symbol": "BTC-FLDC",
            "base": "FLDC",
            "size": 20961.3449312,
            "exchange": "bittrex"
        },
        {
            "symbol": "BTC-FTC",
            "base": "FTC",
            "size": 4016.38046354,
            "exchange": "bittrex"
        },
        {
            "symbol": "BTC-HMQ",
            "base": "HMQ",
            "size": 904.89369098,
            "exchange": "bittrex"
        },
        {
            "symbol": "BTC-LGD",
            "base": "LGD",
            "size": 1039.18152461,
            "exchange": "bittrex"
        },
        {
            "symbol": "BTC-RDD",
            "base": "RDD",
            "size": 85006.9222,
            "exchange": "bittrex"
        },
        {
            "symbol": "BTC-SC",
            "base": "SC",
            "size": 17377.74237745,
            "exchange": "bittrex"
        },
        {
            "symbol": "BTC-XVG",
            "base": "XVG",
            "size": 13110.65502738,
            "exchange": "bittrex"
        }
    ]
}
```

### HTTP Request
`GET api/v1/position/openpositions`

`HEAD api/v1/position/openpositions`


<!-- END_2fe3a712cc1ec50a5966b66f7739732e -->

#general
<!-- START_957f36a05e68536731995f18794d7502 -->
## Get details about the current authenticated user

> Example request:

```bash
curl -X GET "http://localhost:8000/api/v1/user/me" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/v1/user/me",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "success": false,
    "msg": "Trying to get property 'id' of non-object"
}
```

### HTTP Request
`GET api/v1/user/me`

`HEAD api/v1/user/me`


<!-- END_957f36a05e68536731995f18794d7502 -->

<!-- START_4cbfc10e6683f1ccf3cd3e9c465eb214 -->
## Update the current authenticated user

> Example request:

```bash
curl -X POST "http://localhost:8000/api/v1/user/update/profile" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/v1/user/update/profile",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/user/update/profile`


<!-- END_4cbfc10e6683f1ccf3cd3e9c465eb214 -->

<!-- START_bd940c6839871748c76df287c47bd34f -->
## Update user&#039;s password

> Example request:

```bash
curl -X POST "http://localhost:8000/api/v1/user/update/password" \
-H "Accept: application/json" \
    -d "old_password"="neque" \
    -d "new_password"="neque" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/v1/user/update/password",
    "method": "POST",
    "data": {
        "old_password": "neque",
        "new_password": "neque"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/user/update/password`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    old_password | string |  required  | 
    new_password | string |  required  | 

<!-- END_bd940c6839871748c76df287c47bd34f -->

<!-- START_464b40b0317ac3e6c0e591f9d2fe1a23 -->
## Get symbols
Used for autocomplete

> Example request:

```bash
curl -X GET "http://localhost:8000/api/v1/symbols" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/v1/symbols",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "success": false,
    "msg": "Validation Error",
    "errors": {
        "symbol": [
            "The symbol field is required."
        ]
    }
}
```

### HTTP Request
`GET api/v1/symbols`

`HEAD api/v1/symbols`


<!-- END_464b40b0317ac3e6c0e591f9d2fe1a23 -->

