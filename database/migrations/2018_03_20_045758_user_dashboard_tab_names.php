<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserDashboardTabNames extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_dashboard_tab_names', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('tab_name');
            $table->timestamps();
        });

        Schema::table('positions', function (Blueprint $table) {
            $table->unsignedInteger('user_dashboard_tab')->nullable();
            $table->foreign('user_dashboard_tab')->references('id')->on('user_dashboard_tab_names')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_dashboard_tab_names');

        Schema::table('positions', function (Blueprint $table) {
            $table->dropColumn(['user_dashboard_tab']);
        });
    }
}
