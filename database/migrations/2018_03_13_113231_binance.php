<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Binance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('symbols', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('exchange_id');
            $table->foreign('exchange_id')->references('id')->on('supported_exchanges')->onDelete('cascade');
            $table->string('symbol');
            $table->string('base')->nullable();
            $table->string('quote')->nullable();
            $table->boolean('limit_order')->default(false);
            $table->boolean('market_order')->default(false);
            $table->boolean('stop_loss_order')->default(false);
            $table->boolean('stop_loss_limit_order')->default(false);
            $table->boolean('take_profit_order')->default(false);
            $table->boolean('take_profit_limit_order')->default(false);
            $table->boolean('limit_maker_order')->default(false);
            $table->boolean('iceberg_allowed')->nullable();
            $table->float('min_price', 8, 10)->nullable();
            $table->float('max_price', 8, 10)->nullable();
            $table->float('tick_size', 8, 10)->nullable();
            $table->float('min_qty', 8, 10)->nullable();
            $table->float('max_qty', 8, 10)->nullable();
            $table->string('type')->default('crypto');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('symbols');
    }
}
