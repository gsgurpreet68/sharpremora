<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PasswordIntoExchanges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('exchange_conn', 'api_password')) {
            Schema::table('exchange_conn', function (Blueprint $table) {
                $table->string('api_password')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('exchange_conn', 'api_password')) {
            Schema::table('exchange_conn', function (Blueprint $table) {
                $table->dropColumn(['api_password']);
            });
        }
    }
}
