<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExchangeSupportedOrderTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('supported_exchanges', function (Blueprint $table) {
            $table->string('exchange_supported_order_types')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('supported_exchanges', function (Blueprint $table) {
           $table->dropColumn(['exchange_supported_order_types']);
        });
    }
}
