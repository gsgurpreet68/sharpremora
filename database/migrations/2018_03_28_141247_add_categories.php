<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Schema::dropIfExists('user_dashboard_tab_names'); // doesn't work because no way to define cascade
        DB::statement("DROP TABLE user_dashboard_tab_names CASCADE ");

        if (Schema::hasColumn('user_positions', 'user_dashboard_tab_name_id')) {
            Schema::table('user_positions', function (Blueprint $table) {
                $table->dropColumn(['user_dashboard_tab_name_id']);
            });
        }

        Schema::create('user_position_categories_names', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('name');
            $table->softDeletes();
            $table->timestamps();
        });
        Schema::create('user_position_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedInteger('user_positions_id');
            $table->foreign('user_positions_id')->references('id')->on('user_positions')->onDelete('cascade');
            $table->unsignedInteger('user_position_categories_names_id');
            $table->foreign('user_position_categories_names_id')->references('id')->on('user_position_categories_names')->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_position_categories_names');
        Schema::dropIfExists('user_position_categories');
    }
}
