<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangesToOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('positions', 'user_orders');

        Schema::create('user_positions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('exchange_id');
            $table->foreign('exchange_id')->references('id')->on('supported_exchanges')->onDelete('cascade');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedInteger('user_dashboard_tab_name_id')->nullable();
            $table->foreign('user_dashboard_tab_name_id')->references('id')->on('user_dashboard_tab_names')->onDelete('cascade');
            $table->enum('asset_type', ['crypto', 'stock', 'futures', 'forex', 'derivatives']);
            $table->string('symbol');
            $table->float('quantity', 8, 8);
            $table->float('price', 8, 8);
            $table->string('price_currency');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('user_orders', 'positions');
        Schema::dropIfExists('user_positions');
    }
}
