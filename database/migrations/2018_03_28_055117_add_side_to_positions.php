<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSideToPositions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_positions', function (Blueprint $table) {
           $table->enum('side', ['buy', 'sell', 'Buy', 'Sell'])->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_positions', function (Blueprint $table) {
           $table->dropColumn(['side']);
        });
    }
}
