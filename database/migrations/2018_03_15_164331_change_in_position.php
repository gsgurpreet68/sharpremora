<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeInPosition extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('positions', function (Blueprint $table) {
            $table->string('symbol')->nullable();
            $table->string('status')->nullable();
            $table->string('side')->nullable();
            $table->string('type')->nullable();
            $table->float('quantity', 8, 10)->nullable();
            $table->float('price', 8, 10)->nullable();
            $table->dropColumn(['success']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('positions', function (Blueprint $table) {
            $table->boolean('success')->default(false);
            $table->dropColumn(['symbol', 'status']);
        });
    }
}
