<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropColumnsSymbols extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('symbols');
        Schema::create('symbols', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('exchange_id');
            $table->foreign('exchange_id')->references('id')->on('supported_exchanges')->onDelete('cascade');
            $table->string('symbol');
            $table->string('base')->nullable();
            $table->string('quote')->nullable();
            $table->string('type')->default('crypto');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('symbols');
    }
}
