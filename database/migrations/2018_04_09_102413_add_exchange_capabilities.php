<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExchangeCapabilities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exchange_capabilities', function (Blueprint $table) {
            $table->mediumIncrements('id');
            $table->string('exchange_name');
            $table->boolean('limit_order')->default(true);
            $table->boolean('market_order')->default(false);
            $table->boolean('stop_loss_order')->default(false);
            $table->boolean('take_profit_order')->default(false);
            $table->boolean('time_in_force')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exchange_capabilities');
    }
}
