<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddPostionIdTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('user_positions', function (Blueprint $table) {
            //$table->dropColumn(['side']);
            $table->string('exchange_position_id')->nullable();
            //$table->enum('side', ['buy', 'sell', 'Buy', 'Sell','short','long'])->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_positions', function (Blueprint $table) {
           $table->dropColumn(['exchange_position_id']);
        });
    }
}
