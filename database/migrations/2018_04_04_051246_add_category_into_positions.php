<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCategoryIntoPositions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_orders', function (Blueprint $table) {
            $table->dropColumn(['position_value_usd', 'position_closed', 'user_dashboard_tab', 'success']);
            $table->boolean('processed')->default('false');
            $table->string('ccxt_symbol')->nullable();
        });

        if (Schema::hasColumn('user_positions', 'position_value_in_usd')) {
            Schema::table('user_positions', function (Blueprint $table) {
                $table->dropColumn(['position_value_in_usd']);
            });
        }

        Schema::table('user_positions', function (Blueprint $table) {
            $table->string('ccxt_symbol')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
