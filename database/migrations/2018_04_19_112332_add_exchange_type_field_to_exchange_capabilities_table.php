<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExchangeTypeFieldToExchangeCapabilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('exchange_capabilities', function (Blueprint $table) {
            $table->enum('exchange_type',['S','C'])->default('C')->comment("S = Stock Exchange, C = Cryto Exchange")->after("exchange_name");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('exchange_capabilities', function (Blueprint $table) {
            $table->dropColumn('exchange_type');
        });
    }
}
