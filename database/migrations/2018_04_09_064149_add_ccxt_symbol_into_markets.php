<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCcxtSymbolIntoMarkets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('exchange_markets', function (Blueprint $table) {
            $table->string('ccxt_symbol')->nullable();
            $table->enum('type', ['stock', 'crypto', 'futures'])->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('exchange_markets', function (Blueprint $table) {
            $table->dropColumn(['ccxt_symbol', 'type']);
        });
    }
}
