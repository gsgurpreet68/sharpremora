<?php

use Carbon\Carbon;
use ccxt\Exchange;
use Illuminate\Database\Seeder;
use App\Model\SupportedExchanges;


class SupportedExchangesSeeder extends Seeder
{
    public $active_exchanges = ['binance', 'bitfinex', 'bitmex', 'bittrex'];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $exchanges = Exchange::$exchanges;
        $insert = [];
        // SupportedExchanges::truncate();
        foreach ($exchanges as $exchange) {
            $insert[] = [
                'name' => $exchange,
                'active' => in_array($exchange, $this->active_exchanges) ? true : false,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ];
        }

        SupportedExchanges::insert($insert);
    }
}
