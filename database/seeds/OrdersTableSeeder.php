<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement("TRUNCATE user_orders RESTART IDENTITY CASCADE");
        $now = Carbon::now();
        $data = [];
        $userId = 7;
        for ($i = 0; $i <= 50; $i++) {
            if ($i > 10 && $i < 20) {
                $data[] = [
                    'user_id' => $userId,
                    'exchange_id' => 17,
                    'symbol' => str_random(5),
                    'status' => 'open',
                    'price' => 0.01,
                    'side' => $i % 2 === 0 ? 'buy' : 'sell',
                    'type' => 'limit',
                    'price_currency' => 'BTC',
                    'asset_type' => 'crypto',
                    'created_at' => $now->subDay(3),
                    'exchange_response' => json_encode([])
                ];
            } elseif ($i > 40) {
                $data[] = [
                    'price' => 0.01,
                    'user_id' => $userId,
                    'exchange_id' => 21,
                    'symbol' => str_random(5),
                    'status' => 'cancelled',
                    'side' => $i % 2 === 0 ? 'buy' : 'sell',
                    'type' => 'limit',
                    'price_currency' => 'BTC',
                    'asset_type' => 'crypto',
                    'created_at' => $now->subDay(2),
                    'exchange_response' => json_encode([])
                ];
            } else {
                $data[] = [
                    'user_id' => $userId,
                    'exchange_id' => 59,
                    'symbol' => str_random(5),
                    'status' => 'filled',
                    'price' => 0.01,
                    'side' => $i % 2 === 0 ? 'buy' : 'sell',
                    'type' => 'limit',
                    'price_currency' => 'BTC',
                    'asset_type' => 'crypto',
                    'created_at' => $now->subDay(5),
                    'exchange_response' => json_encode([])
                ];
            }
        }

        \App\Model\Orders::insert($data);
    }
}
