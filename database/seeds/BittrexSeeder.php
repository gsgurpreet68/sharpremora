<?php

use App\Model\SupportedExchanges;
use App\Model\Symbol;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use ccxt\bittrex;
use Illuminate\Support\Facades\DB;

class BittrexSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $se = SupportedExchanges::where('name', 'bittrex')->first();
        if ($se) {
            Symbol::where(['exchange_id' => $se->id])->delete();
            $exchange = new ccxt\bittrex();
            $markets = $exchange->fetchMarkets();
            $data = [];
            $now = Carbon::now()->format('Y-m-d H:i:s');
            foreach ($markets as $market) {
                $data[] = [
                    'exchange_id' => $se->id,
                    'symbol' => $market['symbol'],
                    'base' => $market['base'],
                    'quote' => $market['quote'],
                    'created_at' => $now,
                    'updated_at' => $now
                ];
            }

            Symbol::insert($data);
        }
    }
}
