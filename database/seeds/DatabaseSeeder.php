<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([SupportedExchangesSeeder::class]);
        // $this->call([BittrexSeeder::class]);
        $this->call([BinanceMarkets::class]);
        
    }
}
