<?php

use App\Model\SupportedExchanges;
use App\Model\Symbol;
use Carbon\Carbon;
use ccxt\binance;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BinanceMarkets extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $se = SupportedExchanges::where('name', 'binance')->first();
        if ($se) {
            $exchange = new ccxt\binance();
            $markets = $exchange->fetchMarkets();
            $data = [];
            $now = Carbon::now()->format('Y-m-d H:i:s');
            foreach ($markets as $market) {
                $data[] = [
                    'symbol' => $market['info']['symbol'],
                    'exchange_id' => $se->id,
                    'base' => $market['base'],
                    'quote' => $market['quote'],
                    'created_at' => $now,
                    'updated_at' => $now
                ];
            }

            Symbol::insert($data);
        }
    }
}
