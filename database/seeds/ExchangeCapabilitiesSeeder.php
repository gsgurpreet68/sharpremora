<?php

use Illuminate\Database\Seeder;

class ExchangeCapabilitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $capabilities = [];
        $exchanges = ['binance', 'bitmex', 'bittrex', 'kucoin', 'gdax', 'cryptopia'];
        foreach ($exchanges as $exchange) {
            switch ($exchange) {
                case 'binance':
                    $capabilities[] = [
                        'exchange_name' => $exchange,
                        'limit_order' => true,
                        'market_order' => true,
                        'stop_loss_order' => true,
                        'take_profit_order' => true,
                        'time_in_force' => true
                    ];
                    break;
                case 'bitmex':
                    $capabilities[] = [
                        'exchange_name' => $exchange,
                        'limit_order' => true,
                        'market_order' => true,
                        'stop_loss_order' => true,
                        'take_profit_order' => false,
                        'time_in_force' => false
                    ];
                    break;
                case 'bittrex':
                    $capabilities[] = [
                        'exchange_name' => $exchange,
                        'limit_order' => true,
                        'market_order' => false,
                        'stop_loss_order' => false,
                        'take_profit_order' => false,
                        'time_in_force' => false
                    ];
                    break;
                case 'cryptopia':
                    $capabilities[] = [
                        'exchange_name' => $exchange,
                        'limit_order' => true,
                        'market_order' => false,
                        'stop_loss_order' => false,
                        'take_profit_order' => false,
                        'time_in_force' => false
                    ];
                    break;
                case 'kucoin':
                    $capabilities[] = [
                        'exchange_name' => $exchange,
                        'limit_order' => true,
                        'market_order' => false,
                        'stop_loss_order' => false,
                        'take_profit_order' => false,
                        'time_in_force' => false
                    ];
                    break;
                case 'gdax':
                    $capabilities[] = [
                        'exchange_name' => $exchange,
                        'limit_order' => true,
                        'market_order' => true,
                        'stop_loss_order' => false,
                        'take_profit_order' => false,
                        'time_in_force' => true
                    ];
                    break;
                case 'bitstamp':
                    $capabilities[] = [
                        'exchange_name' => $exchange,
                        'limit_order' => true,
                        'market_order' => true,
                        'stop_loss_order' => false,
                        'take_profit_order' => false,
                        'time_in_force' => false
                    ];
                    break;
            }
        }
        \App\Model\ExchangeCapability::truncate();
        \App\Model\ExchangeCapability::insert($capabilities);
    }
}
