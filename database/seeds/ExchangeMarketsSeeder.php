<?php

use Illuminate\Database\Seeder;
use App\Model\ExchangeMarkets;

class ExchangeMarketsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ExchangeMarkets::truncate();
        $insert = [];

        // For binance
        $binance = new ccxt\binance();
        $fetchedMarkets = $binance->fetchMarkets();
        foreach ($fetchedMarkets as $fm) {
            $insert[] = [
                'exchange_name' => 'binance',
                'symbol' => $fm['info']['symbol'],
                'base' => $fm['base'],
                'quote' => $fm['quote'],
                'ccxt_symbol' => $fm['symbol'], 'type' => 'crypto'
            ];
        }

        // For bittrex
        $bittrex = new ccxt\bittrex();
        $fetchedMarkets = $bittrex->fetchMarkets();
        foreach ($fetchedMarkets as $fm) {
            $insert[] = [
                'exchange_name' => 'bittrex',
                'symbol' => $fm['info']['MarketName'],
                'base' => $fm['base'],
                'quote' => $fm['quote'],
                'ccxt_symbol' => $fm['symbol'], 'type' => 'crypto',
            ];
        }

        // For bitmex
        $bitmex = new ccxt\bitmex();
        $fetchedMarkets = $bitmex->fetchMarkets();
        foreach ($fetchedMarkets as $fm) {
            $insert[] = [
                'exchange_name' => 'bitmex',
                'symbol' => $fm['info']['symbol'],
                'base' => $fm['base'],
                'quote' => $fm['quote'],
                'ccxt_symbol' => $fm['symbol'], 'type' => 'crypto'
            ];
        }

        // For bitfinex
        /*$bitfinex = new ccxt\bitfinex();
        $fetchedMarkets = $bitfinex->fetchMarkets();
        foreach ($fetchedMarkets as $fm) {
            if (!$fm['active']) {
                continue;
            }
            $insert[] = [
                'exchange_name' => 'bitfinex',
                'symbol' => $fm['symbol'], 'type' => 'crypto',
                'base' => $fm['base'],
                'quote' => $fm['quote']
            ];
        }*/

        // For cryptopia
        $cryptopia = new ccxt\cryptopia();
        $fetchedMarkets = $cryptopia->fetchMarkets();
        foreach ($fetchedMarkets as $fm) {
            $insert[] = [
                'exchange_name' => 'cryptopia',
                'symbol' => $fm['info']['Label'],
                'base' => $fm['base'],
                'quote' => $fm['quote'],
                'ccxt_symbol' => $fm['symbol'], 'type' => 'crypto'
            ];
        }

        // For kucoin
        $kucoin = new ccxt\kucoin();
        $fetchedMarkets = $kucoin->fetchMarkets();
        foreach ($fetchedMarkets as $fm) {
            $insert[] = [
                'exchange_name' => 'kucoin',
                'symbol' => $fm['info']['symbol'],
                'base' => $fm['base'],
                'quote' => $fm['quote'],
                'ccxt_symbol' => $fm['symbol'], 'type' => 'crypto'
            ];
        }

        // For bitstamp
        $bitstamp = new ccxt\bitstamp();
        $fetchedMarkets = $bitstamp->fetchMarkets();
        foreach ($fetchedMarkets as $fm) {
            $insert[] = [
                'exchange_name' => 'bitstamp',
                'symbol' => $fm['info']['name'],
                'base' => $fm['base'],
                'quote' => $fm['quote'],
                'ccxt_symbol' => $fm['symbol'],
                'type' => 'crypto'
            ];
        }

        // for gdax
        $gdax = new ccxt\gdax();
        $markets = $gdax->fetchMarkets();
        foreach ($markets as $fm) {
            $insert[] = [
                'exchange_name' => 'gdax',
                'symbol' => $fm['info']['display_name'],
                'base' => $fm['base'],
                'quote' => $fm['quote'],
                'ccxt_symbol' => $fm['symbol'],
                'type' => 'crypto'
            ];
        }

        ExchangeMarkets::insert($insert);
    }

}
