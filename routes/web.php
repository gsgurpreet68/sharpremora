<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/hash', function () {
    return view('welcome');
});


Route::get('{reactRoutes}', function () {
    return view('welcome'); // your start view
})->where('reactRoutes', '^((?!api).)*$'); // except 'api' word
//Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

Route::get('/verify/email', 'Reset@verifyNewEmail');

Route::get('/reset/password/view', 'Reset@resetPasswordView');

Route::post('/reset/password', 'Reset@resetPassword');

//Route::get('/test', 'API\Exchange@test');