<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/register', 'Auth\RegisterController@registerUsingApi');

Route::post('/login', 'Auth\LoginController@loginUsingApi');

Route::post('/password/reset', 'Auth\ForgotPasswordController@passwordResetUsingApi');

Route::group(['middleware' => ['auth:api']], function () {

    Route::group(['prefix' => 'v1'], function () {
        Route::group(['prefix' => 'user'], function () {
            // get current authenticated user info
            Route::get('/me', 'API\UserController@details');

            // update user info
            Route::post('/update/profile', 'API\UserController@update');

            // update password
            Route::post('/update/password', 'API\UserController@updatePassword');
        });

        // exchange related
        Route::group(['prefix' => 'exchange'], function () {
            // connect to an exchange
            Route::post('/connect', 'API\Exchange@connect');

            // connected exchanges
            Route::get('/connected', 'API\Exchange@connectedExchanges');

            // disconnect from an exchange
            Route::post('/disconnect', 'API\Exchange@disconnectExchange');

            // supported exchanges
            Route::get('/supported', 'API\Exchange@supportedExchanges');

            // get markets for an exchange
            Route::get('/markets', 'API\Exchange@getMarkets');

            // fetch open orders
            Route::post('/fetch-open-orders', 'API\FetchOrders@fetchOpenOrders');
            Route::get('/capabilities', 'API\Exchange@exchangeCapabilities');
            Route::get('/get-balance', 'API\Exchange@getBalance');
        });

        Route::group(['prefix' => 'orders'], function () {
            Route::get('/', 'API\FetchOrders@getOrders');
            Route::post('/cancel-order', 'API\FetchOrders@cancelOrder');
            Route::get('/calendar-dates-list', 'API\FetchOrders@calendarDatesList');
        });

        Route::group(['prefix' => 'calendar'], function () {
            Route::get('/positions', 'API\Calendar@getPositions');
        });

        // order or position
        Route::group(['prefix' => 'position'], function () {
            Route::post('/open', 'API\Position@openPosition');
            Route::get('/openpositions', 'API\Position@getOpenPositions');
            Route::post('/close', 'API\ClosePosition@closePosition');
        });

        // get symbols for autocomplete
        Route::get('/symbols', 'API\Symbols@getSymbols');

        // dashboard
        Route::group(['prefix' => 'dashboard'], function () {
            Route::get('/get-open-positions', 'API\Dashboard@openPositions');
            Route::get('/get-categories-names', 'API\Dashboard@getCategoriesNames');
            Route::post('/create-position-category', 'API\Dashboard@createPositionCategory');
            Route::post('/edit-position-category', 'API\Dashboard@editPositionCategoryName');
            Route::post('/delete-position-category', 'API\Dashboard@deletePositionCategory');
            Route::post('/add-position-into-category', 'API\Dashboard@addPositionIntoCategory');
        });
    });
});

Route::get('/test', 'API\Exchange@test');