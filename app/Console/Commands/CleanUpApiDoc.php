<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CleanUpApiDoc extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clean-up-api';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "While using the laravel doc generator, it doesn't format 
    the response in correct way, that's why this custom command used. Reference- 
https://github.com/mpociot/laravel-apidoc-generator/issues/198#issuecomment-350689404";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $file = __DIR__ . '/../../../public/docs/index.html';
        if (file_exists($file)) {
            $html = file_get_contents($file);

            $html = str_replace('\n', '<br>', $html);
            $html = str_replace('"{', '{', $html);
            $html = str_replace('}"', '}', $html);
            $html = str_replace('\"', '"', $html);

            file_put_contents($file, $html);
        }
    }
}
