<?php

namespace App\Providers;

use App\Model\Orders;
use App\Model\Positions;
use App\Policies\OrderPolicy;
use App\Policies\PositionPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        Positions::class => PositionPolicy::class,
        Orders::class => OrderPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // added for laravel passport
        Passport::routes();
        Passport::tokensExpireIn(now()->addDays(15));
        Passport::refreshTokensExpireIn(now()->addDays(30));

        Gate::define('position-category-operations', function ($user, $positionCategory) {
            return $positionCategory->user_id === $user->id;
        });

        Gate::define('position-operations', function ($user, $position) {
            return $position->user_id === $user->id;
        });
    }
}
