<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Symbol extends Model
{
    protected $table = 'symbols';

    protected $fillable = [
        'exchange_id',
        'symbol',
        'base',
        'quote',
        'type'
    ];

    public function exchange()
    {
        return $this->hasMany(SupportedExchanges::class, 'id', 'exchange_id');
    }
}
