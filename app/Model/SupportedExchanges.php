<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SupportedExchanges extends Model
{
    protected $table = 'supported_exchanges';
    protected $fillable = ['name', 'active', 'created_at', 'updated_at'];
    protected $date = ['created_at', 'updated_at'];
}
