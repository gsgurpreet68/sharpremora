<?php

namespace App\Model;

use App\Model\SupportedExchanges;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExchangeConn extends Model
{
    use SoftDeletes;
    /**
     * table for this model
     * @var string
     */
    protected $table = 'exchange_conn';
    protected $dates = ['deleted_at'];

    /**
     * Fields that are mass-assignable
     * @var array
     */
    protected $fillable = [
        'user_id',
        'api_key',
        'api_secret',
        'api_password',
        'exchange',
        'deleted',
        'permission_obj_obtained',
        'permissions_obtained',
        'api_uid',
        'api_password'
    ];


    public function exchange()
    {
        return $this->hasOne(SupportedExchanges::class, 'name');
    }
}
