<?php

namespace App\Model\PositionCategories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PositionCategoryName extends Model
{
    use softDeletes;

    protected $table = 'user_position_categories_names';

    protected $fillable = [
        'user_id',
        'name'
    ];

    protected $dates = ['deleted_at', 'created_at', 'updated_at'];

    /**
     * One category name id may be at multiple rows
     * in categories table
     */
    public function Categories()
    {
        return $this->hasMany(PositionCategory::class);
    }
}
