<?php

namespace App\Model\PositionCategories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PositionCategory extends Model
{
    use softDeletes;

    protected $table = 'user_position_categories';

    protected $fillable = [
        'user_id',
        'user_positions_id',
        'user_position_categories_names_id'
    ];

    protected $dates = ['deleted_at', 'created_at', 'updated_at'];

    /**
     * Get category name for this category id
     */
    public function CategoryName()
    {
        return $this->belongsTo(PositionCategoryName::class);
    }
}
