<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PasswordReset extends Model
{
    /**
     * table for this model
     * @var string
     */
    protected $table = 'password_resets';

    /**
     * fields that can be filled
     * @var array
     */
    protected $fillable = [
        'email',
        'token',
        'expired_at'
    ];

    /**
     * no updated at column
     * @var boolean
     */
    public $timestamps = false;
}
