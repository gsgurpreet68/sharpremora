<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Positions extends Model
{
    use SoftDeletes;

    protected $table = 'user_positions';

    protected $fillable = [
        'exchange_id',
        'user_id',
        'user_dashboard_tab_name_id',
        'asset_type',
        'quantity',
        'symbol',
        'side',
        'price',
        'price_currency',
        'exchange_position_id',
        'ccxt_symbol'
    ];

    protected $dates = ['deleted_at', 'created_at', 'updated_at'];
}
