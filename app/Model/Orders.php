<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    protected $table = 'user_orders';

    protected $fillable = [
        'exchange_id',
        'user_id',
        'symbol',
        'status',
        'exchange_response',
        'side',
        'type',
        'quantity',
        'price',
        'price_currency',
        'position_value_usd',
        'asset_type',
        'position_closed',
        'exchange_order_id',
        'success',
        'ccxt_symbol'
    ];
}
