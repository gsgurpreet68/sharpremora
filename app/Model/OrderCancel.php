<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderCancel extends Model
{
    use softdeletes;

    protected $table = 'user_orders_cancelled';

    protected $fillable = [
        'user_id',
        'user_order_id',
        'exchange_response'
    ];

    protected $dates = ['deleted_at', 'created_at', 'updated_at'];
}
