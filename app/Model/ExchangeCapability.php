<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ExchangeCapability extends Model
{
    protected $table = 'exchange_capabilities';

    protected $fillable = [
        'exchange_name',
        'limit_order',
        'market_order',
        'stop_loss_order',
        'take_profit_order',
        'time_in_force'
    ];
}
