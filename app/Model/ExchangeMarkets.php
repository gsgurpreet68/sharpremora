<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ExchangeMarkets extends Model
{
    //
    protected $table = 'exchange_markets';
    protected $fillable = ['exchange_name', 'symbol', 'base', 'quote'];

    public static function getMarkets($exchange, $marketName = null)
    {
        $fetchedMarkets = ExchangeMarkets::where('exchange_name', $exchange);
        if ($marketName != 'undefined' && $marketName) {
            $fetchedMarkets = $fetchedMarkets->where('ccxt_symbol', 'ilike', $marketName);
        }
        $fetchedMarkets = $fetchedMarkets->get();
        if ($fetchedMarkets) {
            foreach ($fetchedMarkets as $fm) {
                $fm->label = $fm->symbol;
                $fm->value = $fm->ccxt_symbol;
            }
            return $fetchedMarkets;
        } else {
            return [];
        }
    }
}
