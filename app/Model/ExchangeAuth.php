<?php

namespace App\Model;

use App\Model\SupportedExchanges;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExchangeAuth extends Model
{
    use SoftDeletes;
    /**
     * table for this model
     * @var string
     */
    protected $table = 'exchange_auth';
    protected $dates = ['deleted_at'];

    /**
     * Fields that are mass-assignable
     * @var array
     */
    protected $fillable = [
        'user_id',
        'exchange_id',
        'auth_token',
        'active'
    ];


    public function exchange()
    {
        return $this->hasOne(SupportedExchanges::class, 'id');
    }
}
