<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ChangeEmail extends Model
{
    /**
     * Table for this model
     * @var string
     */
    protected $table = 'change_user_email';

    /**
     * fields that can be filled with it
     * @var array
     */
    protected $fillable = [
        'user_id',
        'new_email',
        'token',
        'verified',
        'expired_at'
    ];
}
