<?php

namespace App\Policies;

use App\Model\Positions;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PositionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can close the positions.
     *
     * @param  \App\User $user
     * @param \App\Model\Positions $positions
     * @return mixed
     */
    public function close(User $user, Positions $positions)
    {
        return $user->id === $positions->user_id;
    }

    /**
     * Determine whether the user can create positions.
     *
     * @param  \App\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the positions.
     *
     * @param  \App\User $user
     * @param  \App\Positions $positions
     * @return mixed
     */
    public function update(User $user, Positions $positions)
    {
        //
    }

    /**
     * Determine whether the user can delete the positions.
     *
     * @param  \App\User $user
     * @param  \App\Positions $positions
     * @return mixed
     */
    public function delete(User $user, Positions $positions)
    {
        //
    }
}
