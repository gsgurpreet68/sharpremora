<?php

namespace App\Policies;

use App\Model\Orders;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class OrderPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Check if the order can be cancelled by the user or not
     * @param User $user
     * @param Orders $order
     * @return bool
     */
    public function cancel(User $user, Orders $order)
    {
        return $user->id === $order->user_id && $order->status != 'cancelled';
    }

    /**
     * Check if the user can do anything with the order
     * basically check if the user owns that order
     * @param User $user
     * @param Orders $order
     * @return bool
     */
    public function edit(User $user, Orders $order)
    {
        return $user->id === $order->user_id;
    }
}
