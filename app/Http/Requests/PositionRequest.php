<?php

namespace App\Http\Requests;

use App\Model\SupportedExchanges;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PositionRequest extends FormRequest
{
    private $side = ['buy', 'sell', 'Buy', 'Sell'];
    private $type = [
        'stop_loss',
        'limit',
        'market',
        'stop_loss_limit',
        'take_profit',
        'take_profit_limit',
        'limit_maker',
        'stop_entry',
        'market_limit',
        'stop_market',

    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // any rule with sometimes is described here
        // so that it shows in the api doc
        return [
            'exchange_name' => 'required',
            'symbol' => 'required',
            'type' => ['required', Rule::in($this->type)],
            'side' => ['required', Rule::in($this->side)],
            'quantity' => 'required|numeric',
            'price' => 'required_unless:type,market|numeric',
            'stop_price' => 'sometimes',
            'time_in_force' => 'sometimes'
        ];
    }

    /**
     * Validation error custom msg
     * @return array
     */
    public function messages()
    {
        return [
            'side.in' => 'Please select either buy or sell',
            'side.required' => 'Please select either buy or sell',
            'type.required' => 'Please select the kind of order',
            'symbol.required' => 'Please select the asset',
            'quantity.required' => 'Kindly provide a quantity',
            'price.required_unless' => 'Please specify the price'
        ];
    }
}
