<?php

namespace App\Http\Requests;

use Illuminate\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class ConnectToExchange extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */


    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'exchange_name' => 'required',
            'api_key' => 'required',
            'api_secret' => 'required',
            'extra_1' => 'required_if:exchange_name,bitstamp|required_if:exchange_name,gdax',
            'revalidate' => 'sometimes|boolean'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'api_key.required' => 'Please enter api key',
            'api_secret.required' => 'Please enter api secret'
            //'exchange_name.in' => 'Exchange not supported, only ' . implode(', ', $this->exchanges) . ' are supported.'
        ];
    }


    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator $validator
     * @return void
     */
    // public function withValidator($validator)
    // {
    //     $validator->after(function ($validator) {
    //         if ($this->somethingElseIsInvalid()) {
    //             $validator->errors()->add('supportedExchanges',implode(', ', $this->exchanges));
    //         }
    //     });
    // }
}
