<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClosePositionReq extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_position_id' => 'required|numeric',
            'exit_size' => 'sometimes|numeric'
        ];
    }

    /**
     * Get error messages for validation error
     * @return array
     */
    public function messages()
    {
        return [
            'user_position_id.required' => 'Please provide user position id',
            'exit_size.numeric' => 'Please send the percentage number of how much 
            of that position you want to exit. Eg- 25 for 25% and so on.'
        ];
    }
}
