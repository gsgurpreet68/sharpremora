<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class GetOrders extends FormRequest
{
    private $statuses = ['open', 'cancelled', 'filled'];

    private $orderTimeOrders = ['asc', 'desc'];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_status' => ['sometimes', 'string', Rule::in($this->statuses)],
            'exchange_name' => 'sometimes|string',
            'order_time_flow' => ['sometimes', 'string', Rule::in($this->orderTimeOrders)],
            'page' => 'sometimes|numeric',
            'from_date' => 'sometimes|nullable|date_format:"Y-m-d"|before_or_equal:to_date',
            'to_date' => 'sometimes|nullable|date_format:"Y-m-d"|after_or_equal:from_date'
        ];
    }
}
