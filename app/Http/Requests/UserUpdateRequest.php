<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $request = array_keys($this->all());
        $user = \Auth::user();

        $rule = [];

        if(in_array('first_name',$request))
        {
            $rule['first_name'] = 'required';
        }

        if(in_array('last_name',$request))
        {
            $rule['last_name'] = 'required';
        }

        if(in_array('email',$request))
        {
            $rule['email'] = 'required|email|unique:users,email,'.$user->id;
        }
        

        return $rule;
    }
}
