<?php

namespace App\Http\Controllers;

use App\Model\ChangeEmail;
use App\Model\PasswordReset;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class Reset extends Controller
{
    /**
     * verify user's new email
     * @param Request $request
     * @return string
     */
    public function verifyNewEmail(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'token' => 'required'
        ]);

        $id = decrypt($request->get('id'));
        $changeEmail = ChangeEmail::find($id);

        if (!$changeEmail->verified) {
            if (strtotime((new \DateTime())->format('Y-m-d H:i:s')) < strtotime($changeEmail->expired_at)) {
                DB::beginTransaction();
                $user = User::find($changeEmail->user_id);
                $user->email = $changeEmail->new_email;
                $user->save();

                $changeEmail->verified = true;
                $changeEmail->save();

                DB::commit();

                return "email updated";
            } else {
                return "link expired";
            }
        } else {
            return "already verified";
        }
    }

    /**
     * reset password view
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function resetPasswordView(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'token' => 'required'
        ]);
        return view('auth.passwords.reset', [
            'token' => $request->get('token'),
            'id' => $request->get('id')
        ]);
    }

    /**
     * reset password
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function resetPassword(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'token' => 'required',
            'email' => 'required',
            'password' => 'required',
            'password_confirmation' => 'required|same:password'
        ]);

        $id = $request->get('id');
        $id = decrypt($id);

        $token = $request->get('token');
        $email = $request->get('email');
        $password = $request->get('password');

        $passwordReset = PasswordReset::find($id);
        if (strtotime((new \DateTime())->format('Y-m-d H:i:s')) < strtotime($passwordReset->expired_at)) {
            if (!$passwordReset->used) {
                if ($passwordReset->email == $email) {
                    if ($passwordReset->token == $token) {
                        DB::beginTransaction();
                        $user = User::where('email', $email)->first();
                        $user->password = Hash::make($password);
                        $user->save();

                        $passwordReset->used = true;
                        $passwordReset->save();

                        // log out from everywhere
                        DB::table('oauth_access_tokens')
                            ->where('user_id', $user->id)
                            ->delete();

                        DB::commit();
                        return back()->with('status', 'Password reset successfully');
                    } else {
                        return back()->with('status', 'Token does not match');
                    }
                } else {
                    return back()->with('status', 'Email does not match. Kindly provide right mail. Thank you.');
                }
            } else {
                return back()->with('status', 'Link already used to reset. Kindly request for a new reset link. Thank you');
            }
        } else {
            return back()->with('status', 'Link expired. Kindly request for a new reset link. Thank you');
        }
    }
}
