<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\BaseApiController;
use App\Model\Symbol;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class Symbols extends BaseApiController
{
    /**
     * Get symbols
     * Used for autocomplete
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSymbols(Request $request)
    {
        Validator::make($request->all(), [
            'symbol' => 'required'
        ])->validate();

        $symbol = $request->get('symbol');

        // if - or / present in the symbol
        // that means most probably the user has input standard
        // symbol with something like base-quote or base/quote
        if (preg_match('/-|\//', $symbol, $matches)) {
            $str = explode($matches[0], $symbol);
            $symbols = Symbol::where('base', 'ilike', '%' . $str[0] . '%');
            if (count($str) >= 2) {
                $symbols = $symbols->where('quote', 'ilike', '%' . $str[1] . '%');
            }
            $symbols = $symbols
                ->take(20)
                ->get();
        } else {
            $symbol = '%' . $symbol . '%';

            $symbols = Symbol::where('symbol', 'ilike', $symbol)
                ->orWhere('base', 'ilike', $symbol)
                ->orWhere('quote', 'ilike', $symbol)
                ->take(20)
                ->get();
        }

        $allSymbols = [];
        // different exchanges have different format for symbol
        // how the symbol is formed is basically
        // base + {a special chrc like / or - or something else}+ quote
        // in order to maintain a standard we use - in between base and quote
        // when we send this symbol data to exchange
        // we format it according to needed for that exchange
        foreach ($symbols as $symbol) {
            $stdSymbol = $symbol->base . '-' . $symbol->quote;
            if (!in_array($stdSymbol, $allSymbols)) {
                $allSymbols[] = $stdSymbol;
            }
        }

        return $this->success('Symbols retrieved', ['symbols' => $allSymbols]);
    }
}
