<?php

namespace App\Http\Controllers\API;

use App\User;
use Carbon\Carbon;
use App\Model\ChangeEmail;
use App\Custom\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\UserResource;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use App\Http\Requests\UserUpdateRequest;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\BaseApiController;
use App\Http\Requests\updatePasswordRequest;
use App\Notifications\VerifyChangedEmailNotification;

class UserController extends BaseApiController
{
    /**
     * Get details about the current authenticated user
     * @return mixed
     */
    public function details()
    {
        $user = new UserResource(\App\User::find(Auth::user()->id));
        return $this->success('Retrieved user info', $user);
    }

    /**
     * Update the current authenticated user
     * @param UserUpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UserUpdateRequest $request)
    {
        $user = \Auth::user();
        $email = $user->email;
        $update = $request->all();
        $otherResponse = [];

        try {
            DB::beginTransaction();
            $token = bin2hex(random_bytes(32));
            $changeEmail = ChangeEmail::create([
                'user_id' => $user->id,
                'new_email' => $email,
                'token' => $token,
                'expired_at' => Carbon::now()->addHours(24)
            ])->id;

            $user->update($update);
            $msg = 'Profile updated ';
                
            if (isset($update['email']) && $email != $update['email']) {
                $user->notify(new VerifyChangedEmailNotification($user, $token));
                $msg .= '& kindly verify new email address.';
                $otherResponse['verify_email'] = true;
            }

            DB::commit();

            return $this->success($msg, [], 200, $otherResponse);

        } catch (\Exception $e) {
            DB::rollback();
            return $this->error($e->getMessage());
        }
    }

    /**
     * Update user's password
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updatePassword(updatePasswordRequest $request)
    {
        $user = Auth::user();

        if (!Hash::check($request->old_password, $user->password)) {
            $msg = 'Old password does not match';
            return $this->error($msg);
        }
        try {
            DB::beginTransaction();
            $user->password = Hash::make($request->get('new_password'));
            $user->save();

            // log out from everywhere
            DB::table('oauth_access_tokens')
                ->where('user_id', \Auth::user()->id)
                ->delete();

            DB::commit();

            $msg = 'Password updated and logged out of everywhere';
            return $this->success($msg);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->error($e->getMessage());
        }
    }
}
