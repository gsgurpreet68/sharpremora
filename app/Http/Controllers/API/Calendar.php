<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\BaseApiController;
use App\Http\Requests\PositionsOnADay;
use App\Model\Positions;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * @resource Calendar
 * Get the dates when order(s) was/were placed.
 * Get the positions opened and closed on dates
 */
class Calendar extends BaseApiController
{
    /**
     * Get positions on dates
     * Get activity with positions on a particualar date.
     * If something was opened or closed.
     * Send `date` in `YYYY-MM-DD` format a.k.a UTC format
     * @response {
     *  "success": true,
     *  "msg": "Retrieved positions",
     *  "data" : [
     *      "openedPositions": [],
     *      "closedPositions: []
     *   ]
     * }
     * @param PositionsOnADay $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPositions(PositionsOnADay $request)
    {
        $date = $request->get('date');
        $query = DB::table('user_positions as up')
            ->join('supported_exchanges as se', 'se.id', '=', 'up.exchange_id')
            ->select('up.*', 'se.name as exchange_name')
            ->where('user_id', Auth::user()->id);

        $openedPositions = $query->where(DB::raw('up.created_at::date'), '=', $date)->get();
        $closedPositions = $query->where(DB::raw('up.deleted_at::date'), '=', $date)
            ->get();

        return $this->success('Retrieved positions', [
            'openedPositions' => $openedPositions,
            'closedPositions' => $closedPositions
        ]);
    }
}
