<?php

namespace App\Http\Controllers\API;

use App\Model\ExchangeMarkets;
use App\Model\Orders;
use Carbon\Carbon;
use ccxt;
use App\Http\Controllers\BaseApiController;
use App\Http\Requests\ClosePositionReq;
use App\Model\ExchangeConn;
use App\Model\Positions;
use App\Model\SupportedExchanges;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

/**
 * @resource Close Position
 * Close a position on various exchanges
 */
class ClosePosition extends BaseApiController
{
    /**
     * Close a position on Binance
     * @param $apiCredentials
     * @param $position - position instance
     * @param $exitSize -  exit size like 25 for 25% and so on
     * @param $side - buy or sell
     * @return array
     */
    private function closePositionOnBinance($apiCredentials, $position, $exitSize, $side)
    {
        $exchangeParams = [
            'side' => $side,
            'symbol' => $position->symbol,
            'type' => 'market',
            'quantity' => $exitSize
        ];

        try {
            $binance = new ccxt\binance([
                'apiKey' => $apiCredentials->api_key,
                'secret' => $apiCredentials->api_secret
            ]);

            // binance needs timestamp and that value has to
            // 13 digit micro secs
            $exchangeParams['timestamp'] = $this->getCurrentMicroSecs();

            $order = $binance->privatePostOrder($exchangeParams);
            $status = strtolower($order['status']);

            return [
                'error' => false,
                'orderPlaced' => true,
                'order' => $order,
                'price' => $order['price'],
                'orderStatus' => $status,
                'exchangeOrderId' => $order['orderId'],
                'msg' => $status == 'filled' ? 'Closed position' : 'Something went wrong. Kindly contact support'
            ];
        } catch (\Exception $e) {
            return [
                'error' => true,
                'msg' => $this->getErrorMsgFromExchange($e, 'binance')
            ];
        }
    }

    /**
     * Close a position on bitmex
     * @param $apiCredentials
     * @param $position
     * @param $exitSize
     * @return array
     */
    private function closePositionOnBitmex($apiCredentials, $position, $exitSize, $side)
    {
        try {
            $bitmex = new ccxt\bitmex([
                'apiKey' => $apiCredentials->api_key,
                'secret' => $apiCredentials->api_secret
            ]);

            $order = $bitmex->privatePostOrder([
                'symbol' => $position->symbol,
                'side' => ucfirst($side),
                'orderQty' => $exitSize,
                'ordType' => 'Market'
            ]);

            $orderStatus = strtolower($order['ordStatus']);
            $orderStatus = $orderStatus == 'new' ? 'open' : $orderStatus;

            if ($orderStatus == 'filled') {
                $msg = 'Position closed';
            } else {
                $msg = 'Order sent to close position and status is ' . $orderStatus;
            }
            return [
                'error' => false,
                'msg' => $msg,
                'orderPlaced' => true,
                'order' => $order,
                'price' => $order['price'],
                'exchangeOrderId' => $order['orderID'],
                'orderStatus' => $orderStatus
            ];
        } catch (\Exception $e) {
            return [
                'error' => true,
                'msg' => $this->getErrorMsgFromExchange($e, 'bitmex')
            ];
        }
    }

    /**
     * close a position on Bittrex
     * @param $apiCredentials
     * @param $position
     * @param $exitSize
     * @param $side
     * @return array|void
     */
    private function closePositionBittrex($apiCredentials, $position, $exitSize, $side)
    {
        try {
            $bittrex = new ccxt\bittrex();
            $bittrex->apiKey = $apiCredentials->api_key;
            $bittrex->secret = $apiCredentials->api_secret;
            // we use ccxt symbol here because we're using ccxt method
            $symbol = $position->ccxt_symbol;
            $currentMarket = $bittrex->fetch_ticker($symbol);

            $order = [];
            if ($side == 'buy') {
                $rate = $currentMarket['ask'];
                $order = $bittrex->createLimitBuyOrder($symbol, $exitSize, $rate);
            } elseif ($side == 'sell') {
                $rate = $currentMarket['bid'];
                $order = $bittrex->createLimitSellOrder($symbol, $exitSize, $rate);
            }

            if (empty($order)) {
                return [
                    'error' => true,
                    'msg' => 'Something went wrong. Contact support.'
                ];
            }
            $ordStatus = strtolower($order['status']);
            // on Bittrex, there's no way to place market order
            // we get the latest ticker and place a market order using that data

            if ($ordStatus == 'closed') {
                $msg = 'Position closed';
            } elseif ($ordStatus == 'open') {
                $msg = 'Request sent to Bittrex for closing position. 
            Currently, status is open';
            } else {
                $msg = 'Order sent to exchange and status is ' . $ordStatus;
            }
            return [
                'error' => false,
                'orderPlaced' => true,
                'msg' => $msg,
                'order' => $order,
                'exchangeOrderId' => $order['id'],
                'price' => $rate,
                'orderStatus' => $ordStatus
            ];
        } catch (\Exception $e) {
            return [
                'error' => true,
                'msg' => $this->getErrorMsgFromExchange($e, 'bittrex')
            ];
        }
    }

    /**
     * Close position on KuCoin
     * @param $apiCredentials
     * @param $position
     * @param $exitSize
     * @param $side
     * @return array
     */
    private function closePositionOnKuCoin($apiCredentials, $position, $exitSize, $side)
    {
        try {
            $kucoin = new ccxt\kucoin([
                'apiKey' => $apiCredentials->api_key,
                'secret' => $apiCredentials->api_secret
            ]);
            // ccxt symbol used here because we use ccxt method
            $symbol = $position->ccxt_symbol;
            $currentMarket = $kucoin->fetchTicker($symbol);
            $order = [];
            if ($side == 'buy') {
                $price = (float)$currentMarket['ask'];
                $order = $kucoin->createLimitBuyOrder($symbol, $exitSize, $price);
            } elseif ($side == 'sell') {
                $price = (float)$currentMarket['bid'];
                $order = $kucoin->createLimitSellOrder($symbol, $exitSize, $price);
            }

            if (empty($order)) {
                return [
                    'error' => true,
                    'msg' => 'Something went wrong. Contact support.'
                ];
            }

            $status = strtolower($order['status']);
            if ($status == 'filled') {
                $msg = 'Position closed';
            } elseif ($status == 'open') {
                $msg = 'Order sent to exchange and currently waiting to get filled.';
            } else {
                $msg = 'Order sent and status is ' . $status;
            }

            return [
                'error' => false,
                'orderPlaced' => true,
                'order' => $order,
                'msg' => $msg,
                'price' => $price,
                'exchangeOrderId' => $order['id'],
                'orderStatus' => $status
            ];
        } catch (\Exception $e) {
            return [
                'error' => true,
                'msg' => $this->getErrorMsgFromExchange($e, 'kucoin')
            ];
        }
    }


    /**
     * Close position on Gdax
     * @param $apiCredentials
     * @param $position
     * @param $exitSize
     * @param $side
     * @return array
     */
    private function closePositionGdax($apiCredentials, $position, $exitSize, $side)
    {
        try {

            $gdax = new ccxt\gdax([
                'apiKey' => $apiCredentials->api_key,
                'secret' => $apiCredentials->api_secret,
                'password' => $apiCredentials->api_password
            ]);

            $symbol = $position->symbol;

            $order = $gdax->privatePostOrders([
                'product_id' => $position->symbol,
                'side' => strtolower($side),
                'funds' => $exitSize,
                'type' => 'market'
            ]);
            // get the status of order
            // privatepostorder always return pending status
            // so we check it by order ID
            $orderDetail = $gdax->fetchOrder($order['id']);

            $orderStatus = strtolower($orderDetail['status']);

            if ($orderStatus == 'closed') {
                $msg = 'Position closed';
            } else {
                $msg = 'Order sent to close and status is ' . $orderStatus;
            }
            return [
                'error' => false,
                'msg' => $msg,
                'orderPlaced' => true,
                'order' => $order,
                'price' => $orderDetail['price'],
                'exchangeOrderId' => $order['id'],
                'orderStatus' => $orderStatus
            ];
        } catch (\Exception $e) {
            return [
                'error' => true,
                'msg' => $this->getErrorMsgFromExchange($e, 'kucoin')
            ];
        }
    }


    /**
     * Close position on Whaleclub
     * @param $apiCredentials
     * @param $position
     * @param $exitSize
     * @param $side
     * @return array
     */
    private function closePositionWhaleclub($apiCredentials, $position, $exitSize, $side, $request)
    {

        $positionId = '';
        $newClosedPostionId = '';

        try {
            $whaleclubUrl = "https://api.whaleclub.co/v1/";
            $ratio = $request->get('exit_size');
            $headers = array(
                'Content-Type: application/json',
                sprintf('Authorization: Bearer %s', $apiCredentials->api_key)
            );

            // open position id 
            // whaleclub need ID of position to close it
            if ($position->exchange_position_id) {
                $positionId = $position->exchange_position_id;
            } else {
                return [
                    'error' => true,
                    'msg' => "Position id does't exist."
                ];
            }

            // if ratio is less 100 then we have to split position in ratio
            // and close the new generated first position from pair.

            if ($ratio && $ratio < 100) {
                $data['ratio'] = $ratio;
                $whaleclub = curl_init($whaleclubUrl . "position/split/" . $positionId);
                curl_setopt($whaleclub, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($whaleclub, CURLOPT_POSTFIELDS, json_encode($data));
                curl_setopt($whaleclub, CURLOPT_RETURNTRANSFER, true);
                $splitPosition = json_decode(curl_exec($whaleclub));
                curl_close($whaleclub);

                if (isset($splitPosition->code) && $splitPosition->code == 404) {
                    return [
                        'error' => true,
                        'msg' => $splitPosition->message
                    ];
                }
                // store new positions array into DB
                if (sizeof($splitPosition)) {
                    foreach ($splitPosition as $key => $value) {
                        $positionData = [
                            'user_id' => Auth::user()->id,
                            'exchange_id' => $position->exchange_id,
                            'symbol' => $position->symbol,
                            'quantity' => $value->size,
                            'price' => $position->price,
                            'price_currency' => $position->price_currency,
                            'asset_type' => 'stock',
                            'side' => $position->side,
                            'exchange_position_id' => $value->id
                        ];
                        $record = Positions::create($positionData);
                        // get the new splited position id and exchange id
                        if ($key == 0) {
                            $positionId = $record->exchange_position_id;
                            $newClosedPostionId = $record->id;
                        }
                    }
                }

            }

            // closing the position by ID
            $whaleclubClose = curl_init($whaleclubUrl . "position/close/" . $positionId);

            curl_setopt($whaleclubClose, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($whaleclubClose, CURLOPT_CUSTOMREQUEST, 'PUT');
            curl_setopt($whaleclubClose, CURLOPT_RETURNTRANSFER, true);

            $positionStatus = json_decode(curl_exec($whaleclubClose));


            $msg = 'Something went wrong. Contact support';

            if (isset($positionStatus->error)) {
                $msg = $positionStatus->error->message;
            }

            if (isset($positionStatus->state)) {
                $orderStatus = $positionStatus->state;
            } else {
                $orderStatus = "error";
            }

            if ($orderStatus == 'closed') {
                // deleting position from DB
                if ($newClosedPostionId) {
                    Positions::destroy($newClosedPostionId);
                }

                return [
                    'error' => false,
                    'msg' => 'Position closed'
                ];
            } else {
                return [
                    'error' => true,
                    'msg' => $msg
                ];
            }
        } catch (\Exception $e) {
            return [
                'error' => true,
                'msg' => $this->getErrorMsgFromExchange($e, 'kucoin')
            ];
        }
    }


    /**
     * Close a position
     * @param ClosePositionReq $request
     * @return array|\Illuminate\Http\JsonResponse|void
     */
    public function closePosition(ClosePositionReq $request)
    {
        $userPositionId = $request->get('user_position_id');
        $position = Positions::find($userPositionId);
        $user = Auth::user();

        if ($user->can('close', $position)) {
            $exchange = SupportedExchanges::where([
                'id' => $position->exchange_id,
                'active' => true
            ])->first();

            $apiCredentials = ExchangeConn::where([
                'user_id' => Auth::user()->id,
                'exchange' => $position->exchange_id
            ])->first();

            $exchangeName = strtolower($exchange->name);
            $exitSize = $request->get('exit_size');
            $qty = $position->quantity;
            $exitSize = $exitSize ? (($qty * $exitSize) / 100) : $position->quantity;
            // sometimes, if it was a sell order, we have - before the number
            $exitSize = abs($exitSize);
            $side = strtolower($position->side) == 'buy' ? 'sell' : 'buy';

            $positionClosed = [];
            switch ($exchangeName) {
                case 'binance':
                    $positionClosed = $this->closePositionOnBinance($apiCredentials, $position, $exitSize, $side);
                    break;
                case 'bitmex':
                    $positionClosed = $this->closePositionOnBitmex($apiCredentials, $position, $exitSize, $side);
                    break;
                case 'bittrex':
                    $positionClosed = $this->closePositionBittrex($apiCredentials, $position, $exitSize, $side);
                    break;
                case 'gdax':
                    $positionClosed = $this->closePositionGdax($apiCredentials, $position, $exitSize, $side);
                    break;
                case 'kucoin':
                    $positionClosed = $this->closePositionOnKuCoin($apiCredentials, $position, $exitSize);
                    break;
                case stripos($exchangeName, 'whaleclub') !== false:
                    $positionClosed = $this->closePositionWhaleclub($apiCredentials, $position, $exitSize, $side, $request);
                    break;
            }

            if (empty($positionClosed)) {
                return $this->error('Something went wrong. Contact support.');
            }

            if (array_key_exists('orderPlaced', $positionClosed)) {
                $order = $positionClosed['order'];
                $exchangeName = strtolower($exchange->name);
                switch ($exchangeName) {
                    case 'binance':
                    case 'gdax':
                    case 'bitmex':
                        $type = 'market';
                        break;
                    case 'bittrex':
                    case 'kucoin':
                        $type = 'limit'; // we place a limit order to close order here
                        break;
                    default:
                        $type = null;
                        break;
                }
                // get the price currency for that symbol
                $symbol = ExchangeMarkets::where([
                    'exchange_name' => $exchangeName,
                    'symbol' => $position->symbol
                ])->first();

                $priceCurrency = null;
                if ($side == 'buy') {
                    $priceCurrency = $symbol->base;
                } elseif ($side == 'sell') {
                    $priceCurrency = $symbol->quote;
                }
                Orders::create([
                    'exchange_id' => $position->exchange_id,
                    'user_id' => Auth::user()->id,
                    'asset_type' => $position->asset_type,
                    'symbol' => $position->symbol,
                    'quantity' => $exitSize,
                    'side' => $side,
                    'price_currency' => $priceCurrency,
                    'type' => $type,
                    'ccxt_symbol' => $symbol->ccxt_symbol,
                    'price' => $positionClosed['price'],
                    'exchange_order_id' => $positionClosed['exchangeOrderId'],
                    'exchange_response' => json_encode($order),
                    'status' => $positionClosed['orderStatus']
                ]);

                // it means one exited that order partially
                if ($exitSize != abs($position->quantity)) {
                    // we update that position's size
                    // and create another position with everything
                    // same except for quantity
                    // save it
                    if ($side == 'buy') {
                        $newQty = $position->quantity + $exitSize;
                    } elseif ($side == 'sell') {
                        $newQty = $position->quantity - $exchangeName;
                    }
                    $position->quantity = $side == 'sell' ? $exitSize : -$exitSize;
                    $position->save();

                    $position->delete();

                    Positions::insert([
                        'exchange_id' => $position->exchange_id,
                        'user_id' => Auth::user()->id,
                        'asset_type' => $position->asset_type,
                        'symbol' => $position->symbol,
                        'quantity' => $newQty,
                        'price' => $position->price,
                        'price_currency' => $position->price_currency,
                        'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                        'side' => $position->side,
                        'ccxt_symbol' => $position->ccxt_symbol
                    ]);
                } else {
                    $position->delete();
                }
            }

            if ($positionClosed['error']) {
                return $this->error($positionClosed['msg']);
            }
            return $this->success('Position closed');
        } else {
            return $this->error('You can\'t close this position', [], 501);
        }
    }
}
