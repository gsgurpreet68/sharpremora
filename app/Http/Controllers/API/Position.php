<?php

namespace App\Http\Controllers\API;

use App\Model\Orders;
use App\Model\Positions;
use ccxt;
use App\Http\Controllers\BaseApiController;
use App\Http\Requests\PositionRequest;
use App\Model\ExchangeConn;
use App\Model\ExchangeAuth;
use App\Model\SupportedExchanges;
use App\Model\ExchangeMarkets;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

/**
 * @resource Open Position
 * Open a position on various exchanges
 */
class Position extends BaseApiController
{
    /**
     * Open a position on binance
     * @param PositionRequest $request
     * @param $se - supported exchange database obj for binance
     * @param $apiCredentials - api cred obj for binance
     * @return array
     */
    private function openPositionOnBinance($request, $se, $apiCredentials)
    {
        $type = [
            'stop_loss',
            'stop_loss_limit',
            'limit',
            'market',
            'market_limit',
        ];

        // Sometimes we need some exchange specific validation
        Validator::make($request->all(),
            [
                'type' => [Rule::in($type)],
                'stop_price' => 'required_if:type,stop_loss',
                'time_in_force' => 'required_if:type,limit,stop_loss_limit'
            ],
            [
                'time_in_force.required_if' => 'Kindly select the time in force',
            ])->validate();

        $qty = $request->get('quantity');
        $type = strtolower($request->get('type'));
        $price = $request->get('price');
        $side = strtolower($request->get('side'));
        $symbol = strtoupper($request->get('symbol'));

        $binance = new ccxt\binance([
            'apiKey' => $apiCredentials->api_key,
            'secret' => $apiCredentials->api_secret
        ]);

        try {
            $exchangeParams = [
                'side' => $request->get('side'),
            ];
            $timeInForce = strtoupper($request->get('time_in_force'));
            switch ($type) {
                case 'limit':
                    $exchangeParams['type'] = 'LIMIT';
                    $exchangeParams['timeInForce'] = $timeInForce;
                    $exchangeParams['price'] = $price;
                    break;
                case 'stop_loss':
                    $exchangeParams['type'] = 'MARKET';
                    $exchangeParams['stopPrice'] = $request->get('stop_price');
                    break;
                case 'market':
                    $exchangeParams['type'] = 'MARKET';
                    break;

                case 'stop_loss_limit':
                    $exchangeParams['type'] = 'STOP_LOSS_LIMIT';
                    $exchangeParams['stopPrice'] = $request->get('stop_price');
                    break;
            }

            $markets = $binance->fetchMarkets();
            // check if symbol valid or not
            $symbolKey = array_search($symbol, array_column($markets, 'symbol'));

            // don't know why !$symbolKey always kept on returning
            // invalid symbol while $symbolKey === false worked as expected
            if ($symbolKey === false) {
                return [
                    'error' => true,
                    'msg' => 'Invalid symbol'
                ];
            }
            // since we're using Binance's method for placing
            // we will need to use the binance version of the symbol
            // ETH/BTC in ccxt equals to ETHBTC on Binance
            $exchangeParams['symbol'] = $markets[$symbolKey]['info']['symbol'];
            $market = $markets[$symbolKey];
            // check if currently trading or not
            $isTrading = $market['info']['status'];

            if (strtolower($isTrading) != 'trading') {
                return [
                    'error' => true,
                    'msg' => 'This symbol is currently not trading'
                ];
            }

            // check if this type of order is supported for this symbol or not
            $orderTypes = $market['info']['orderTypes'];
            $supported = in_array(strtoupper($type), $orderTypes);

            if (!$supported) {
                return [
                    'error' => true,
                    'msg' => str_replace('_', ' ', $type) . ' order is not supported for this symbol'
                ];
            }

            // check if qty is less than max and greater than min
            $minQty = $market['limits']['amount']['min'];
            $maxQty = $market['limits']['amount']['max'];

            if ($qty < $minQty || $qty > $maxQty) {
                return [
                    'error' => true,
                    'msg' => 'Invalid quantity. Min qty is ' . $minQty . ' and max qty is ' . $maxQty
                ];
            }

            /*// check if price is > min price and < max price
            $minPrice = $market['limits']['price']['min'];
            $maxPrice = $market['limits']['price']['max'];

            if ($price < $minPrice || $price > $maxQty) {
                $msg = 'Invalid price. Min price 
                can be ' . $minPrice . ' and max price can be ' . $maxPrice;

                return [
                    'error' => true,
                    'msg' => $msg
                ];
            }*/

            // on Binance, there's a special error case if you don't send
            // the right decimal digits
            // for more info, read this below link
            //https://www.reddit.com/r/BinanceExchange/comments/7ucllm/binance_api_error_1013_filter_failure_price_filter/
            //$pricePrecision = $market['precision']['price'];
            //$qtyPrecision = $market['precision']['amount'];

            // Kindly read the above link to understand why we did it
            //$price = number_format($price, $pricePrecision, '.', ' ');
            //$qty = number_format($price, $qtyPrecision, '.', ' ');

            $exchangeParams['quantity'] = $qty;

            // if it's a sell, then under the base currency, one must have balance
            // if it's a buy, then under the quote currency, one must have balance
            // eg, if it's HMQBTC pair and it's a sell, then one must have balance under
            // HMQ and if it's a buy, then one must have balance under BTC
            $priceCurrency = '';
            if ($side == 'buy') {
                $priceCurrency = $market['quote'];
            } elseif ($side == 'sell') {
                $priceCurrency = $market['base'];
            }

            // 13 digit micro secs format is needed. Not less
            $serverTime = $binance->publicGetTime();
            $serverTime = $serverTime['serverTime'];
            $exchangeParams['timestamp'] = $serverTime;

            $order = $binance->privatePostOrder($exchangeParams);
            $status = $order['status'];

            $exchangeOrderId = $order['orderId'];

            // the order data that will be inserted into
            // user_orders table a.k.a Orders model
            $orderData = [
                'user_id' => Auth::user()->id,
                'exchange_id' => $se->id,
                'status' => $status,
                'symbol' => $exchangeParams['symbol'],
                'exchange_response' => json_encode($order),
                'side' => $side,
                'type' => $type,
                'quantity' => $qty,
                'price' => $order['price'],
                'price_currency' => $priceCurrency,
                'asset_type' => 'crypto',
                'exchange_order_id' => $exchangeOrderId,
                'ccxt_symbol' => $symbol
            ];

            // we check if this order was filled
            // if yes, then it becomes a position and we store
            // it in user_positions table a.k.a Positions model
            $positionData = [];
            if (strtolower($status) == 'filled') {
                $positionData = [
                    'user_id' => Auth::user()->id,
                    'exchange_id' => $se->id,
                    'asset_type' => 'crypto',
                    'symbol' => $exchangeParams['symbol'],
                    'ccxt_symbol' => $symbol,
                    'quantity' => $qty,
                    'price' => $order['price'],
                    'price_currency' => $priceCurrency
                ];
            }

            return [
                'error' => false,
                'orderData' => $orderData,
                // binance returns new for any open order
                'status' => strtolower($status) == 'new' ? 'Open' : ucfirst($status),
                'positionData' => $positionData
            ];
        } catch (\Exception $e) {
            $msg = $this->getErrorMsgFromExchange($e, 'binance');
            if (preg_match('/(notional)/i', $msg)) {
                $msg = 'Total order value has to be more than 0.001 BTC';
            }
            return [
                'error' => true,
                'msg' => $msg
            ];
        }
    }

    /**
     * Open a position on bittrex
     * @param PositionRequest $request
     * @param $se
     * @param $apiCredentials
     * @return array
     */
    private function openPositionOnBittrex($request, $se, $apiCredentials)
    {
        $types = ['limit'];
        Validator::make($request->all(),
            [
                'type' => [Rule::in($types)],
            ],
            [
                'type.in' => 'Supported types are ' . implode(',', $types),
            ])->validate();

        // in bittrex, they call symbol as market
        $market = $request->get('symbol');
        $ccxtSymbol = str_replace('-', '/', $market);

        // price is called rate on bittrex
        $rate = $request->get('price');

        $qty = $request->get('quantity');
        $side = strtolower($request->get('side'));

        try {
            $bittrex = new ccxt\bittrex();
            $bittrex->apiKey = $apiCredentials->api_key;
            $bittrex->secret = $apiCredentials->api_secret;

            $symbols = $bittrex->fetchMarkets();
            $key = array_search($market, array_column($symbols, 'symbol'));

            if ($key === false) {
                return [
                    'error' => true,
                    'msg' => 'Invalid symbol'
                ];
            }

            $symbol = $symbols[$key];
            $minTradeQty = $symbol['info']['MinTradeSize'];
            $symbolActive = $symbol['info']['IsActive'];
            $bittrexSymbol = $symbols[$key]['info']['MarketName'];

            if (!$symbolActive) {
                return [
                    'error' => true,
                    'msg' => 'Symbol not active for trading now'
                ];
            }

            if ($qty < $minTradeQty) {
                return [
                    'error' => true,
                    'msg' => 'Quantity can not be less than ' . $minTradeQty
                ];
            }

            if ($side == 'buy') {
                $priceCurrency = $symbols[$key]['info']['BaseCurrency'];
            } elseif ($side == 'sell') {
                $priceCurrency = $symbols[$key]['info']['MarketCurrency'];
            }
            // search for the symbol in bittrex
            /* $symbols = $bittrex->fetchMarkets();
             $key = array_search($ccxtSymbol, array_column($symbols, 'symbol'));

             if ($key === false) {
                 return [
                     'error' => true,
                     'msg' => 'Invalid symbol'
                 ];
             }



             // get balance and verify if sufficient bal is there
             $balances = $bittrex->fetchBalance();
             $balances = $balances['info'];*/

            // if it's a sell, then under the base currency, one must have balance
            // if it's a buy, then under the quote currency, one must have balance
            // eg, if it's HMQ/BTC pair and it's a sell, then one must have balance under
            // HMQ and if it's a buy, then one must have balance under BTC
            /*if ($side == 'buy') {
                $currency = $symbol['quote'];
            } else {
                $currency = $symbol['base'];
            }*/

            /*$balanceFoundKey = array_search(strtoupper($currency), array_column($balances, 'Currency'));

            if (!$balanceFoundKey) {
                return [
                    'error' => true,
                    'msg' => 'Couldn\'t find balance. Kindly try again. Thank you.'
                ];
            }*/

            /*$avlBal = $balances[$balanceFoundKey]['Available'];
            $totalPrice = $qty * $rate;
            if ($totalPrice > $avlBal) {
                return [

                ];
                return $this->error('');
            }*/

            // If trade is market then we need to pass current price
            // of symbol
            /*if ($tradeType == "market") {
                $currentMarket = $bittrex->fetch_ticker($market);
                $rate = (float)$currentMarket['ask'];
            } else if ($tradeType == 'stop_loss') {
                return $this->error('Stop loss not supported yet');
            } else if ($tradeType == 'stop_loss_limit') {
                return $this->error('STOP LOSS LIMIT not supported yet');
            } else if ($tradeType == 'market_limit') {
                return $this->error('MARKET LIMIT not supported yet');
            }*/

            $order = [];
            if ($side == 'buy') {
                $order = $bittrex->createLimitBuyOrder($market, $qty, $rate);
            } elseif ($side == 'sell') {
                $order = $bittrex->createLimitSellOrder($market, $qty, $rate);
            }

            if (!empty($order)) {
                $exchangeOrderId = $order['id'];

                // Bittrex always returns status as open
                // even if it was closed. SO, we catch the order id and make another
                // request to fetch that order status
                $fetchOrder = $bittrex->fetchOrder($exchangeOrderId);

                $orderStatus = $fetchOrder['status'];

                $orderData = [
                    'user_id' => Auth::user()->id,
                    'exchange_id' => $se->id,
                    'status' => $orderStatus,
                    'symbol' => $ccxtSymbol,
                    'ccxt_symbol' => strtoupper($request->get('symbol')),
                    'exchange_response' => json_encode($order),
                    'side' => $side,
                    'type' => $request->get('type'),
                    'quantity' => $qty,
                    'price' => $rate,
                    'price_currency' => $priceCurrency,
                    'asset_type' => 'crypto',
                    'exchange_order_id' => $exchangeOrderId
                ];
                $positionData = [];

                if ($orderStatus == 'closed' || $orderStatus == 'close') {
                    $positionData = [
                        'user_id' => Auth::user()->id,
                        'exchange_id' => $se->id,
                        'asset_type' => 'crypto',
                        'symbol' => $bittrexSymbol,
                        'ccxt_symbol' => strtoupper($request->get('symbol')),
                        'quantity' => $qty,
                        'side' => strtolower($side),
                        'price' => $rate,
                        'price_currency' => $priceCurrency
                    ];
                }

                return [
                    'error' => false,
                    'positionData' => $positionData,
                    'status' => $orderStatus,
                    'orderData' => $orderData
                ];
            } else {
                return [
                    'error' => true,
                    'msg' => 'Something went wrong. No order placed.'
                ];
            }
        } catch (\Exception $e) {
            return [
                'error' => true,
                'msg' => $this->getErrorMsgFromExchange($e, 'bittrex')
            ];
        }
    }

    /**
     * Open a position on cryptopia
     * @param $request
     * @param $se
     * @param $apiCredentials
     * @return \Illuminate\Http\JsonResponse
     */
    private function openPositionOnCryptopia($request, $se, $apiCredentials)
    {
        $types = ['limit', 'market'];

        //keep seperate validator for each exchange
        Validator::make($request->all(),
            [
                'type' => [Rule::in($types)],
                'price' => 'required_if:type,limit|numeric',
            ],
            [
                'type.in' => 'Supported types are ' . implode(',', $types),
            ])->validate();

        $market = $request->get('symbol');

        // price is called rate on bittrex
        $rate = $request->get('price');

        $qty = $request->get('quantity');
        $tradeType = strtolower($request->get('type'));
        $side = strtolower($request->get('side'));

        try {
            $cryptopia = new ccxt\cryptopia();
            $cryptopia->apiKey = $apiCredentials->api_key;
            $cryptopia->secret = $apiCredentials->api_secret;

            // search for the symbol in bittrex
            $symbols = $cryptopia->fetchMarkets();
            $key = array_search($market, array_column($symbols, 'symbol'));
            $symbol = $symbols[$key];

            if (!$key) {
                return $this->error('Symbol is invalid');
            }
            $minTradeQty = (float)$symbol['info']['MinimumTrade'];
            $symbolActive = $symbol['active'];

            if (!$symbolActive) {
                return $this->error('Symbol not active for trading now');
            }

            if ($qty < $minTradeQty) {
                return $this->error('Quantity can not be less than ' . $minTradeQty);
            }

            // get balance and verify if sufficient bal is there
            $balances = $cryptopia->fetchBalance();
            $balances = $balances['info']['Data'];

            // if it's a sell, then under the base currency, one must have balance
            // if it's a buy, then under the quote currency, one must have balance
            // eg, if it's HMQ/BTC pair and it's a sell, then one must have balance under
            // HMQ and if it's a buy, then one must have balance under BTC
            if ($side == 'buy') {
                $currency = $symbol['quote'];
            } else {
                $currency = $symbol['base'];
            }

            $balanceFoundKey = array_search(strtoupper($currency), array_column($balances, 'Symbol'));

            if (!$balanceFoundKey) {
                return $this->error('Couldn\'t find balance. Kindly try again. Thank you.');
            }

            $avlBal = $balances[$balanceFoundKey]['Available'];
            $totalPrice = $qty * $rate;


            if ($totalPrice > $avlBal) {
                return $this->error('Not enough balance to make this trade');
            }
            if ($tradeType == "market") {
                return $this->error('MARKET order not supported yet');
            } else if ($tradeType == 'stop_loss') {
                return $this->error('Stop loss not supported yet');
            } else if ($tradeType == 'stop_loss_limit') {
                return $this->error('STOP LOSS LIMIT not supported yet');
            } else if ($tradeType == 'market_limit') {
                return $this->error('MARKET LIMIT not supported yet');
            }

            $order = [];
            // cryptopia allow limit order only
            if ($side == 'buy') {
                $order = $cryptopia->createLimitBuyOrder($market, $qty, $rate);
            } elseif ($side == 'sell') {
                $order = $cryptopia->createLimitSellOrder($market, $qty, $rate);
            }

            if (!empty($order)) {
                $orderInfo = $order['info'];
                $success = false;

                if (isset($orderInfo) && isset($orderInfo['Success'])) {
                    $executed = $orderInfo['Success'];
                    if ($executed) {
                        $success = true;
                        $msg = 'Order executed successfully';
                    } else {
                        $msg = 'Order not executed';
                    }
                } else {
                    $msg = 'No info returned by exchange. Kindly make sure that price and quantity are valid.';
                }

                try {
                    Positions::create([
                        'user_id' => Auth::user()->id,
                        'exchange_id' => $se->id,
                        'status' => $success ? 'success' : 'not success',
                        'symbol' => $market,
                        'exchange_response' => json_encode($order),
                        'side' => $side,
                        'type' => $request->get('type'),
                        'quantity' => $qty,
                        'price' => $rate
                    ]);
                    $storedInDatabase = true;
                } catch (\Exception $e) {
                    $storedInDatabase = false;
                }

                if ($success) {
                    return $this->success($msg . ($storedInDatabase ? ' & stored in  database' :
                            'but couldn\'t store in the database'),
                        [
                            'exchangeReturned' => $order
                        ], $storedInDatabase ? 201 : 200);
                } else {
                    return $this->error($msg, [], 404, ['exchangeReturned' => $order]);
                }
            } else {
                return $this->error('Something went wrong. No order placed.');
            }


        } catch (\Exception $e) {
            return $this->error($e->getMessage());
        }

    }


    /**
     * Open a position on Bitfinex
     * @param $request
     */
    private function openPositionOnBitfinex($request)
    {

    }

    /**
     * Open a position on Bitmex
     * @param $request
     * @param $se
     * @param $apiCredentials
     * @return array|\Illuminate\Http\JsonResponse
     */
    private function openPositionOnBitmex($request, $se, $apiCredentials)
    {
        $types = ['limit', 'market', 'stop_loss'];
        Validator::make($request->all(),
            [
                'type' => [Rule::in($types)],
            ],
            [
                'type.in' => 'Supported types are ' . implode(',', $types),
            ])->validate();

        $symbol = strtoupper($request->get('symbol'));
        $price = $request->get('price');
        $tradeType = strtolower($request->get('type'));

        // quantity (needs to be whole number as we trade futures contract on Bitmex and 
        // not the asset directly) is called orderQty on Bitmex
        $orderQty = ceil($request->get('quantity'));
        $type = strtolower($request->get('type'));
        $tradeType = $type == 'limit' ? 'Limit' : ($type == 'market' ? 'Market' : ($type == 'market_limit' ? 'MarketWithLeftOverAsLimit' : 'Stop'));
        $side = strtolower($request->get('side'));

        try {
            $bitmex = new ccxt\bitmex();
            $bitmex->apiKey = $apiCredentials->api_key;
            $bitmex->secret = $apiCredentials->api_secret;

            // search for the symbol in bitmex
            $symbols = $bitmex->fetchMarkets();

            $key = array_search($symbol, array_column($symbols, 'symbol'));

            if ($key === false) {
                return [
                    'error' => true,
                    'msg' => 'Symbol is invalid'
                ];
            }

            $priceCurrency = null;
            if (strtolower($side) == 'buy') {
                $priceCurrency = $symbols[$key]['base'];
            } elseif (strtolower($side) == 'sell') {
                $priceCurrency = $symbols[$key]['quote'];
            }

            // let this be handled by the exchange
            // if there's not enough balance, exchange gonna return
            // that error anyway and we can show it

            // get balance and verify if sufficient bal is there
            /*$balances = $bitmex->fetchBalance();
            if (isset($balances[$basequote[1]])) {
                $balance = $balances[$basequote[$side == 'buy' ? 1 : 0]]['free'];
            } else {
                return [
                    'error' => true,
                    'msg' => 'Couldn\'t find balance. Kindly try again. Thank you.'
                ];
            }

            $avlBal = $balance;
            $totalPrice = $orderQty * $price;
            if ($totalPrice > $avlBal) {
                return [
                    'error' => true,
                    'msg' => 'Not enough balance to make this trade'
                ];
            }*/

            // Place order code here
            $order = $bitmex->privatePostOrder([
                'symbol' => $symbol,
                'side' => ucfirst($side),
                'price' => $price,
                'orderQty' => $orderQty,
                'ordType' => $tradeType
            ]);
            $exchangeOrderId = $order['orderID'];
            $orderStatus = strtolower($order['ordStatus']);
            // new basically means open order
            $orderStatus = $orderStatus == 'new' ? 'open' : $orderStatus;

            $orderData = [
                'user_id' => Auth::user()->id,
                'exchange_id' => $se->id,
                'status' => $orderStatus,
                'symbol' => $symbol,
                'ccxt_symbol' => $symbol,
                'exchange_response' => json_encode($order),
                'side' => $side,
                'type' => $request->get('type'),
                'quantity' => $orderQty,
                'price' => $price,
                'price_currency' => $priceCurrency,
                'asset_type' => 'crypto',
                'exchange_order_id' => $exchangeOrderId
            ];

            // if order status is filled that means it was executed
            // and can now be counted as a position
            $positionData = [];
            if ($orderStatus == 'filled') {
                $positionData = [
                    'exchange_id' => $se->id,
                    'user_id' => Auth::user()->id,
                    'asset_type' => 'crypto',
                    'symbol' => $order['symbol'],
                    'ccxt_symbol' => $symbol,
                    'quantity' => $orderQty,
                    'side' => $side,
                    'price' => $order['price'],
                    'price_currency' => $priceCurrency
                ];
            }
            return [
                'error' => false,
                'status' => $orderStatus,
                'orderData' => $orderData,
                'positionData' => $positionData
            ];

        } catch (\Exception $e) {
            return [
                'error' => true,
                'msg' => $this->getErrorMsgFromExchange($e, 'bitmex')
            ];
        }
    }

    /**
     * Open a position on KuCoin
     * @param $request
     * @param $se
     * @param $apiCredentials
     * @return array|\Illuminate\Http\JsonResponse
     */
    private function openPositionOnKuCoin($request, $se, $apiCredentials)
    {
        $types = ['limit', 'market'];
        Validator::make($request->all(), [
            'type' => [Rule::in($types)]
        ], [
            'type.in' => 'Only limit type is supported on KuCoin'
        ]);

        $type = strtolower($request->get('type'));
        $side = strtolower($request->get('side'));
        $symbol = strtoupper($request->get('symbol'));
        $quantity = $request->get('quantity');
        $price = $request->get('price');


        try {
            $kuCoin = new ccxt\kucoin([
                'apiKey' => $apiCredentials->api_key,
                'secret' => $apiCredentials->api_secret
            ]);
            // check if the symbol is valid and active for trading or not
            $markets = $kuCoin->fetchMarkets();

            $marketFoundAt = array_search($symbol, array_column($markets, 'symbol'));
            if ($marketFoundAt === false) {
                return [
                    'error' => true,
                    'msg' => 'Invalid symbol'
                ];
            }

            $marketSymbolInfo = $markets[$marketFoundAt]['info'];
            $isTrading = $marketSymbolInfo['trading'];
            $kucoinSymbol = $markets[$marketFoundAt]['info']['symbol'];

            if (!$isTrading) {
                return [
                    'error' => true,
                    'msg' => 'This symbol not trading right now.'
                ];
            }

            $order = [];
            $priceCurrency = null;

            // If trade is market then we need to pass current price
            // of symbol
            if ($type == "market") {
                $currentMarket = $kuCoin->fetchTicker($symbol);
                $price = (float)$currentMarket['ask'];
            }

            if ($side == 'buy') {
                $priceCurrency = $markets[$marketFoundAt]['base'];
                $order = $kuCoin->createLimitBuyOrder($symbol, $quantity, $price);
            } elseif ($side == 'sell') {
                $priceCurrency = $markets[$marketFoundAt]['quote'];
                $order = $kuCoin->createLimitSellOrder($symbol, $quantity, $price);
            }

            if (empty($order)) {
                return [
                    'error' => true,
                    'msg' => 'Something went wrong. Contact Admin'
                ];
            }

            $exchangeOrderId = $order['id'];
            $status = $order['status'];

            $orderData = [
                'user_id' => Auth::user()->id,
                'exchange_id' => $se->id,
                'status' => $status,
                'symbol' => $kucoinSymbol,
                'ccxt_symbol' => $symbol,
                'exchange_response' => json_encode($order),
                'side' => $side,
                'type' => $type,
                'quantity' => $quantity,
                'price' => $price,
                'price_currency' => $priceCurrency,
                'asset_type' => 'crypto',
                'exchange_order_id' => $exchangeOrderId
            ];

            $positionData = [];

            if (strtolower($status) == 'filled' || strtolower($status) == 'closed') {
                $positionData = [
                    'user_id' => Auth::user()->id,
                    'exchange_id' => $se->id,
                    'asset_type' => 'crypto',
                    'symbol' => $kucoinSymbol,
                    'ccxt_symbol' => $symbol,
                    'quantity' => $quantity,
                    'price' => $price,
                    'price_currency' => $priceCurrency
                ];
            }

            return [
                'error' => false,
                'msg' => 'Order sent and status is ' . $status,
                'orderData' => $orderData,
                'positionData' => $positionData
            ];
        } catch (\Exception $e) {
            return [
                'error' => true,
                'msg' => $this->getErrorMsgFromExchange($e, 'kucoin')
            ];
        }
    }

    /**
     * Open a position on Bitstamp
     * @param $request
     * @param $se
     * @param $apiCredentials
     * @return \Illuminate\Http\JsonResponse
     */
    public function openPositionOnBitstamp($request, $se, $apiCredentials)
    {
        $types = ['market', 'limit'];
        // make sure to validate for order type
        Validator::make($request->all(), [
            'type' => [Rule::in($types)]
        ], [
            'type.in' => 'Supported types of order are limit and market'
        ]);

        $type = strtolower($request->get('type'));
        $side = strtolower($request->get('side'));
        $symbol = $request->get('symbol');
        $quantity = $request->get('quantity');
        $price = $request->get('price');

        // unlike other exchanges, bitstamp requires uid for validation
        $bitstamp = new ccxt\bitstamp([
            'apiKey' => $apiCredentials->api_key,
            'secret' => $apiCredentials->api_secret,
            'uid' => $apiCredentials->api_uid
        ]);

        $markets = $bitstamp->fetchMarkets();
        $foundMarketAt = array_search($symbol, array_column($markets, 'symbol'));
        if ($foundMarketAt === false) {
            return [
                'error' => true,
                'msg' => 'Invalid symbol'
            ];
        }
        $market = $markets[$foundMarketAt];
        if ($tradeType == "market") {
            // return $this->error('MARKET order not supported yet');
        } else if ($tradeType == 'stop_loss') {
            return $this->error('Stop loss not supported yet');
        } else if ($tradeType == 'stop_loss_limit') {
            return $this->error('STOP LOSS LIMIT not supported yet');
        } else if ($tradeType == 'market_limit') {
            return $this->error('MARKET LIMIT not supported yet');
        }
        try {
            $priceCurrency = null;
            $order = [];
            if ($side == 'buy') {
                $priceCurrency = $market['base'];
                if ($type == 'limit') {
                    $order = $bitstamp->createLimitBuyOrder($symbol, $quantity, $price);
                } elseif ($type == 'market') {
                    $order = $bitstamp->createMarketBuyOrder($symbol, $quantity);
                }
            } elseif ($side == 'sell') {
                $priceCurrency = $market['quote'];
                if ($type == 'limit') {
                    $order = $bitstamp->createLimitSellOrder($symbol, $quantity, $price);
                } elseif ($type == 'market') {
                    $order = $bitstamp->createMarketSellOrder($symbol, $quantity);
                }
            }

            if (empty($order)) {
                return $this->error('Something went wrong. Contact Admin');
            }

            $exchangeOrderId = $order['id'];

            // on Bitstamp, it doesn't return the status after you execute that order
            // it just returns something like this
            /*{
                "info": {
                    "price": "175.00",
                    "amount": "0.10000000",
                    "type": "1",
                    "id": "1183682477",
                    "datetime": "2018-03-22 10:53:45.051086"
                },
                "id": "1183682477"
           }*/

            // in order to check the status of that trade, we will have to
            // make another call with the order id
            $orderStatus = $bitstamp->privatePostOrderStatus(['id' => $exchangeOrderId]);

            $status = $orderStatus['status'];

            try {
                Positions::create([
                    'user_id' => Auth::user()->id,
                    'exchange_id' => $se->id,
                    'success' => true,
                    'status' => $status,
                    'symbol' => $symbol,
                    'exchange_response' => json_encode($orderStatus),
                    'side' => $side,
                    'type' => $type,
                    'quantity' => $quantity,
                    'price' => $price,
                    'price_currency' => $priceCurrency,
                    'asset_type' => 'crypto',
                    'exchange_order_id' => $exchangeOrderId
                ]);
                $storedInDatabase = true;
            } catch (\Exception $e) {
                $storedInDatabase = false;
            }

            return $this->success('Order succeeded and current status is ' . $status);
        } catch (\Exception $e) {
            $error = str_replace('bitstamp ', '', $e->getMessage());
            $error = json_decode($error);
            // this is how the json structure looks like after doing the replace
            //"reason": {
            //  "__all__": [
            //      "Minimum order size is 0.001 BTC."
            //  ]
            //}
            if (gettype($error) == 'object') {
                $error = (array)$error;
                // bitstamp returns all the error under reason key
                // reason key consists of another obj
                $errorReason = (array)$error['reason'];
                $msg = '';
                foreach ($errorReason as $errorItemValue) {
                    $msg .= $errorItemValue[0] . '\n';
                }

                return $this->error($msg);
            } else {
                return $this->error($e->getMessage());
            }
        }
    }


    /**
     * Open a position on Gdax
     * @param $request
     * @param $se
     * @param $apiCredentials
     * @return array
     */
    private function openPositionGdax($request, $se, $apiCredentials)
    {
        $type = [
            'limit',
            'market',
        ];

        Validator::make($request->all(),
            [
                'type' => [Rule::in($type)],
                'time_in_force' => 'required_if:type,limit'
            ],
            [
                'type.in' => 'Supported types are ' . implode(',', $type)
            ])->validate();

        $qty = $request->get('quantity');
        $type = strtolower($request->get('type'));
        $price = $request->get('price');
        $side = strtolower($request->get('side'));
        $symbol = strtoupper($request->get('symbol'));

        $gdax = new ccxt\gdax([
            'apiKey' => $apiCredentials->api_key,
            'secret' => $apiCredentials->api_secret,
            'password' => $apiCredentials->api_password
        ]);


        try {
            $exchangeParams = [
                'type' => $type,
                'side' => $request->get('side'),
                'size' => $request->get('quantity')
            ];
            $timeInForce = strtoupper($request->get('time_in_force'));
            switch ($type) {
                case 'limit':
                    $exchangeParams['time_in_force'] = $timeInForce;
                    $exchangeParams['price'] = $request->get('price');
                    break;
                case 'market':
                    $exchangeParams['funds'] = $request->get('funds');
                    break;
            }

            $markets = $gdax->fetchMarkets();
            
            // on Gdax, a valid symbol doesn't contain
            // any - or / in between base and quote
            // the entire symbol looks like
            // this baseQuote.
            // so remove -  symbol in case it's there
            // check if symbol valid or not
            $symbolKey = array_search($symbol, array_column($markets, 'symbol'));
            if ($symbolKey === false) {
                return [
                    'error' => true,
                    'msg' => 'Invalid symbol'
                ];
            }

            $market = $markets[$symbolKey];
            $gdaxSymbolName = $market['info']['display_name'];
            $gdaxSymbolName = str_replace('/', '-', $gdaxSymbolName);
            $exchangeParams['product_id'] = $gdaxSymbolName;
            // check if currently trading or not
            $isTrading = $market['active'];

            if (!$isTrading) {
                return [
                    'error' => true,
                    'msg' => 'This symbol is currently not trading'
                ];
            }
            // check if qty is less than max and greater than min
            // $minQty = $market['limits']['amount']['min'];
            // $maxQty = $market['limits']['amount']['max'];

            // if ($qty < $minQty || $qty > $maxQty) {
            //     return [
            //         'error' => true,
            //         'msg' => 'Invalid quantity. Min qty is ' . $minQty . ' and max qty is ' . $maxQty
            //     ];
            // }

            // // check if price is > min price and < max price
            // $minPrice = $market['limits']['price']['min'];
            // $maxPrice = $market['limits']['price']['max'];

            // if (!$maxPrice) {
            //     if ($price < $minPrice) {
            //         return [
            //             'error' => true,
            //             'msg' => 'Invalid price. Min price 
            //             can be ' . $minPrice
            //         ];
            //     }
            // } else {
            //     if ($price < $minPrice || $price > $maxPrice) {
            //         return [
            //             'error' => true,
            //             'msg' => 'Invalid price. Min price 
            //             can be ' . $minPrice . ' and max price can be ' . $maxPrice
            //         ];
            //     }
            // }

            // if it's a sell, then under the base currency, one must have balance
            // if it's a buy, then under the quote currency, one must have balance
            // eg, if it's HMQBTC pair and it's a sell, then one must have balance under
            // HMQ and if it's a buy, then one must have balance under BTC
            if ($side == 'buy') {
                $priceCurrency = $market['quote'];
            } elseif ($side == 'sell') {
                $priceCurrency = $market['base'];
            }
            $now = Carbon::now();
            // 13 digit micro secs format is needed. Not less
            $micro = round($now->format('U.u') * 1000);
            $exchangeParams['timestamp'] = $micro;

            if ($request->get('stop_loss') && $request->get('stop_entry')) {
                $exchangeParams['stop_price'] = $request->get('stop_price');
                $exchangeParams['stop'] = $request->get('stop');
            }

            $order = $gdax->privatePostOrders($exchangeParams);
            $status = $order['status'];
            $status = $status == 'pending' ? 'open' : $status;
            $exchangeOrderId = $order['id'];

            $orderData = [
                'user_id' => Auth::user()->id,
                'exchange_id' => $se->id,
                'status' => $status,
                'symbol' => $gdaxSymbolName,
                'ccxt_symbol' => $symbol,
                'exchange_response' => json_encode($order),
                'side' => $side,
                'type' => $type,
                'quantity' => $qty,
                'price' => $price,
                'price_currency' => $priceCurrency,
                'asset_type' => 'crypto',
                'exchange_order_id' => $exchangeOrderId
            ];

            // we check if this order was filled
            // if yes, then it becomes a position and we store
            // it in user_positions table a.k.a Positions model
            $positionData = [];
            if (strtolower($status) == 'filled') {
                $positionData = [
                    'user_id' => Auth::user()->id,
                    'exchange_id' => $se->id,
                    'asset_type' => 'crypto',
                    'symbol' => $gdaxSymbolName,
                    'ccxt_symbol' => $symbol,
                    'quantity' => $qty,
                    'price' => $price,
                    'price_currency' => $priceCurrency
                ];
            }

            return [
                'error' => false,
                'orderData' => $orderData,
                // gdax returns pending for any open order
                'status' => ucfirst($status),
                'positionData' => $positionData
            ];
        } catch (\Exception $e) {
            return [
                'error' => true,
                'msg' => $this->getErrorMsgFromExchange($e, 'gdax')
            ];
        }
    }


    /**
     * Open a position on Robinhood
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    private function openPositionOnRobinhood($request) //Robinhod is pending for integration with front end
    {
        $type = [
            'limit',
            'market',
        ];
        $trigger = [
            'immediate',
            'stop',
        ];

        // Kindly keep some validation seperate for each exchange
        // as we will keep on adding more exchanges
        // and each exchange requires seperate set of parameters needed when some specific parameters
        // are true or false
        Validator::make($request->all(),
            [
                'type' => [Rule::in($type)],
                'price' => 'required',
                'time_in_force' => 'required_if:type,limit',
                'trigger' => [Rule::in($trigger)],
                'stop_price' => 'required_if:trigger,stop',
                'account_url' => 'required',
            ],
            [
                'type.in' => 'Supported types are ' . implode(',', $type),
                'trigger.in' => 'Trigger types are ' . implode(',', $trigger),

            ])->validate();

        $robinhoodUrl = "https://api.robinhood.com/";

        //get the request params        
        $qty = $request->get('quantity');
        $type = strtolower($request->get('type'));
        $price = $request->get('price');
        $side = strtolower($request->get('side'));
        $symbol = strtoupper($request->get('symbol'));
        $exchange = $request->get('exchange_name');
        $accountUrl = $request->get('account_url');
        $trigger = $request->get('trigger');
        $time_in_force = $request->get('time_in_force');
        $se = SupportedExchanges::where('name', $exchange)->first();
        $apiCredentials = ExchangeAuth::where([
            'exchange_id' => $se->id,
            'user_id' => Auth::user()->id,
            'active' => true
        ])->first();


        try {
            if (!$apiCredentials) {
                return $this->success('Robinhood auth token has been expired.');
            }

            //check if balance is exist
            $headers = array(
                'Content-Type: application/json',
                sprintf('Authorization: Token %s', $apiCredentials->auth_token)
            );
            $robinhood = curl_init($robinhoodUrl . "accounts/");

            curl_setopt($robinhood, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($robinhood, CURLOPT_RETURNTRANSFER, true);

            $accounts = json_decode(curl_exec($robinhood))->results;
            $balanceToUse = $accounts[0]->buying_power;
            $balanceNeeded = $price * $qty;

            if ($balanceNeeded > $balanceToUse) {
                return $this->error('Not enough balance. Balance needed is ' . $balanceNeeded . ' & free balance is ' . $balanceToUse);
            }
            //check if balance is exist


            //get the instrument URL for specifc symbol
            $headers = array(
                'Content-Type: application/json',
                sprintf('Authorization: Token %s', $apiCredentials->auth_token)
            );
            $robinhood = curl_init($robinhoodUrl . "instruments/");

            curl_setopt($robinhood, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($robinhood, CURLOPT_RETURNTRANSFER, true);

            $markets = json_decode(curl_exec($robinhood))->results;

            $symbolKey = array_search($symbol, array_column($markets, 'symbol'));


            // don't know why !$symbolKey always kept on returning
            // invalid symbol while $symbolKey === false worked as expected
            if ($symbolKey === false) {
                return $this->error('Invalid symbol.');
            }

            $market = $markets[$symbolKey];
            if (!$market->tradeable) {
                return $this->success('Instrument cannot be traded right now.');
            }
            $instrumentURL = $market->url;
            //get the instrument URL completed here


            $headers = array(
                sprintf('Authorization: Token %s', $apiCredentials->auth_token)
            );
            //initialize curl
            $robinhood = curl_init();
            curl_setopt($robinhood, CURLOPT_URL, $robinhoodUrl . "orders/");
            curl_setopt($robinhood, CURLOPT_HTTPHEADER, $headers);

            // set order params
            $data['instrument'] = $instrumentURL;
            $data['account'] = $accountUrl;
            $data['symbol'] = $symbol;
            $data['type'] = $type;
            $data['time_in_force'] = $time_in_force;
            $data['trigger'] = $trigger;
            $data['quantity'] = $qty;
            $data['side'] = $side;
            $data['price'] = $price;

            curl_setopt($robinhood, CURLOPT_POSTFIELDS, $data);
            curl_setopt($robinhood, CURLOPT_RETURNTRANSFER, true);
            $res = json_decode(curl_exec($robinhood));
            curl_close($robinhood);

            //check if order place successfully
            if (isset($res->id)) {
                $status = $res->state;

                Positions::create([
                    'user_id' => Auth::user()->id,
                    'exchange_id' => $se->id,
                    'status' => $status,
                    'symbol' => $symbol,
                    'exchange_response' => json_encode($res),
                    'side' => $side,
                    'type' => $type,
                    'quantity' => $qty,
                    'price' => $price,
                    //'price_currency' => $priceCurrency, fix this line as priceCurrency is not defined
                    'asset_type' => 'stock'
                ]);
                $msg = 'Order is ' . $status . ' & stored in the database ';

            } else {
                $msg = $res->detail;
                return $this->error($msg);
            }
            return $this->success($msg, ['exchangeReturned' => $res]);

        } catch (\Exception $e) {
            return $this->error($e->getMessage());
        }
    }


    /**
     * Open a position on WhaleClub
     * @param $request
     * @param $se
     * @param $apiCredentials
     * @return array
     */
    private function openPositionOnWhaleclub($request, $se, $apiCredentials)
    {

        $type = [
            'limit',
            'market',
            'stop_loss'
        ];

        $direction = [
            'long',
            'short'
        ];

        // Kindly keep some validation seperate for each exchange
        // as we will keep on adding more exchanges
        // and each exchange requires seperate set of parameters needed when some specific parameters
        // are true or false
        Validator::make($request->all(),
            [
                'type' => [Rule::in($type)],
                'price' => 'required',
                'time_in_force' => 'required_if:type,limit',
                'stop_price' => 'required_if:type,market',
                'take_profit' => 'required_if:type,market',
                'entry_price' => 'required_if:type,stop_loss',
                'direction' => [Rule::in($direction)],
            ],
            [
                'type.in' => 'Supported types are ' . implode(',', $type),
                'direction.in' => 'Supported direction are ' . implode(',', $direction),

            ])->validate();

        $whaleclubUrl = "https://api.whaleclub.co/v1/";

        //get the request params        
        $qty = $request->get('quantity');
        $symbol = $request->get('symbol');
        $qty = $request->get('quantity');
        $type = $request->get('type');
        $leverage = $request->get('leverage');
        $direction = $request->get('direction');
        $stop_loss = $request->get('stop_price');
        $take_profit = $request->get('take_profit');
        $entry_price = $request->get('entry_price');
        // $ccxtSymbol = str_replace('/', '-', $symbol);
        $baseCurrency = explode("/", $symbol);
        $side = $direction;

        try {
            if (!$apiCredentials->api_key) {
                return $this->success('Whaleclub auth key has been expired.');
            }


            //get the instrument URL for specifc symbol
            $headers = array(
                'Content-Type: application/json',
                sprintf('Authorization: Bearer %s', $apiCredentials->api_key)
            );
            $whaleclub = curl_init($whaleclubUrl . "markets/");

            curl_setopt($whaleclub, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($whaleclub, CURLOPT_RETURNTRANSFER, true);

            $markets = json_decode(curl_exec($whaleclub));

            if (!property_exists($markets, $symbol)) {
                return [
                    'error' => true,
                    'msg' => 'Invalid symbol'
                ];
            }

            $headers = array(
                'Content-Type: application/json',
                sprintf('Authorization: Bearer %s', $apiCredentials->api_key),
            );
            //initialize curl
            $whaleclub = curl_init();
            curl_setopt($whaleclub, CURLOPT_URL, $whaleclubUrl . "position/new");
            curl_setopt($whaleclub, CURLOPT_HTTPHEADER, $headers);

            // set order params
            $data['direction'] = $direction;
            $data['market'] = $symbol;
            $data['leverage'] = $leverage;
            $data['size'] = $qty;
            if ($type == "market") {
                $data['stop_loss'] = $stop_loss;
                $data['take_profit'] = $take_profit;
                $price = $stop_loss;
            } elseif ($type == "stop_loss") {
                $data['entry_price'] = $entry_price;
                $price = $entry_price;
            } else {
                $data['stop_loss'] = $stop_loss;
                $data['take_profit'] = $take_profit;
                $data['entry_price'] = $entry_price;
                $price = $take_profit;
            }

            curl_setopt($whaleclub, CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($whaleclub, CURLOPT_RETURNTRANSFER, true);
            $order = json_decode(curl_exec($whaleclub));
            curl_close($whaleclub);

            // we can handle all errors here.
            // because it return error as 200 response so we can't find 
            // it in catch method. 

            if (isset($order->error)) {
                return [
                    'error' => true,
                    'msg' => $order->error->message
                ];
            }
            //check if order place successfully
            $status = $order->state;
            $exchangeOrderId = $order->id;
            $priceCurrency = $baseCurrency[0];
            $orderData = [
                'user_id' => Auth::user()->id,
                'exchange_id' => $se->id,
                'status' => $status,
                'symbol' => $symbol,
                'exchange_response' => json_encode($order),
                'side' => $side,
                'type' => $type,
                'quantity' => $qty,
                'price' => $price,
                'price_currency' => $priceCurrency,
                'asset_type' => 'crypto',
                'exchange_order_id' => $exchangeOrderId
            ];

            // we check if this order was filled
            // if yes, then it becomes a position and we store
            // it in user_positions table a.k.a Positions model

            $positionData = [];
            if (strtolower($status) == 'active') {
                $positionData = [
                    'user_id' => Auth::user()->id,
                    'exchange_id' => $se->id,
                    'asset_type' => 'crypto',
                    'symbol' => $symbol,
                    'quantity' => $qty,
                    'price' => $price,
                    'price_currency' => $priceCurrency,
                    'asset_type' => 'stock',
                    'side' => $side,
                    'exchange_position_id' => $exchangeOrderId
                ];
            }

            return [
                'error' => false,
                'orderData' => $orderData,
                // gdax returns pending for any open order
                'status' => ucfirst($status),
                'positionData' => $positionData
            ];
        } catch (\Exception $e) {
            return [
                'error' => true,
                'msg' => $this->getErrorMsgFromExchange($e, 'whaleclub')
            ];
        }


    }


    /**
     * Open a position
     * Opening a position means creating or placing an order
     * @param PositionRequest $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function openPosition(PositionRequest $request)
    {
        $exchange = strtolower($request->get('exchange_name'));

        if (!$this->checkIfExchangeSupported($exchange)) {
            return $this->error('Exchange not supported');
        }

        $se = SupportedExchanges::where('name', $exchange)->first();

        $apiCredentials = ExchangeConn::where([
            'user_id' => Auth::user()->id,
            'exchange' => $se->id
        ])->first();

        if (!$apiCredentials) {
            return $this->error('Please connect to ' . $se->name . ' first');
        }
        $order = [];
        // Kindly try to keep a separate method for each exchange
        // since we will keep on adding more exchanges.
        // It may clutter inside this method if we have too many exchanges
        switch ($exchange) {
            case 'binance':
                $order = $this->openPositionOnBinance($request, $se, $apiCredentials);
                break;
            case 'bittrex':
                $order = $this->openPositionOnBittrex($request, $se, $apiCredentials);
                break;
            case 'bitfinex':
                $order = $this->openPositionOnBitfinex($request, $se, $apiCredentials);
                break;
            case 'cryptopia':
                $order = $this->openPositionOnCryptopia($request, $se, $apiCredentials);
                break;
            case 'kucoin':
                $order = $this->openPositionOnKuCoin($request, $se, $apiCredentials);
                break;
            case 'bitmex':
                $order = $this->openPositionOnBitmex($request, $se, $apiCredentials);
                break;
            case 'gdax':
                $order = $this->openPositionGdax($request, $se, $apiCredentials);
                break;
            case 'bitstamp':
                $order = $this->openPositionOnBitstamp($request, $se, $apiCredentials);
                break;
            case 'robinhood':
                $order = $this->openPositionOnRobinhood($request, $se, $apiCredentials);
                break;
            case stripos($exchange, 'whaleclub') !== false:
                $order = $this->openPositionOnWhaleclub($request, $se, $apiCredentials);
                break;

        }

        if (empty($order)) {
            return $this->error('Something went wrong. Contact Admin');
        }
        if ($order['error']) {
            return $this->error($order['msg']);
        } else {
            $msg = 'Order sent and status is ' . $order['status'];
            Orders::create($order['orderData']);

            if (!empty($order['positionData'])) {
                // see if there is already a position on this exchange,
                // if yes, then add or subtract
                $position = Positions::where([
                    'exchange_id' => $se->id,
                    'symbol' => $order['positionData']['symbol'],
                    'user_id' => Auth::user()->id
                ])->first();
                $side = strtolower($request->get('side'));
                if ($position) {
                    $qty = $order['positionData']['quantity'];
                    if ($side == 'buy') {
                        $position->quantity = $position->quantity + (float)$qty;
                    } elseif ($side == 'sell') {
                        $position->quantity = $position->quantity - (float)$qty;
                    }
                    $position->save();
                } else {
                    if ($side == 'sell') {
                        $order['positionData']['quantity'] = -(float)$order['positionData']['quantity'];
                    }
                    Positions::create($order['positionData']);
                }
            }

            return $this->success($msg);
        }
    }

    // Function to return openPositions, need to get the complete work done.
    // Open to discuss the displacement of code
    public function getOpenPositions()
    {
        $exchanges_connected = DB::table('exchange_conn as e')
            ->join('supported_exchanges as se', 'se.id', '=', 'e.exchange')
            ->select(['se.name', 'se.id', 'e.api_key', 'e.api_secret'])
            ->where([
                'e.user_id' => Auth::user()->id,
                'deleted_at' => null
            ])
            ->get();
        if ($exchanges_connected) {
            $openPositions = [];
            foreach ($exchanges_connected as $exch) {

                switch (strtolower($exch->name)) {

                    case 'bittrex':
                        $bittrex = new ccxt\bittrex();
                        $bittrex->apiKey = $exch->api_key;
                        $bittrex->secret = $exch->api_secret;

                        // $api = $exchange_conn->api;
                        $balances = $bittrex->accountGetBalances();
                        foreach ($balances['result'] as $bal) {
                            if ($bal['Balance'] > 0) {
                                $matchingSymbol = ExchangeMarkets::whereIn('quote', ['BTC', 'ETH', 'USDT'])->where([
                                    'base' => $bal['Currency'],
                                    'exchange_name' => $exch->name
                                ])->orderBy('quote')->first();
                                if (!$matchingSymbol) {
                                    continue;
                                }
                                $asset['symbol'] = $matchingSymbol['symbol'];
                                $asset['base'] = $bal['Currency'];
                                $asset['size'] = $bal['Balance'];
                                $asset['exchange'] = $exch->name;
                                $openPositions[] = $asset;
                            }
                        }
                        break;

                    case 'binance':
                        $binance = new ccxt\binance();
                        $binance->apiKey = $exch->api_key;
                        $binance->secret = $exch->api_secret;

                        $balances = $binance->fetch_balance();
                        foreach ($balances['info']['balances'] as $bal) {
                            if ($bal['free'] * 1.0 != 0.0) {
                                $matchingSymbol = ExchangeMarkets::whereIn('quote', ['BTC', 'ETH', 'USDT'])->where([
                                    'base' => $bal['asset'],
                                    'exchange_name' => $exch->name
                                ])->orderBy('quote')->first();
                                if (!$matchingSymbol) {
                                    continue;
                                }
                                $asset['symbol'] = $matchingSymbol['symbol'];
                                $asset['base'] = $bal['asset'];
                                $asset['size'] = $bal['free'];
                                $asset['exchange'] = $exch->name;
                                $openPositions[] = $asset;
                            }
                        }

                        break;
                }
                unset($asset);
            }
            return $this->success($openPositions);
        } else {
            return $this->error('No exchanges connected');
        }
    }
}
