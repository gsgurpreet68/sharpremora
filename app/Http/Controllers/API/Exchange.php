<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\DisconnectExchange;
use App\Http\Requests\ExchangeCapabilities;
use App\Http\Requests\GetBalance;
use App\Http\Requests\GetMarket;
use App\Model\ExchangeCapability;
use App\Model\Orders;
use App\Model\ExchangeAuth;
use ccxt;
use Carbon\Carbon;
use App\Model\ExchangeConn;
use App\Model\SupportedExchanges;
use App\Http\Requests\ConnectToExchange;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\BaseApiController;
use App\Model\ExchangeMarkets;

/**
 * @resource Exchange
 * Connect to an exchange, disconnect,
 * get list of connected exchanges for a user,
 * get currently supported exchanges by RemoraHub
 */
class Exchange extends BaseApiController
{
    // only for testing
    // nothing serious about it
    // kindly ignore it
    public function test()
    {
        $exchangeConn = ExchangeConn::where('exchange', 21)->first();
        $key = $exchangeConn->api_key;
        $secret = $exchangeConn->api_secret;
        $exchange = new ccxt\bittrex();
        $exchange->apiKey = $key;
        $exchange->secret = $secret;
        $exchange->password = $exchangeConn->api_password;
        return response()->json(
            [
                //'api' => $exchange->api,
                'order' => $exchange->fetchOrder('314dd229-440f-468d-aaba-3a158c44bfad'),
                //'markets' => $exchange->fetchMarkets(),
                //'currentprice' => $exchange->fetchTicker('ADT/BTC'),
                //'price' => $exchange->fetchTicker('LTC/USD'),
            ]
        );
    }

    /**
     * Connected to an exchange and store that in the database
     * @param $exchange
     * @param $apiKey
     * @param $apiSecret
     * @param array $userObjObtained - user obj that is returned after connecting to an exchange
     * @param $uid -  uid needed for some exchange
     * @param $password -  password needed for some exchange
     * @return mixed
     */
    private function storeConnection($exchange, $apiKey, $apiSecret, $userObjObtained = [], $uid, $password)
    {
        $se = SupportedExchanges::where('name', $exchange)->first();

        if (empty($se)) {
            return $this->error('Exchange not found');
        }

        $data = [
            'exchange' => $se->id,
            'api_key' => $apiKey,
            'api_secret' => $apiSecret,
            'api_password' => $password,
            'user_id' => \Auth::user()->id,
            'api_uid' => $uid,
        ];

        if (!empty($userObjObtained)) {
            if (gettype($userObjObtained) == 'object') {
                $data['permission_obj_obtained'] = $userObjObtained;
            } elseif (gettype($userObjObtained) == 'array') {
                $data['permission_obj_obtained'] = json_encode($userObjObtained);
            }
        }
        try {
            ExchangeConn::create($data);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Connect to binance
     * @param $request
     * @param $apiKey
     * @param $apiSecret
     * @return array
     * @throws ccxt\ExchangeError
     */
    private function connectToBinance($request, $apiKey, $apiSecret)
    {
        $binance = new ccxt\binance();
        $binance->apiKey = $apiKey;
        $binance->secret = $apiSecret;
        $permissions = $binance->privateGetAccount();
        // there's no way to get withdraw permission for binance
        // that's why we keeping withdraw as false
        // withdraw has to be false for any exchange in order to work
        return [
            'withdraw' => false,
            'permissions' => $permissions
        ];
    }

    /**
     * Connect to bitmex
     * @param $request
     * @param $apiKey
     * @param $apiSecret
     * @return \Illuminate\Http\JsonResponse | array
     * @throws ccxt\ExchangeError
     */
    private function connectToBitmex($request, $apiKey, $apiSecret)
    {
        $bitmex = new ccxt\bitmex();
        $bitmex->apiKey = $apiKey;
        $bitmex->secret = $apiSecret;

        $permissions = $bitmex->privateGetApiKey();
        $permissions = $permissions[0];// it returns an array with one object inside
        $exchangePermissions = (object)$permissions;
        $exchangePermissions = $exchangePermissions->permissions;
        $lowerCase = function ($value) {
            return strtolower($value);
        };
        $exchangePermissions = array_map($lowerCase, $exchangePermissions);
        $withdraw = in_array('withdraw', $exchangePermissions);

        if ($withdraw) {
            return [
                'withdraw' => true,
                'permissions' => $exchangePermissions
            ];
        }

        // fetch all the pending orders and store that
        // our database
        $pendingOrders = $bitmex->privateGetOrder([
            'filter' => [
                'open' => true
            ]
        ]);

        if (count($pendingOrders)) {
            $bitmexExchange = SupportedExchanges::where('name', 'bitmex')->first();
            $orderData = [];
            $now = Carbon::now()->format('Y-m-d H:i:s');
            foreach ($pendingOrders as $pendingOrder) {
                $orderData[] = [
                    'user_id' => Auth::user()->id,
                    'exchange_id' => $bitmexExchange->id,
                    'exchange_response' => json_encode($pendingOrder),
                    'symbol' => $pendingOrder['symbol'],
                    'status' => $pendingOrder['ordStatus'],
                    'side' => strtolower($pendingOrder['side']),
                    'type' => $pendingOrder['ordType'],
                    'quantity' => $pendingOrder['orderQty'],
                    'price' => $pendingOrder['price'],
                    'asset_type' => 'future', // you trade futures on Bitmex
                    'exchange_order_id' => $pendingOrder['orderID'],
                    'created_at' => $now,
                    'updated_at' => $now
                ];
            }

            Orders::insert($orderData);
        }

        return [
            'withdraw' => $withdraw,
            'permissions' => $permissions
        ];
    }

    /**
     * Connect to bitfinex
     * @param $request
     * @param $apiKey
     * @param $apiSecret
     * @return \Illuminate\Http\JsonResponse | array
     * @throws ccxt\ExchangeError
     */
    private function connectToBitfinex($request, $apiKey, $apiSecret)
    {
        $bitfinex = new ccxt\bitfinex();
        $bitfinex->apiKey = $apiKey;
        $bitfinex->secret = $apiSecret;
        // having trouble with camelcase
        // used unserscore as an exception
        $permissions = $bitfinex->private_post_key_info();
        $exchangePermissions = (object)$permissions;
        $withdraw = $exchangePermissions->withdraw['write'];

        return [
            'withdraw' => $withdraw,
            'permissions' => $permissions
        ];
    }

    /**
     * Connect to bittrex
     * @param $request
     * @param $apiKey
     * @param $apiSecret
     * @return array
     */
    private function connectToBittrex($request, $apiKey, $apiSecret)
    {
        $bittrex = new ccxt\bittrex();
        $bittrex->apiKey = $apiKey;
        $bittrex->secret = $apiSecret;
        //No way to get withdraw permission on bittrex
        //so we initiate a withdraw req
        // if that's successful, then it's failed
        // if that fails, that means permission disbaled and we connect
        $balance = $bittrex->fetchBalance();
        $balance = $balance['info'];
        $foundBtcAt = array_search('BTC', array_column($balance, 'Currency'));

        if ($foundBtcAt === false) {
            return [
                'error' => true
            ];
        }
        $bitcoin = $balance[$foundBtcAt];
        $address = $bitcoin['CryptoAddress'];

        try {
            $canWithdraw = true;
            $withdraw = $bittrex->withdraw('BTC', 0.0001, $address);
        } catch (\Exception $e) {
            $canWithdraw = false;
        }

        return [
            'withdraw' => $canWithdraw,
            'error' => false,
            'permissions' => [] // since we didn't fetch any user info
        ];
    }

    /**
     * connect to cryptopia exchange
     * @param $request
     * @param $apiKey
     * @param $apiSecret
     * @return array
     */
    private function connectToCryptopia($request, $apiKey, $apiSecret)
    {
        $cryptopia = new ccxt\cryptopia();
        $cryptopia->apiKey = $apiKey;
        $cryptopia->secret = $apiSecret;

        try {
            $canWithdraw = false;
            $permissions = $cryptopia->privatePostGetBalance();
        } catch (\Exception $e) {
            $canWithdraw = false;
        }

        return [
            'withdraw' => $canWithdraw,
            'permissions' => [] // to be done
        ];
    }

    /**
     * Connect to KuCoin
     * @param $request
     * @param $apiKey
     * @param $apiSecret
     * @return array
     * @throws ccxt\ExchangeError
     */
    private function connectToKuCoin($request, $apiKey, $apiSecret)
    {
        $kuCoin = new ccxt\kucoin([
            'apiKey' => $apiKey,
            'secret' => $apiSecret
        ]);

        // there's no way to get withdrawl permissions using api
        // for kuCoin as that's a work in progress from their side
        // we try to withdraw 0.001 BTC and if that's successful,
        // that means that the user has withdraw enabled and we refuse to connect
        // if that fails, that means that the withdraw is disabled and we can procceed
        // to connect

        // On KuCoin, we can check if the account is suspended or not.
        // If not suspended, then we connect it
        $userInfo = $kuCoin->privateGetUserInfo();
        $isAccountSuspend = $userInfo['data']['isSuspend'];

        // get the wallet address for BTC
        // this method is different across exchanges. So, don't copy it
        $walletAddress = $kuCoin->privateGetAccountCoinWalletAddress(['coin' => 'BTC']);
        $walletAddress = $walletAddress['data']['address'];

        try {
            $withdrawl = $kuCoin->withdraw('BTC', 0.0001, $walletAddress);
            $canWithdraw = true;
        } catch (\Exception $e) {
            $canWithdraw = false;
        }

        return [
            'withdraw' => $canWithdraw,
            'permissions' => $userInfo,
            'isAccountSuspend' => $isAccountSuspend
        ];
    }


    /**
     * Connect to Gdax
     * @param $request
     * @param $apiKey
     * @param $apiSecret
     * @return array
     * @throws ccxt\ExchangeError
     */
    private function connectToGdax($request, $apiKey, $apiSecret)
    {
        $password = $request->get('extra_1');
        $gdax = new ccxt\gdax([
            'apiKey' => $apiKey,
            'secret' => $apiSecret,
            'password' => $password
        ]);

        //get only Coinbase account
        $coinbaseAccounts = $gdax->privateGetCoinbaseAccounts();
        $accountInfo = $gdax->privateGetAccounts();

        $data = array();
        $coinbaseAccountId = $coinbaseAccounts[0]['id'];
        $data['amount'] = 0.002;
        $data['currency'] = $coinbaseAccounts[0]['currency'];
        $data['coinbase_account_id'] = $coinbaseAccountId;

        try {
            //perform withdraw operation
            $withdraw = $gdax->privatePostWithdrawalsCoinbase($data);
            $canWithdraw = true;
        } catch (\Exception $e) {
            $canWithdraw = false;
        }

        return [
            'withdraw' => $canWithdraw,
            'permissions' => $accountInfo,
        ];

    }


    /**
     * Connect to BitStamp
     * @param $request
     * @param $apiKey
     * @param $apiSecret
     * @return array
     * @throws ccxt\ExchangeError
     */
    private function ConnectToBitStamp($request, $apiKey, $apiSecret)
    {
        $uid = $request->get('extra_1');
        $bitstamp = new ccxt\bitstamp([
            'apiKey' => $apiKey,
            'secret' => $apiSecret,
            'uid' => $uid
        ]);
        $nonce = $this->getCurrentMicroSecs();
        $signature = hash_hmac('sha256', $nonce . $uid . $apiKey, $apiSecret);

        // no way to get if withdraw permission is enabled or not
        // we try to initiate a small withdraw request to see
        // if it worked or not. If worked, then we refuse to connect
        // if failed, then we connect
        //$bchAddress = $bitstamp->privatePostBchAddress();
        //$bchAddress = $bchAddress['address'];
        $balance = $bitstamp->privatePostBalance([
            //'key' => $apiKey,
            //'signature' => $signature,
            'nonce' => $this->getCurrentMicroSecs()
        ]);
        return $balance;
        /*try {
            $withdraw = $bitstamp->privatePostBchWithdrawal([
                'amount' => 0.0001,
                'address' => $bchAddress
            ]);
        } catch (\Exception $e) {
            $withdraw = false;
        }*/

        return [
            //'withdraw' => $withdraw,
            'permissions' => [] // no way to get user info
        ];
    }


    /**
     * Connect to Robinhood
     * @param $request
     * @param $apiKey
     * @param $apiSecret
     * @return
     */
    private function connectToRobinhood($request, $apiKey, $apiSecret)
    {
        $robinhoodUrl = "https://api.robinhood.com/";
        try {
            //initialize curl
            $robinhood = curl_init();
            curl_setopt($robinhood, CURLOPT_URL, $robinhoodUrl . "api-token-auth/");
            curl_setopt($robinhood, CURLOPT_POST, 1);

            //set credentials
            curl_setopt($robinhood, CURLOPT_POSTFIELDS,
                "username=" . $apiKey . "&password=" . $apiSecret);
            curl_setopt($robinhood, CURLOPT_RETURNTRANSFER, true);
            $res = curl_exec($robinhood);
            curl_close($robinhood);

            // get the auth token
            $data = json_decode($res);

            if (isset($data->token)) {
                $token = $data->token;
            } else {
                $msg = $data->non_field_errors[0];
                return [
                    'withdraw' => false,
                    'unauthorized' => true,
                    'permissions' => $data,
                    'msg' => $msg
                ];
            }

            $exchange = strtolower($request->get('exchange_name'));
            $se = SupportedExchanges::where('name', $exchange)->first();

            $data = [
                'exchange_id' => $se->id,
                'auth_token' => $token,
                'user_id' => \Auth::user()->id,
                'active' => true
            ];
            //save token in database
            ExchangeAuth::create($data);
            $withdraw = false;
        } catch (\Exception $e) {
            $withdraw = false;
        }

        return [
            'withdraw' => $withdraw,
            'permissions' => '',
        ];

    }


    /**
     * Connect to Whaleclub
     * @param $request
     * @param $apiKey
     * @param $apiSecret
     * @return
     */
    private function connectToWhaleclub($request, $apiKey)
    {


        $whaleclubUrl = "https://api.whaleclub.co/v1/";
        try {
            //initialize curl
            $headers = array(
                'Content-Type: application/json',
                sprintf('Authorization: Bearer %s', $apiKey)
            );
            $whaleclub = curl_init($whaleclubUrl . "balance");

            curl_setopt($whaleclub, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($whaleclub, CURLOPT_RETURNTRANSFER, true);

            $balance = json_decode(curl_exec($whaleclub));
            curl_close($whaleclub);


            if (isset($balance->error)) {
                return [
                    'withdraw' => false,
                    'unauthorized' => $balance->error,
                    'permissions' => $balance,
                    'msg' => $balance->error->message
                ];
            }


            $exchange = strtolower($request->get('exchange_name'));
            $se = SupportedExchanges::where('name', $exchange)->first();
            // Whaleclub does't allow withdraw using API
            $withdraw = false;
        } catch (\Exception $e) {
            $withdraw = false;
        }

        return [
            'withdraw' => $withdraw,
            'permissions' => [],
        ];

    }


    /**
     * Connect to Whaleclub
     * @param $request
     * @param $apiKey
     * @param $apiSecret
     * @return
     */
    private function connectToTradeIt($request, $apiKey)
    {


        $tradeItUrl = "https://ems.qa.tradingticket.com/api/v2/";
        //try {
        //initialize curl
        $headers = array(
            'Content-Type: application/json',
            // sprintf('Authorization: Bearer %s', $apiKey)
        );
        $tradeIt = curl_init($tradeItUrl . "user/getOAuthLoginPopupUrlForWebApp");

        curl_setopt($tradeIt, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($tradeIt, CURLOPT_RETURNTRANSFER, true);
        $data['apiKey'] = $apiKey;
        $data['broker'] = "Dummys";

        curl_setopt($tradeIt, CURLOPT_POSTFIELDS, $data);
        curl_setopt($tradeIt, CURLOPT_RETURNTRANSFER, true);
        $res = json_decode(curl_exec($tradeIt));
        print_r($res);
        exit;

        // } catch (\Exception $e) {
        //     $withdraw = false;
        // }

        // return [
        //     'withdraw' => $withdraw,
        //     'permissions' => [],
        // ];

    }




    /**
     * Connect to Whaleclub
     * @param $request
     * @param $apiKey
     * @param $apiSecret
     * @return
     */
    private function connectToZerodha($request, $apiKey, $apiSecret)
    {
        // get the 
        $request_token = $request->get('request_token');
        // remove it when we use redirect method
        $request_token = "8PQDgKQFWc4oziRLd1G53NgP56Wi1X8y";
        $zerodhaUrl = "https://api.kite.trade//session/token";

        try {
            
            $headers = array(
                'Content-Type: application/json',
                 sprintf('Authorization: token '.$apiKey.':'.$apiSecret)
            );
            
            // need checksum hask key in params
            $checksum = hash("sha256", $apiKey.$request_token.$apiSecret);

            $params = [
                "api_key" => $apiKey,
                "request_token" => $request_token,
                "checksum" => $checksum
            ];

            $ch = curl_init();
            $payload = null;
            if($payload = http_build_query($params && is_array($params) ? $params : [])) 
            {
                $payload = preg_replace("/%5B(\d+?)%5D/", "", $payload);
            }

            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_URL, $zerodhaUrl);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
            $result = json_decode(curl_exec($ch));
            if (isset($result->status) && $result->status == 'error') {
                return [
                    'withdraw' => false,
                    'unauthorized' => $result->message,
                    'permissions' => '',
                    'msg' => $result->message
                ];
            }
            exit;
        
        } catch (\Exception $e) {
            $withdraw = false;
        }

        return [
            'withdraw' => false,
            'permissions' => [],
        ];

    }

    /**
     * Connect to an exchange
     * `extra_1 param` is supplied with `uid` when exchange is Bitstamp
     * and with `password` when exchange is Gdax.
     * When editing or revalidating a conn, send `revalidate` as `1`
     * and send all the params sent as usual.
     * @param ConnectToExchange $request
     * @return mixed
     */
    public function connect(ConnectToExchange $request)
    {
        //verify if the user has enabled the right permission or now
        //Make sure that user doesn't have withdraw permission
        $conn = null;
        $password = null;
        $uid = null;
        $exchange = strtolower($request->get('exchange_name'));
        $apiKey = $request->get('api_key');
        $apiSecret = $request->get('api_secret');
        $revalidate = $request->get('revalidate');
        if (false && !$this->checkIfExchangeSupported($exchange)) {
            return $this->error('Exchange not supported');
        }
      //  $conn = $this->connectToZerodha($request, $apiKey, $apiSecret);
        try {
            $se = SupportedExchanges::where('name', $exchange)->first();

            $exchangeConn = ExchangeConn::where([
                'user_id' => \Auth::user()->id,
                'exchange' => $se->id
            ])->with('exchange')->first();

            if ($exchangeConn && !$revalidate) {
                return $this->error('Already connected to ' . ucfirst($exchange));
            }
            switch (strtolower($exchange)) {
                case 'binance':
                    $conn = $this->connectToBinance($request, $apiKey, $apiSecret);
                    //$conn['withdraw'] = $conn['permissions']['canWithdraw'];
                    break;
                case 'bitmex':
                    $conn = $this->connectToBitmex($request, $apiKey, $apiSecret);
                    break;
                case 'bitfinex':
                    $conn = $this->connectToBitfinex($request, $apiKey, $apiSecret);
                    break;
                case 'bittrex':
                    $conn = $this->connectToBittrex($request, $apiKey, $apiSecret);
                    if ($conn['error']) {
                        return $this->error('Something went wrong. Contact Admin');
                    }
                    break;
                case 'cryptopia':
                    $conn = $this->connectToCryptopia($request, $apiKey, $apiSecret);
                    break;
                case 'kucoin':
                    $conn = $this->connectToKuCoin($request, $apiKey, $apiSecret);
                    if ($conn['isAccountSuspend']) {
                        return $this->error('Your this account on KuCoin is suspended. Kindly try with another account');
                    }
                    break;
                case 'gdax':
                    $conn = $this->connectToGdax($request, $apiKey, $apiSecret);
                    $password = $request->get('extra_1');
                    break;
                case 'bitstamp':
                    $conn = $this->connectToBitstamp($request, $apiKey, $apiSecret);
                    $uid = $request->get('extra_1');
                    break;
                case 'robinhood':
                    $conn = $this->connectToRobinhood($request, $apiKey, $apiSecret);
                    break;
                case 'whaleclub':
                    $apiSecret = "not required";
                    $conn = $this->connectToWhaleclub($request, $apiKey);
                    break;
                case 'tradeit':
                    $apiSecret = "not required";
                    $conn = $this->connectToTradeIt($request, $apiKey);
                    break;
                case 'zerodha':
                    $conn = $this->connectToZerodha($request, $apiKey, $apiSecret);
                    break;    

            }

            if (empty($conn)) {
                return $this->error('Something went wrong. Contact admin.');
            }
            $permissions = $conn['permissions'];

            if (isset($conn['unauthorized'])) {
                return $this->error($conn['msg'],
                    [], 401, ['exchangeResponse' => $permissions]);
            }

            if ($conn['withdraw']) {
                return $this->error('Withdraw permission enabled. Kindly disable that',
                    [], 501, ['exchangeResponse' => $permissions]);
            }

            if ($revalidate) {
                $exchangeConn->api_key = $apiKey;
                $exchangeConn->api_secret = $apiSecret;
                $exchangeConn->api_uid = $uid;
                $exchangeConn->api_password = $password;
                $exchangeConn->save();
            } else {
                $connStored = $this->storeConnection($exchange, $apiKey, $apiSecret, $permissions, $uid, $password);
                if (empty($connStored)) {
                    return $this->error('Could not save connection. Please contact support.');
                }
            }

            return $this->success('Connected to exchange and connection saved', [
                'exchangeReturnedPermissions' => $permissions
            ]);

        } catch (\Exception $e) {
            $exchangeError = [
                'exchangeError' => $e->getMessage()
            ];
            return $this->error('Could not connect to ' . ucfirst($exchange),
                [], 200, $exchangeError);
        }
    }

    /**
     * Connected exchanges
     * Exchanges where the user is currently connected to using Sharpremora
     * @return \Illuminate\Http\JsonResponse
     */
    public function connectedExchanges()
    {
        $connExchanges = DB::table('exchange_conn as e')
            ->join('supported_exchanges as se', 'se.id', '=', 'e.exchange')
            ->select(['se.name', 'se.id', 'se.exchange_supported_order_types'])
            ->where([
                'e.user_id' => Auth::user()->id,
                'deleted_at' => null
            ])
            ->get();

        return $this->success('Retrieved exchanges', $connExchanges);
    }

    /**
     * Disconnect an exchange
     * @param DisconnectExchange $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function disconnectExchange(DisconnectExchange $request)
    {
        $exchangeId = $request->get('exchange_id');
        $exchange = ExchangeConn::where([
            'exchange' => $exchangeId,
            'user_id' => Auth::user()->id
        ])->first();

        if ($exchange) {
            $exchange->delete();
            return $this->success('Disconnected from the exchange');
        } else {
            return $this->error('Not connected to the exchange');
        }
    }

    /**
     * Supported exchanges
     * The list of exchanges that are currently supported by Sharpremora
     * @return \Illuminate\Http\JsonResponse
     */
    public function supportedExchanges()
    {
        $se = SupportedExchanges::select('id', 'name')
            ->where('active', true)->get();

        return $this->success('Retrieved supported exchanges', $se);
    }

    /**
     * Get all the markets from an exchange
     * @param GetMarket $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMarkets(GetMarket $request)
    {
        $exch = strtolower($request->get('exchange_name'));
        $marketName = $request->get('market_name');
        if (!$this->checkIfExchangeSupported($exch)) {
            return $this->error('Exchange not found');
        }
        return $this->success('Retrieved markets',
            ExchangeMarkets::getMarkets($exch, $marketName));
    }

    /**
     * Exchange capabilities
     * Returns what types of orders supported by an exchange
     * @param ExchangeCapabilities $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function exchangeCapabilities(ExchangeCapabilities $request)
    {
        $exchangeName = $request->get('exchange_name');
        $exchangeName = strtolower($exchangeName);

        $capabilities = ExchangeCapability::where('exchange_name', $exchangeName)->first();
        return $this->success('Retrieved capabilities', $capabilities);
    }

    /**
     * Get balance.
     * Send `asset` as an array.
     * E.g send `asset` as `['btc', 'eth']` to get balance for BTC and ETH for that exchange.
     * If you don't specify any asset, it will send balance for all the assets of that
     * exchange.
     * @response {
     *  "success": true,
     *  "msg": "Fetched Balance",
     *  "data" : [
     *      {"asset": "BTC", "free": "0.1", "used" : "0.02", "total": "0.12"},
     *      {"asset": "ETH", "free": "0.1", "used" : "0.02", "total": "0.12"}
     *  ]
     * }
     * @param GetBalance $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBalance(GetBalance $request)
    {
        $exchangeName = strtolower($request->get('exchange_name'));
        $asset = $request->get('asset');
        $se = SupportedExchanges::where([
            'name' => $exchangeName
        ])->first();
        // check if already connected to this exchange
        $conn = $this->connectedToExchange($se->id);
        if (!$conn) {
            return $this->error('Please connect to ' . ucfirst($se->name) . ' first');
        }
        $ccxtClassName = '\\ccxt\\' . $exchangeName;
        $exchange = new $ccxtClassName();
        $exchange->apiKey = $conn['apiKey'];
        $exchange->secret = $conn['apiSecret'];
        if ($exchangeName == 'bitstamp') {
            $exchange->uid = $conn['apiUid'];
        }
        if ($exchangeName == 'gdax') {
            $exchange->password = $conn['apiPassword'];
        }
        $excludedKeys = ['free', 'used', 'total', 'info'];
        try {
            if ($exchangeName == 'binance') {
                // on binance, using the ccxt method can create problem
                // on slow network, that's why exchange method with
                // with 1 sec extra time window
                $binanceBal = $exchange->privateGetAccount([
                    'recvWindow' => 15000
                ]);
                $binanceBal = $binanceBal['balances'];
                $balance = [];
                foreach ($binanceBal as $item) {
                    $balance[$item['asset']] = [
                        'free' => $item['free'],
                        'used' => $item['locked'],
                        'total' => $item['free'] + $item['locked']
                    ];
                }
                /*$balance = $exchange->fetchBalance([
                    'recvWindow' => 15000
                ]);*/
            } else {
                $balance = $exchange->fetchBalance();
            }
            $balanceArr = [];
            foreach ($balance as $key => $assetBal) {
                if ($asset) {
                    foreach ($asset as $item) {
                        if (strtoupper($item) == strtoupper($key)) {
                            $balanceArr[] = [
                                'asset' => strtoupper($key),
                                'free' => $assetBal['free'],
                                'used' => $assetBal['used'],
                                'total' => $assetBal['total']
                            ];
                        }
                    }
                } else {
                    if (!in_array(strtolower($key), $excludedKeys) && (float)$assetBal['total'] > 0) {
                        $balanceArr[] = [
                            'asset' => strtoupper($key),
                            'free' => $assetBal['free'],
                            'used' => $assetBal['used'],
                            'total' => $assetBal['total']
                        ];
                    }
                }
            }

            return $this->success('Fetched balance', $balanceArr);
        } catch (\Exception $e) {
            return $this->error($e->getMessage());
        }
    }
}
