<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\BaseApiController;
use App\Http\Requests\CancelOrder;
use App\Http\Requests\FetchOpenOrders;
use App\Http\Requests\GetOrders;
use App\Model\ExchangeConn;
use App\Model\OrderCancel;
use App\Model\Orders;
use App\Model\SupportedExchanges;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use ccxt;
use Illuminate\Support\Facades\DB;

/**
 * @resource Fetch Orders
 * Fetch Open orders on various exchanges, fetch open orders
 * of various exchanges from our database
 */
class FetchOrders extends BaseApiController
{
    /**
     * Get orders
     * Send `status` as `all` to get orders of any status.
     * Get `open`, `cancelled`, `filled` or
     * all kinds of orders on all exchange or a
     * particular exchange from our database.
     * To filter orders in ascending or descending order
     * of order time, send `order_time_flow` as `asc` or `desc`.
     * Send `from_date` and `to_date` in `YYYY-MM-DD` format from front end
     * a.k.a `UTC` format
     * @param GetOrders $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOrders(GetOrders $request)
    {
        $whereArr = [
            'uo.user_id' => Auth::user()->id
        ];
        $status = strtolower($request->get('status'));
        $exchangeName = strtolower($request->get('exchange_name'));
        if ($exchangeName) {
            if (!$this->checkIfExchangeSupported($exchangeName)) {
                return $this->error('Exchange not found');
            }
            $whereArr['se.name'] = $exchangeName;
        }
        $orders = DB::table('user_orders as uo')
            ->join('supported_exchanges as se', 'se.id', '=', 'uo.exchange_id')
            ->select('uo.id as order_id', 'uo.exchange_order_id', 'uo.symbol',
                'uo.quantity as size',
                DB::raw("INITCAP (uo.status) as status, INITCAP (uo.side) as side,
                INITCAP (uo.type) as type, INITCAP (se.name) as exchange_name, 
                to_char(uo.created_at, 'Mon DD, YYYY HH24:MI') || ' UTC' as ordered_at, 
                COUNT(*) OVER() as total_orders"),
                'uo.quantity', 'uo.price',
                'uo.price_currency', 'uo.asset_type')
            ->where($whereArr);

        $fromDate = $request->get('from_date');
        $toDate = $request->get('to_date');

        if ($fromDate && $toDate) {
            $orders = $orders->where(DB::raw('uo.created_at::date'), '>=', $fromDate)
                ->where(DB::raw("uo.created_at::date"), '<=', $toDate);
        }

        if ($status != 'all') {
            $orders = $orders->where('uo.status', 'ilike', $status);
        }

        $page = $request->get('page');
        if ($page) {
            $page = (int)$page;
            $orders = $orders->take(10)
                ->skip(($page - 1) * 10);
        } else {
            $orders = $orders->take(10)
                ->skip(0);
        }

        //return $orders->toSql();
        $orderTimeFlow = $request->get('order_time_flow');
        if ($orderTimeFlow) {
            $orders = $orders->orderBy('uo.created_at', $orderTimeFlow)
                ->get();
        } else {
            $orders = $orders->orderBy('uo.created_at', 'desc')
                ->get();
        }

        if (count($orders)) {
            $pages = ceil($orders[0]->total_orders);
            $pages = ceil($pages / 10);
        } else {
            $pages = 0;
        }

        return $this->success('Retrieved orders', $orders, 200, [
            'pageCount' => $pages
        ]);
    }

    /**
     * Cancel an order
     * Cancel an order on any exchange
     * @param CancelOrder $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancelOrder(CancelOrder $request)
    {
        $id = $request->get('order_id');
        $order = Orders::find($id);
        $user = Auth::user();
        if (!$user->can('cancel', $order)) {
            return $this->error('You can\'t cancel this order');
        }
        $se = SupportedExchanges::find($order->exchange_id);
        // check if already connected to this exchange
        $conn = $this->connectedToExchange($se->id);
        if (!$conn) {
            return $this->error('Please connect to ' . ucfirst($se->name) . ' first');
        }
        $exchangeName = strtolower($se->name);
        if ($exchangeName == 'whaleclub' || $exchangeName == 'tradeit' || $exchangeName == 'trade it') {
            return $this->error('Can not cancel order on ' . $exchangeName);
        }
        $ccxtClassName = '\\ccxt\\' . $exchangeName;
        $exchange = new $ccxtClassName();
        $exchange->apiKey = $conn['apiKey'];
        $exchange->secret = $conn['apiSecret'];
        if ($exchangeName == 'bitstamp') {
            $exchange->uid = $conn['apiUid'];
        }
        if ($exchangeName == 'gdax') {
            $exchange->password = $conn['apiPassword'];
        }
        try {
            $orderCancelled = $exchange->cancelOrder($order->exchange_order_id);
            DB::beginTransaction();
            $now = Carbon::now()->format('Y-m-d H:i:s');
            OrderCancel::insert([
                'user_id' => $user->id,
                'user_orders_id' => $id,
                'exchange_response' => json_encode($orderCancelled),
                'created_at' => $now,
                'updated_at' => $now
            ]);
            $order->status = 'cancelled';
            $order->save();
            DB::commit();
            return $this->success('Order cancelled');
        } catch (\Exception $e) {
            try {
                $msg = $this->error($this->getErrorMsgFromExchange($e, $exchangeName));
                return $this->success($msg);
            } catch (\Exception $e) {
                return $this->error('Something went wrong.');
            }
        }
    }

    /**
     * Fetch open orders on Bitmex
     * @param $supportedExchange
     * @param $apiCredentials
     * @return array
     * @throws ccxt\ExchangeError
     */
    private function fetchOpenOrdersBitmex($supportedExchange, $apiCredentials)
    {
        $bitmex = new ccxt\bitmex([
            'apiKey' => $apiCredentials->api_key,
            'secret' => $apiCredentials->api_secret
        ]);

        $pendingOrders = $bitmex->privateGetOrder([
            'filter' => [
                'open' => true
            ]
        ]);
        $orderData = [];
        $now = Carbon::now()->format('Y-m-d H:i:s');
        foreach ($pendingOrders as $pendingOrder) {
            $orderData[] = [
                'user_id' => Auth::user()->id,
                'exchange_id' => $supportedExchange->id,
                'exchange_response' => json_encode($pendingOrder),
                'symbol' => $pendingOrder['symbol'],
                'status' => $pendingOrder['ordStatus'],
                'side' => strtolower($pendingOrder['side']),
                'type' => $pendingOrder['ordType'],
                'quantity' => $pendingOrder['orderQty'],
                'price' => $pendingOrder['price'],
                'asset_type' => 'future', // you trade futures on Bitmex
                'exchange_order_id' => $pendingOrder['orderID'],
                'created_at' => $now,
                'updated_at' => $now
            ];
        }

        return [
            'error' => false,
            'openOrders' => $orderData
        ];
    }

    /**
     * Fetch open orders on Bittrex
     * @param $supportedExchange
     * @param $apiCredentials
     * @return array
     * @throws ccxt\ExchangeError
     */
    private function fetchOpenOrdersBittrex($supportedExchange, $apiCredentials)
    {
        $bittrex = new ccxt\bittrex([
            'apiKey' => $apiCredentials->api_key,
            'secret' => $apiCredentials->api_secret
        ]);

        $pendingOrders = $bittrex->marketGetOpenorders();
        $pendingOrders = $pendingOrders['result'];
        $orderData = [];
        $now = Carbon::now()->format('Y-m-d H:i:s');

        foreach ($pendingOrders as $pendingOrder) {
            $limitOrder = preg_match('/limit/i', $pendingOrder['OrderType']);
            preg_match('/(buy|sell)/i', $pendingOrder['OrderType'], $side);
            $orderData[] = [
                'user_id' => Auth::user()->id,
                'exchange_id' => $supportedExchange->id,
                'exchange_response' => json_encode($pendingOrder),
                'symbol' => $pendingOrder['Exchange'], // bittrex sends symbol under exchange
                'status' => 'open',
                'side' => strtolower($side[0]),
                'type' => $limitOrder ? 'limit' : null,
                'quantity' => $pendingOrder['Quantity'],
                'price' => $pendingOrder['Limit'], // price comes under limit
                'asset_type' => 'crypto', // you trade futures on Bitmex
                'exchange_order_id' => $pendingOrder['OrderUuid'],
                'created_at' => $now,
                'updated_at' => $now
            ];
        }

        return [
            'error' => false,
            'openOrders' => $orderData
        ];
    }

    /**
     * Fetch open positions on KuCoin
     * @param $supportedExchange
     * @param $apiCredentials
     */
    private function fetchOpenOrdersKuCoin($supportedExchange, $apiCredentials)
    {

    }

    /**
     * Fetch open orders Binance
     * @param $supportedExchange
     * @param $apiCredentials
     * @return array
     * @throws ccxt\ExchangeError
     */
    private function fetchOpenOrdersBinance($supportedExchange, $apiCredentials)
    {
        $binance = new ccxt\binance([
            'apiKey' => $apiCredentials->api_key,
            'secret' => $apiCredentials->api_secret
        ]);
        // binance kept on returning timestamp 1000ms ahead of server's time
        // that's why subtracted 1000 from 13 digit micro secs to make
        // that work
        $microSec = $this->getCurrentMicroSecs() - 1000;
        $openOrders = $binance->privateGetOpenOrders([
            'timestamp' => $microSec
        ]);
        $orderData = [];
        $now = Carbon::now()->format('Y-m-d H:i:s');

        foreach ($openOrders as $openOrder) {
            $orderData[] = [
                'user_id' => Auth::user()->id,
                'exchange_id' => $supportedExchange->id,
                'exchange_response' => json_encode($openOrder),
                'symbol' => $openOrder['symbol'],
                'status' => 'open',
                'side' => strtolower($openOrder['side']),
                'type' => strtolower($openOrder['type']),
                'quantity' => $openOrder['origQty'],
                'price' => $openOrder['price'], // price comes under limit
                'asset_type' => 'crypto', // you trade futures on Bitmex
                'exchange_order_id' => $openOrder['orderId'],
                'created_at' => $now,
                'updated_at' => $now
            ];
        }

        return [
            'error' => false,
            'openOrders' => $orderData
        ];
    }

    /**
     * Fetch open orders on Gdax
     * @param $supportedExchange
     * @param $apiCredentials
     * @return array
     * @throws ccxt\ExchangeError
     */
    private function fetchOpenOrdersGdax($supportedExchange, $apiCredentials)
    {
        $gdax = new ccxt\gdax([
            'apiKey' => $apiCredentials->api_key,
            'secret' => $apiCredentials->api_secret,
            'password' => $apiCredentials->api_password
        ]);
        $markets = $gdax->fetchMarkets();
        $openOrders = $gdax->privateGetOrders(['status' => 'open']);
        $now = Carbon::now()->format('Y-m-d H:i:s');
        $orderData = [];

        foreach ($openOrders as $openOrder) {
            $side = strtolower($openOrder['side']);
            $arr = [
                'user_id' => Auth::user()->id,
                'exchange_id' => $supportedExchange->id,
                'exchange_response' => json_encode($openOrder),
                'symbol' => $openOrder['product_id'],
                'status' => 'open',
                'side' => $side,
                'type' => strtolower($openOrder['type']),
                'quantity' => $openOrder['size'],
                'price' => $openOrder['price'], // price comes under limit
                'asset_type' => 'crypto', // you trade futures on Bitmex
                'exchange_order_id' => $openOrder['id'],
                'created_at' => $openOrder['created_at'],
                'updated_at' => $now
            ];
            $symbolKey = array_search($openOrder['product_id'], array_column($markets, 'id'));
            if ($symbolKey) {
                $arr['price_currency'] = $side == 'buy' ? $markets[$symbolKey]['quote'] :
                    $markets[$symbolKey]['base'];
                $arr['ccxt_symbol'] = $markets[$symbolKey]['symbol'];
            }
            $orderData[] = $arr;
        }

        return [
            'error' => false,
            'openOrders' => $orderData
        ];
    }

    /**
     * Fetch open orders
     * Fetch open orders on various exchanges
     * and insert that into our database
     * @param FetchOpenOrders $request
     * @return array|\Illuminate\Http\JsonResponse
     * @throws ccxt\ExchangeError
     */
    public function fetchOpenOrders(FetchOpenOrders $request)
    {
        $exchangeName = $request->get('exchange_name');
        $exchangeName = strtolower($exchangeName);

        if (!$this->checkIfExchangeSupported($exchangeName)) {
            return $this->error('Exchange not supported');
        }

        $se = SupportedExchanges::where('name', $exchangeName)->first();

        $apiCredentials = ExchangeConn::where([
            'exchange' => $se->id,
            'user_id' => Auth::user()->id
        ])->first();

        if (!$apiCredentials) {
            return $this->error('Connect to ' . ucfirst($exchangeName) . ' first');
        }

        $openOrders = [];
        switch ($exchangeName) {
            case 'bitmex':
                $openOrders = $this->fetchOpenOrdersBitmex($se, $apiCredentials);
                break;
            case 'bittrex':
                $openOrders = $this->fetchOpenOrdersBittrex($se, $apiCredentials);
                break;
            case 'binance':
                $openOrders = $this->fetchOpenOrdersBinance($se, $apiCredentials);
                break;
            case 'kucoin':
                $openOrders = $this->fetchOpenOrdersKuCoin($se, $apiCredentials);
                break;
            case 'gdax':
                $openOrders = $this->fetchOpenOrdersGdax($se, $apiCredentials);
                //return $openOrders;
                break;
        }

        //return $openOrders;

        if (empty($openOrders)) {
            return $this->error('Something went wrong. Contact support.');
        }
        if ($openOrders['error']) {
            return $this->error('Couldn\'t fetch open orders');
        }

        if (count($openOrders['openOrders'])) {
            Orders::insert($openOrders['openOrders']);
            return $this->success('Fetched open orders on ' . ucfirst($exchangeName));
        } else {
            return $this->success('No open orders on ' . ucfirst($exchangeName));
        }
    }

    /**
     * Fetch orders dates list
     * Dates list use for calendar page
     * to highlight the date on which user place orders
     * @param calendarDatesList $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function calendarDatesList()
    {
        /**
         * sort orders list by user id
         * group it by created_at column
         */
        $datesList = Orders::select(DB::raw("date(created_at)"))
            ->where('user_id', Auth::user()->id)
            ->groupBy('created_at')
            ->orderBy('created_at')
            ->get();

        if (count($datesList)) {
            return $this->success('Retrieved orders dates list', $datesList, 200);
        } else {
            return $this->error('Couldn\'t found any order date list.');
        }
    }
}
