<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\BaseApiController;
use App\Http\Requests\AddPositionIntoCategory;
use App\Http\Requests\CreatePositionCategory;
use App\Http\Requests\DeletePositionCategory;
use App\Http\Requests\EditPositionCategory;
use App\Http\Requests\OpenPositionRequest;
use App\Model\PositionCategories\PositionCategory;
use App\Model\PositionCategories\PositionCategoryName;
use App\Model\Positions;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

/**
 * @resource Dashboard
 * Get currently open positions, orders, position categories
 * Add position into category, add category and delete
 */
class Dashboard extends BaseApiController
{
    /**
     *Get position categories names
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCategoriesNames()
    {
        $categoriesNames = PositionCategoryName::select(
            DB::raw("INITCAP(name) as name"), 'id'
        )->where('user_id', Auth::user()->id)
            ->orderBy('id', 'desc')
            ->get();

        return $this->success('Retrieved categories names', $categoriesNames);
    }

    /**
     * Create a position category
     * @param CreatePositionCategory $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createPositionCategory(CreatePositionCategory $request)
    {
        $name = $request->get('name');
        $nameFound = PositionCategoryName::where([
            'user_id' => Auth::user()->id,
            'name' => $name
        ])->first();

        if ($nameFound) {
            return $this->error('Already a category with same name exists');
        }

        PositionCategoryName::create([
            'user_id' => Auth::user()->id,
            'name' => strtolower($name)
        ]);

        return $this->success('Created position category name');
    }

    /**
     * Edit position category name
     * @param EditPositionCategory $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function editPositionCategoryName(EditPositionCategory $request)
    {
        $id = $request->get('position_category_id');
        $name = $request->get('name');
        $positionCategory = PositionCategoryName::find($id);

        if (Gate::denies('position-category-operations', $positionCategory)) {
            return $this->error('You don\'t own this category', [], 501);
        }

        if ($positionCategory->name == $name) {
            return $this->error('Nothing to edit');
        }

        $nameAlreadyExists = PositionCategoryName::where([
            'name' => $name,
            'user_id' => Auth::user()->id
        ])->first();
        if ($nameAlreadyExists) {
            return $this->error('Already a category with same name exists');
        }

        $positionCategory->name = $name;
        $positionCategory->save();

        return $this->success('Name edited successfully');
    }

    /**
     * Delete a position category name
     * @param DeletePositionCategory $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deletePositionCategory(DeletePositionCategory $request)
    {
        $id = $request->get('position_category_id');
        $positionCategory = PositionCategoryName::find($id);

        if (Gate::denies('position-category-operations', $positionCategory)) {
            return $this->error('You don\'t own this category', [], 501);
        }

        PositionCategoryName::destroy($id);
        return $this->success('Deleted position category');
    }

    /**
     * Add a position into a category
     * @param AddPositionIntoCategory $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addPositionIntoCategory(AddPositionIntoCategory $request)
    {
        $positionId = $request->get('position_id');
        $categoryId = $request->get('position_category_id');
        $position = Positions::find($positionId);
        $positionCat = PositionCategoryName::find($categoryId);

        if (Gate::denies('position-operations', $position)) {
            return $this->error('You don\'t own this position', [], 501);
        }

        if (Gate::denies('position-category-operations', $positionCat)) {
            return $this->error('You don\'t own this position category', [], 501);
        }

        $alreadyAdded = PositionCategory::where([
            'user_id' => Auth::user()->id,
            'user_positions_id' => $positionId,
            'user_position_categories_names_id' => $categoryId
        ])->first();

        if ($alreadyAdded) {
            return $this->error('Position already added into this category');
        }

        PositionCategory::create([
            'user_id' => Auth::user()->id,
            'user_positions_id' => $positionId,
            'user_position_categories_names_id' => $categoryId
        ]);

        return $this->success('Added position into category');
    }

    /**
     * Get currently open positions
     * Send `category_name` as `all` or `null` to get all the positions,
     * `category_name` as `uncategorized` to get positions that are not in
     * any category or a category name to get positions of
     * that category
     * @response {
     * {
     *  "success": true,
     *  "msg": "Retrieved positions",
     *  "data": [
     *     {
     *      "user_position_id": "9",
     *      "asset_type": "crypto",
     *      "symbol": "ETHBTC",
     *      "quantity": "0.01",
     *      "entry": "0",
     *      "price_currency": "ETH",
     *      "side": null,
     *      "exchange_name": "binance"
     *     }
     *  ]
     * }
     * }
     * @param OpenPositionRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function openPositions(OpenPositionRequest $request)
    {
        $categoryName = strtolower($request->get('category_name'));
        $whereArr = [
            'up.user_id' => Auth::user()->id,
            'up.deleted_at' => null
        ];
        $positions = DB::table('user_positions as up')
            ->join('supported_exchanges as se', 'se.id', '=', 'up.exchange_id')
            ->leftJoin('user_position_categories as upc', 'upc.user_positions_id', '=', 'up.id')
            ->leftJoin('user_position_categories_names as upcn', 'upcn.id', '=', 'upc.user_position_categories_names_id')
            ->select('up.id as user_position_id', 'asset_type', 'symbol',
                'quantity', 'price as entry',
                'price_currency', 'side', 'se.name as exchange_name')
            ->where($whereArr);
        if ($categoryName == 'uncategorized') {
            $positions = $positions->where('upcn.name', null);
        } elseif ($categoryName != 'all') {
            $positions = $positions->where('upcn.name', 'ilike', '%' . $categoryName . '%');
        }
        $positions = $positions->orderBy('up.id', 'desc')
            ->get();

        return $this->success(count($positions) ? 'Retrieved positions' : 'No open positions found', $positions);
    }
}
