<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Custom\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\BaseApiController;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Requests\registerUsingApiRequest;

class RegisterController extends BaseApiController
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    /**
     * Register using the API
     * @param registerUsingApiRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function registerUsingApi(registerUsingApiRequest $request)
    {

        try {

            DB::beginTransaction();

            $input = $request->all();
            $input['password'] = Hash::make($input['password']);
            $user = User::create($input);
            $newUser = User::find($user->id);
            $token = $user->createToken('normal_auth', ['*']);

            DB::commit();

            $response = [
                'user' => $newUser,
                'newToken' => $token
            ];

            return $this->success('Registered successfully', [], 200, $response);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->error($e->getMessage());
        }
    }
}
