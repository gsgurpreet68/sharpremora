<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Custom\ApiResponse;
use App\Model\PasswordReset;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Notifications\PasswordResetNotification;
use App\Http\Requests\passwordResetUsingApiRequest;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * password reset using api
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function passwordResetUsingApi(passwordResetUsingApiRequest $request)
    {
        
        $email = $request->get('username');
        try{
            $user = User::where('email',$email)->first();
            $token = bin2hex(random_bytes(32));
            $reset = PasswordReset::create([
                'token' => $token,
                'email' => $email
            ])->id;
            // $link = $request->getSchemeAndHttpHost() . '/reset/password/view?id=' . encrypt($reset) . '&token=' . $token;
            // Mail::to($email)->send(new \App\Mail\PasswordReset($link));

            $user->notify(new PasswordResetNotification($reset,$token));
        
            return $this->success('Reset email sent');
        }
        catch(\Exception $e)
        {   
            return $this->error($e->getMessage());
        }
        
    }
}
