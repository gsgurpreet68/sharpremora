<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\loginUsingApiRequest;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\BaseApiController;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends BaseApiController
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Login using the API
     * @param loginUsingApiRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function loginUsingApi(loginUsingApiRequest $request)
    {
        $email = $request->get('username');
        if (!Auth::attempt(['email' => $email, 'password' => $request->get('password')])) {
            return $this->error('Wrong credentials');
        }
        try {
            $user = User::find(Auth::user()->id);
            $token = $user->createToken('normal_auth', ['*']);
            $response = [
                'user' => $user,
                'newToken' => $token,
            ];
            return $this->success("Login Successful", [], 200, $response);
        } catch (\Exception $e) {
            return $this->error($e->getMessage());
        }
    }
}
