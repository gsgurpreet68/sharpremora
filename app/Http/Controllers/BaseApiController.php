<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Model\ExchangeConn;
use App\Model\SupportedExchanges;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;


class BaseApiController extends Controller
{
    /**
     * Send success response along with result
     * @param $msg
     * @param array $result
     * @param int $code
     * @param array $otherResponse - if wants to send any other response along with it
     * @return \Illuminate\Http\JsonResponse
     */
    public static function success($msg, $result = [], $code = 200, $otherResponse = [])
    {
        $response = [
            'success' => true,
            'msg' => $msg
        ];

        if (!empty($result)) {
            $response['data'] = $result;
        }

        if (!empty($otherResponse)) {
            foreach ($otherResponse as $key => $value) {
                $response[$key] = $value;
            }
        }

        return response()->json($response, $code);
    }

    /**
     * send error response
     * @param $error - error msg
     * @param array $errorMsgs - if there's more than one error msg
     * @param int $code
     * @param array $otherResponse
     * @return \Illuminate\Http\JsonResponse
     */
    public static function error($error, $errorMsgs = [], $code = 404, $otherResponse = [])
    {
        $response = [
            'success' => false,
            'msg' => $error
        ];

        if (!empty($errorMsgs)) {
            $response['data'] = $errorMsgs;
        }

        if (!empty($otherResponse)) {
            foreach ($otherResponse as $key => $value) {
                $response[$key] = $value;
            }
        }

        return response()->json($response, $code);
    }

    /**
     * Check if this exchange is supported or not
     * @param $exchange
     * @return bool
     */
    public function checkIfExchangeSupported($exchange)
    {
        $se = SupportedExchanges::where([
            'active' => true,
            'name' => strtolower($exchange)
        ])->first();
     
        return $se ? true : false;
    }

    /**
     * Check if already connected to an exchange or not
     * if yes, then return the credentials if no, then return false
     * @param $exchangeId int
     * @return array | bool
     */
    public function connectedToExchange($exchangeId)
    {
        $conn = ExchangeConn::where([
            'user_id' => Auth::user()->id,
            'exchange' => $exchangeId
        ])->first();

        if ($conn) {
            return [
                'apiKey' => $conn->api_key,
                'apiSecret' => $conn->api_secret,
                'apiPassword' => $conn->api_password,
                'api_uid' => $conn->api_uid
            ];
        } else {
            return false;
        }
    }

    // ** Check out the dropbox doc for handling error
    // using ccxt library for different exchanges
    // https://www.dropbox.com/home/Sharp%20Remora/Development/Bakc%20end%20dev%20team?preview=handling-exchange-error.tmd

    /**
     * Get the error msg returned by an exchange
     * @param \Exception $exceptionInstance
     * @return string
     */
    public function getErrorMsgFromExchange($exceptionInstance, $exchangeName)
    {
        $error = $exceptionInstance->getMessage();
        preg_match('/{.+}/i', $error, $matches);
        if (sizeof($matches)) {
            $exchangeError = json_decode($matches[0]);
        } else {
            $exchangeError = $error;
        }
        
        $msg = '';
        switch (strtolower($exchangeName)) {
            case 'binance':
                $msg = gettype($exchangeError) == 'object' ? $exchangeError->msg : $exceptionInstance->getMessage();
                break;
            case 'bittrex':
                $msg = gettype($exchangeError) == 'object' ?
                    // bittrex returns _ at the places of all the spaces
                    str_replace('_', ' ', $exchangeError->message)
                    : $error;
                break;
            case 'kucoin':
                $msg = gettype($exchangeError) == 'object' ? $exchangeError->msg : $error;
                break;
            case 'bitmex':
                $msg = gettype($exchangeError) == 'object' ? $exchangeError->error->message : $error;
                break;
            case 'gdax':
                $msg = gettype($exchangeError) == 'array' ? $exchangeError['message'] : $error;
                break;
            case 'whaleclub':
                $msg = gettype($exchangeError) == 'object' ? $exchangeError->error->message : $error;
                break;
        }

        return $msg;
    }

    /**
     * Get micro secs in 13 digit format
     * A lot of exchanges need this 13 secs micro second
     * in a lot of operations
     * @return int
     */
    public function getCurrentMicroSecs()
    {
        $now = Carbon::now();
        // 13 digit micro secs format is needed. Not less
        $micro = round($now->format('U.u') * 1000);
        return $micro;
    }
}
