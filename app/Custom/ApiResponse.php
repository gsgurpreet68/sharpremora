<?php
/**
 * Created by PhpStorm.
 * User: koush
 * Date: 08-Mar-18
 * Time: 9:00 PM
 */

namespace App\Custom;


class ApiResponse
{
    /**
     * Send success response along with result
     * @param $msg
     * @param array $result
     * @param int $code
     * @param array $otherResponse - if wants to send any other response along with it
     * @return \Illuminate\Http\JsonResponse
     */
    public static function success($msg, $result = [], $code = 200, $otherResponse = [])
    {
        $response = [
            'success' => true,
            'msg' => $msg
        ];

        if (!empty($result)) {
            $response['data'] = $result;
        }

        if (!empty($otherResponse)) {
            foreach ($otherResponse as $key => $value) {
                $response[$key] = $value;
            }
        }

        return response()->json($response, $code);
    }

    /**
     * send error response
     * @param $error - error msg
     * @param array $errorMsgs - if there's more than one error msg
     * @param int $code
     * @param array $otherResponse
     * @return \Illuminate\Http\JsonResponse
     */
    public static function error($error, $errorMsgs = [], $code = 404, $otherResponse = [])
    {
        $response = [
            'success' => false,
            'msg' => $error
        ];

        if (!empty($errorMsgs)) {
            $response['data'] = $errorMsgs;
        }

        if (!empty($otherResponse)) {
            foreach ($otherResponse as $key => $value) {
                $response[$key] = $value;
            }
        }

        return response()->json($response, $code);
    }
}