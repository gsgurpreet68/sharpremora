<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\AuthenticationException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

use Log;


class Handler extends ExceptionHandler
{

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
     ];
     
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        // add api excepeption as json
        if ($request->ajax() || $request->wantsJson() || $request->expectsJson())
        {
            //Log::error('api-exception-log : ', [$exception]);

            //we are only adding exceptions that are not handled in the normal way here
        if ($exception instanceof MethodNotAllowedHttpException)
        {
            return $this->displayExceptionAsJson(405,"Method Not Allowed");
        }
        else if ($exception instanceof ModelNotFoundException) {

            return $this->displayExceptionAsJson(500,"Model Not Found");
        } else if ($exception instanceof NotFoundHttpException) {

            return $this->displayExceptionAsJson(404,"Invalid Api Endpoint");
        }
        else if ($exception instanceof HttpException) {
                switch ($exception->getStatusCode()){
                    case '404':
                        return $this->display404ExceptionAsJson($exception);
                        break;
                    default:
                }
            
            }
            else if ($exception instanceof ValidationException) {
                $errorMessage = $exception->getMessage();
                $statusCode = $exception->getCode();
        
                if(empty($message)){
                    $errorMessage = 'Validation Error';
                }
        
                if(empty($statusCode)){
                    $statusCode = 422;
                }
        
                return $this->displayExceptionAsJson($statusCode,$errorMessage,$exception->errors());
            }

            else
            {
                // does not catch validation exception
                if(!$exception instanceof ValidationException)
                {

                    if (env('APP_ENV') == 'local' || $exception instanceof AuthenticationException) {
                        if ($exception instanceof AuthenticationException) {
                            return $this->displayExceptionAsJson(401, $exception->getMessage());
                        }

                        return $this->displayExceptionAsJson(500, $exception->getMessage());
                    }
                    else {
                        return $this->displayExceptionAsJson(500, "Internal Error");
                    }
                }


            }



        }

        return parent::render($request, $exception);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return $this->display401ExceptionAsJson($exception);
        }

        return redirect()->guest('login');
    }

    /**
     * display the 404 error as json
     *
     * @param Exception $e
     * @return \Illuminate\Http\JsonResponse
     */
    public function display404ExceptionAsJson(Exception $e){

        $errorMessage = $e->getMessage();
        $statusCode = $e->getCode();

        if(empty($message)){
            $errorMessage = 'Unknown API endpoint';
        }

        if(empty($statusCode)){
            $statusCode = Response::HTTP_NOT_FOUND;
        }

        return $this->displayExceptionAsJson($statusCode,$errorMessage);

    }

    public function display401ExceptionAsJson($e){

        $errorMessage = $e->getMessage();
        $statusCode = $e->getCode();

        if(empty($message)){
            $errorMessage = 'Unauthorized access';
        }

        if(empty($statusCode)){
            $statusCode = Response::HTTP_UNAUTHORIZED;
        }

        return $this->displayExceptionAsJson($statusCode,$errorMessage);

    }


    /**
     * display the exception as json
     *
     * @param $statusCode
     * @param $errorMessage
     * @return \Illuminate\Http\JsonResponse
     */
    public function displayExceptionAsJson($statusCode = 500,$errorMessage,$errors = []){


        $json = [
            'success' => false,
            'msg' => $errorMessage
        ];

        if(!empty($errors))
        {
            $json['errors'] = $errors;
        }

        return response()->json($json, $statusCode);

    }
}