# sharp-remora

Sharp Remora is a trade management application that provides a frictionless way for retail investors/traders to manage risk in their investment positions, and perform advanced actions such as semi-automated position management using pre-defined rules, and improve their trading and investment knowledge by providing an online community where they connect with other investors.