<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
Hi, kindly verify your new email using this link
<a href="{{ $link }}">Reset link</a>
<br>
<b>It's valid for only 24hrs</b>
</body>
</html>