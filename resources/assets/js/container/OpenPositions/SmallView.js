import React from 'react';
import './SmallView.css';

class SmallView extends React.Component{

    constructor() {
        super();
        this.state = {
            elemX: 0,
            elemY: 0
        }
    }

    handleDragStart(event) {
        var rects = event.currentTarget.getClientRects()[0];
        this.setState({elemX: event.clientX - rects.x, elemY: event.clientY - rects.y});
        // console.log(event);
    }

    handleDrag(event) {
        // document.getElementById("dragger").style.top = (event.clientY-this.state.elemY)+"px";
        // document.getElementById("dragger").style.left = (event.clientX-this.state.elemX)+"px";
        this.refs.srdrag.style.top = (event.clientY-this.state.elemY)+"px";
        this.refs.srdrag.style.left = (event.clientX-this.state.elemX)+"px";
    }

    handleDragOver(event) {
        event.preventDefault();
    }

    render(){
        return (
            <div className="sr-open-small-view" draggable ref="srdrag" onDragStart={(event) => this.handleDragStart(event)} onDrag={(event) => this.handleDrag(event)} onDragOver={(event) => this.handleDragOver(event)} >
                <div className="sr-osv-top" >
                    <div>BTC/USD</div>
                    <div>
                        <select>
                            <option>10x</option>
                            <option>20x</option>
                            <option>5x</option>
                            <option>3x</option>
                            <option>1x</option>
                        </select>
                    </div>
                    <div>0 BTC</div>
                    <div className="sr-osvt-dragger" ></div>
                    <div>Set Price</div>
                    <div>x</div>
                </div>
                <div className="sr-osv-mid">
                    <div>ENTER PRICE</div>
                    <div>
                        <input type="text" />
                    </div>
                </div>
                <div className="sr-osv-bot" >
                    <div className="sr-osvb-ind sr-osvb-ind-sb" >SELL</div>
                    <div className="sr-osvb-ind" >
                        <div className="sr-osvbi-ind" >
                            <div className="sr-osvbii-val" >0</div>
                            <div className="sr-osvbii-spread" >SPREAD 0.79%</div>
                        </div>
                    </div>
                    <div className="sr-osvb-ind sr-osvb-ind-sb" >BUY</div>
                </div>
            </div>
        )
    }
}

export default SmallView;