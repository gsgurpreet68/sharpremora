import React, {Component} from 'react';
import Modal from '../../components/Modal/Modal';
import RangeSlider from '../../components/Widget/RangeSlider/RangeSlider';
import './OpenPositions.css';

import {platforms} from '../../data/PlatformSteps';

class OpenPostions extends Component {
    constructor() {
        super();
        this.state = {
            broker: 'none',
            is_asset_in_portfolio: false,
            asset_type: 'Crypto',
            asset_name: '',
            order_side: 'buy',
            order_size: '0',
            order_price_rate: '0.028',
            order_type: ['limit'],
            time_in_force: 'GTC',
            fetched_markets: [],
            added_exchanges: [],
            show_commit_status: false,
            post_commit_message: '',
            commit_success: true,
            stop_loss: {
                absolute: '',
                percent: ''
            }
        }
    }

    setOrderType(type) {
        if (this.state.order_type.indexOf(type) >= 0) {
            if (this.state.order_type.length > 1) {
                this.state.order_type.splice(this.state.order_type.indexOf(type), 1);

            }

        } else {
            if (type == 'stop_loss' && this.state.order_type.indexOf('market') >= 0) {
                this.state.order_type.splice(this.state.order_type.indexOf('market'), 1);
            } else if (type == 'market' && this.state.order_type.indexOf('stop_loss') >= 0) {
                this.state.order_type.splice(this.state.order_type.indexOf('stop_loss'), 1);
            }
            this.state.order_type.push(type);
        }
        this.setState({order_type: this.state.order_type}, () => {
            console.log(this.state.order_type);
        });
    }

    getOrderType() {
        if (this.state.order_type.indexOf('limit') >= 0) {
            return this.state.order_type.indexOf('market') >= 0 ? 'market_limit' : (this.state.order_type.indexOf('stop_loss') >= 0 ? 'stop_loss_limit' : 'limit');
        } else {
            return this.state.order_type[0];
        }
    }

    componentDidMount() {
        this.getExchanges()
    }

    commitOrder() {
        this.setState({show_commit_status: false});
        if (this.state.broker == 'none') {
            console.log('Select broker');
        }
        if (this.state.fetched_markets.indexOf(this.state.asset_name) < 0) {
            console.log('Symbol does not exist');
        }
        var params = {
            exchange_name: this.state.broker.toLowerCase(),
            symbol: this.state.asset_name,
            type: this.getOrderType(),
            side: this.state.order_side,
            quantity: this.state.order_size,
            price: this.state.order_price_rate,
            time_in_force: this.state.time_in_force
        }
        console.log(params);
        axios.post('/api/v1/position/open', params, {
            headers: {
                Authorization: "Bearer " + localStorage.getItem('token')
            }
        })
            .then(response => {
                console.log(response);
                if (response.data.success) {
                    this.setState({
                        show_commit_status: true,
                        commit_success: true,
                        post_commit_message: 'Order placed successfully'
                    });
                } else {
                    this.setState({
                        show_commit_status: true,
                        commit_success: false,
                        post_commit_message: response.data.msg
                    });
                }
            })
            .catch(error => {
                console.log(error.response);
                this.setState({
                    show_commit_status: true,
                    commit_success: false,
                    post_commit_message: error.response.data.msg
                });
            })
    }

    getMarkets() {

        axios.get('/api/v1/exchange/' + this.state.broker.toLowerCase() + '/markets', {
            headers: {
                Authorization: "Bearer " + localStorage.getItem('token')
            }
        })
            .then(response => {
                this.setState({fetched_markets: response.data, asset_name: ''});
            })
            .catch(error => {
                console.log(error);
            })
    }

    getExchanges() {
        this.setState({show_commit_status: false});
        axios.get('/api/v1/exchange/connected', {
            headers: {
                Authorization: "Bearer " + localStorage.getItem('token')
            }
        })
            .then(response => {
                var data = response.data;
                this.setState({added_exchanges: data.data});
            })
            .catch(error => {
                console.log(error.response.data);
            })
    }

    isValidSelected() {
        if (this.state.asset_name == '') {
            return false;
        }
        for (var i = 0; i < this.state.fetched_markets.length; i++) {
            if (this.state.asset_name == this.state.fetched_markets[i].symbol) {
                return true;
            }
        }
        return false;
    }

    render() {
        return (
            <Modal closeModal={() => this.props.closeModal()}>
                <div className="row no-gutter grey-100" style={{width: '100%', padding: '5px'}}>
                    <div className="col col-5 row b-r">
                        <div className="col col-12 p-l-md">
                            {
                                this.state.asset_name != '' ? (
                                    <div>
                                        <span className="_600"
                                              style={{marginRight: '5px'}}>{this.state.asset_name}</span>
                                        <span className="label accent label-sm">{this.state.asset_type}</span>
                                        <span style={{display: 'block'}}>Current: 0 shares ($0)</span>
                                    </div>
                                ) : (
                                    <span>No asset selected</span>
                                )
                            }

                        </div>
                        {/* <div className="col col-4">
                                    <span className="label label-sm danger" title="Leverage: Margin - $1750, Fees - 1% of position">x4 Leverage</span> &nbsp;
                                    <span className="label label-sm grey-400" title="Interactive Brokers">Short</span>
                                </div> */}
                    </div>
                    <div className="col col-3 b-r p-l-md">
                        <span className="_600">Broker</span><br/>
                        <select value={this.state.broker}
                                onChange={(e) => this.setState({broker: e.target.value}, () => this.getMarkets())}>
                            <option value={'none'}>Select broker</option>
                            {
                                this.state.added_exchanges.map((e, i) => {
                                    return (
                                        <option value={e.name} key={i}>{e.name.toUpperCase()}</option>
                                    )
                                })
                            }
                        </select>
                    </div>
                    <div className="col col-4 p-l-sm p-t-sm">
                        <div className="input-group input-group-sm ">
                            {
                                this.state.broker != 'none' && !this.isValidSelected() ? (
                                    <div style={{position: 'absolute', top: 30, left: 0, width: '100%', zIndex: 2006}}>
                                        <ul className="sr-suggestion-ul">
                                            {
                                                this.state.fetched_markets.map((e, i) => {
                                                    var patt = new RegExp(' ' + this.state.asset_name, 'gi');
                                                    if (patt.test(' ' + e.symbol) || patt.test(' ' + e.base + '-' + e.quote)) {
                                                        return (<li key={i}
                                                                    onClick={() => this.setState({asset_name: e.symbol})}>
                                                            <div>{e.symbol + '(' + e.base + '-' + e.quote + ')'}</div>
                                                        </li>);
                                                    }
                                                })
                                            }


                                        </ul>
                                    </div>
                                ) : (
                                    null
                                )
                            }
                            <input type="text" placeholder="Switch Asset" value={this.state.asset_name}
                                   onChange={e => this.setState({asset_name: e.target.value})}/>
                            <span className="input-group-btn">
                                        <button className="btn b-a n-shadow white" type="button">Search</button>
                                    </span>
                        </div>
                    </div>
                </div>
                <div style={{marginBottom: '10px'}}></div>
                <div className="row" style={{height: 400}}>
                    <div className="col-md-4"
                         style={{height: 400, borderRight: '1px solid rgba(120, 130, 140, 0.13)', overflow: 'auto'}}>
                        <div className="" style={{marginTop: 10}}>
                            <div className="m-b-sm">
                                <span className="pull-left m-r-sm">Buy/Long</span>
                                <label className="md-switch pull-left">
                                    <input type="checkbox" value={this.state.order_side == 'buy'}
                                           onChange={e => console.log(e.target.value)}/>
                                    <i className="danger"></i>
                                </label> Sell/Short
                            </div>
                        </div>
                        <div className="form-group row">
                            <label className="col-sm-2 form-control-label">Size</label>
                            <div className="col-sm-6">
                                <input type="text" className="form-control" id="inputEmail3" placeholder=""
                                       value={this.state.order_size}
                                       onChange={e => this.setState({order_size: e.target.value})}/>
                            </div>
                            <div className="col-sm-4">
                                <button className="md-btn md-flat m-b-sm text-success col-md-6"
                                        onClick={() => this.setState({order_size: (parseFloat(this.state.order_size) + 1) + ''})}>
                                    <i className="fa fa-plus"></i></button>
                                <button className="md-btn md-flat m-b-sm text-danger col-md-6"
                                        onClick={() => this.setState({order_size: parseFloat(this.state.order_size) >= 1 ? (parseFloat(this.state.order_size) - 1) + '' : this.state.order_size})}>
                                    <i className="fa fa-minus"></i></button>
                            </div>
                        </div>
                        <div className="form-group row">
                            <label className="col-sm-2 form-control-label">Price</label>
                            <div className="col-sm-10">
                                <input type="number" className="form-control" id="inputEmail3" placeholder=""
                                       value={this.state.order_price_rate}
                                       onChange={e => this.setState({order_price_rate: e.target.value})}
                                       disabled={this.state.order_type.indexOf('market') >= 0}/>
                            </div>
                            <div className="col-md-12" style={{marginTop: 10}}>
                                <button className="md-btn md-flat m-b-sm text-default col-md-4"
                                        style={{backgroundColor: this.state.order_type.indexOf('limit') >= 0 ? 'rgba(158, 158, 158, 0.2)' : ''}}
                                        onClick={() => this.setOrderType('limit')}>L
                                </button>
                                <button className="md-btn md-flat m-b-sm text-default col-md-4"
                                        style={{backgroundColor: this.state.order_type.indexOf('market') >= 0 ? 'rgba(158, 158, 158, 0.2)' : ''}}
                                        onClick={() => this.setOrderType('market')}>M
                                </button>
                                {/* <button className="md-btn md-flat m-b-sm text-default col-md-3" style={{backgroundColor:this.state.order_type=='market_limit'?'rgba(158, 158, 158, 0.2)':''}} onClick={()=>this.setState({order_type:'market_limit'})}>ML</button> */}
                                <button className="md-btn md-flat m-b-sm text-default col-md-4"
                                        style={{backgroundColor: this.state.order_type.indexOf('stop_loss') >= 0 ? 'rgba(158, 158, 158, 0.2)' : ''}}
                                        onClick={() => this.setOrderType('stop_loss')}>S
                                </button>
                            </div>
                        </div>
                        <div className="form-group row">
                            <label className="col-sm-5 form-control-label">Time in force</label>
                            <div className="col-sm-7">
                                <select className="form-control c-select" value={this.state.time_in_force}
                                        onChange={e => this.setState({time_in_force: e.target.value})}>
                                    <option value="gtc">GTC</option>
                                    <option value="ioc">IOC</option>
                                    <option value="fok">FOK</option>
                                </select>
                            </div>
                        </div>
                        <div className="form-group row">
                            <label className="col-sm-12 form-control-label">Stop Loss</label>
                            <div className="col-sm-8">
                                <input type="number" className="form-control" id="inputEmail3"
                                       placeholder={(25 * this.state.order_price_rate / 100)}
                                       value={this.state.stop_loss.absolute} onChange={(e) => this.setState({
                                    stop_loss: {
                                        absolute: e.target.value,
                                        percent: e.target.value == '' ? '' : (parseFloat(e.target.value) / parseFloat(this.state.order_price_rate) * 100)
                                    }
                                })}/>
                            </div>
                            <div className="col-md-4">
                                <button className="md-btn md-flat m-b-sm text-success col-md-6"><i
                                    className="fa fa-plus"></i></button>
                                <button className="md-btn md-flat m-b-sm text-danger col-md-6"><i
                                    className="fa fa-minus"></i></button>
                            </div>
                            <div className="col-sm-8">
                                <input type="number" className="form-control" id="inputEmail3" placeholder="25%"
                                       value={this.state.stop_loss.percent} onChange={(e) => this.setState({
                                    stop_loss: {
                                        percent: e.target.value,
                                        absolute: e.target.value == '' ? '' : (parseFloat(e.target.value) * parseFloat(this.state.order_price_rate) / 100)
                                    }
                                })}/>
                            </div>
                            <div className="col-md-4">
                                <button className="md-btn md-flat m-b-sm text-success col-md-6"><i
                                    className="fa fa-plus"></i></button>
                                <button className="md-btn md-flat m-b-sm text-danger col-md-6"><i
                                    className="fa fa-minus"></i></button>
                            </div>
                        </div>
                        <div className="form-group row">
                            <label className="col-sm-12 form-control-label">Take Profit</label>
                            <div className="col-sm-8">
                                <input type="number" className="form-control" id="inputEmail3" placeholder="Absolute"/>
                            </div>
                            <div className="col-md-4">
                                <button className="md-btn md-flat m-b-sm text-success col-md-6"><i
                                    className="fa fa-plus"></i></button>
                                <button className="md-btn md-flat m-b-sm text-danger col-md-6"><i
                                    className="fa fa-minus"></i></button>
                            </div>
                            <div className="col-sm-8">
                                <input type="number" className="form-control" id="inputEmail3"
                                       placeholder="Percentage"/>
                            </div>
                            <div className="col-md-4">
                                <button className="md-btn md-flat m-b-sm text-success col-md-6"><i
                                    className="fa fa-plus"></i></button>
                                <button className="md-btn md-flat m-b-sm text-danger col-md-6"><i
                                    className="fa fa-minus"></i></button>
                            </div>
                        </div>
                        {
                            false ? (
                                <div>
                                    <div className="form-group row">
                                        <label className="col-sm-3 form-control-label">Rule</label>
                                        <div className="col-sm-9">
                                            <select className="form-control c-select">
                                                <option>Rule 1</option>
                                                <option>Rule 2</option>
                                                <option>Rule 3</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label className="col-sm-3 form-control-label">Leverage</label>
                                        <div className="col-sm-9" style={{marginTop: '0.8rem'}}>
                                            <RangeSlider
                                                rangeStart={0}
                                                rangeEnd={5}
                                                interval={1}
                                                startPosition={2}
                                                markerMoved={point => console.log(point)}
                                            />
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label className="col-sm-2 form-control-label">Cost</label>
                                        <div className="col-sm-4">
                                            <p className="form-control-static">$XXXXX</p>
                                        </div>
                                        <label className="col-sm-2 form-control-label">Fees</label>
                                        <div className="col-sm-4">
                                            <p className="form-control-static">$XX.XX</p>
                                        </div>
                                    </div>
                                </div>

                            ) : null
                        }

                        <div className="row">
                            <div className="col-md-12">
                                <button className="btn success btn-block" onClick={() => this.commitOrder()}>Commit
                                </button>
                            </div>
                        </div>
                        {
                            this.state.show_commit_status ? (
                                <div
                                    className={this.state.commit_success ? "alert alert-success" : "alert alert-danger"}
                                    style={{width: '100%'}}>{this.state.post_commit_message}</div>
                            ) : null
                        }

                    </div>
                    <div className="col-md-8">
                        <div className="row" style={{height: '50%'}}>
                            <div className="col-md-6" style={{
                                borderRight: '1px solid rgba(120, 130, 140, 0.13)',
                                borderBottom: '1px solid rgba(120, 130, 140, 0.13)'
                            }}>
                                R/R Projections
                            </div>
                            <div className="col-md-6"
                                 style={{borderBottom: '1px solid rgba(120, 130, 140, 0.13)', overflow: 'hidden'}}>
                                <div className="sr-box">
                                    <div className="sr-row buy-sell">
                                        <div>
                                            Buy Order
                                        </div>
                                        <div>
                                            Sell Order
                                        </div>
                                    </div>
                                    <div className="sr-row pri-qty">
                                        <div>Price</div>
                                        <div>Qty</div>
                                        <div>Price</div>
                                        <div>Qty</div>
                                    </div>
                                    <div className="sr-row content">
                                        <div className="child-three-ind">
                                            <div className="child-three-ind-ind">123656</div>
                                            <div className="child-three-ind-ind">1</div>
                                            <div className="child-three-ind-ind">456</div>
                                            <div className="child-three-ind-ind">2</div>
                                        </div>
                                        <div className="child-three-ind">
                                            <div className="child-three-ind-ind">123</div>
                                            <div className="child-three-ind-ind">1</div>
                                            <div className="child-three-ind-ind">456</div>
                                            <div className="child-three-ind-ind">2</div>
                                        </div>
                                        <div className="child-three-ind">
                                            <div className="child-three-ind-ind">123</div>
                                            <div className="child-three-ind-ind">1</div>
                                            <div className="child-three-ind-ind">456</div>
                                            <div className="child-three-ind-ind">2</div>
                                        </div>
                                        <div className="child-three-ind">
                                            <div className="child-three-ind-ind">123</div>
                                            <div className="child-three-ind-ind">1</div>
                                            <div className="child-three-ind-ind">456</div>
                                            <div className="child-three-ind-ind">2</div>
                                        </div>
                                        <div className="child-three-ind">
                                            <div className="child-three-ind-ind">123</div>
                                            <div className="child-three-ind-ind">1</div>
                                            <div className="child-three-ind-ind">456</div>
                                            <div className="child-three-ind-ind">2</div>
                                        </div>
                                        <div className="child-three-ind">
                                            <div className="child-three-ind-ind">123</div>
                                            <div className="child-three-ind-ind">1</div>
                                            <div className="child-three-ind-ind">456</div>
                                            <div className="child-three-ind-ind">2</div>
                                        </div>
                                        <div className="child-three-ind">
                                            <div className="child-three-ind-ind">123</div>
                                            <div className="child-three-ind-ind">1</div>
                                            <div className="child-three-ind-ind">456</div>
                                            <div className="child-three-ind-ind">2</div>
                                        </div>
                                        <div className="child-three-ind">
                                            <div className="child-three-ind-ind">123</div>
                                            <div className="child-three-ind-ind">1</div>
                                            <div className="child-three-ind-ind">456</div>
                                            <div className="child-three-ind-ind">2</div>
                                        </div>
                                        <div className="child-three-ind">
                                            <div className="child-three-ind-ind">123</div>
                                            <div className="child-three-ind-ind">1</div>
                                            <div className="child-three-ind-ind">456</div>
                                            <div className="child-three-ind-ind">2</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row" style={{height: '50%'}}>
                            <div className="col-md-6" style={{borderRight: '1px solid rgba(120, 130, 140, 0.13)'}}>
                                Impact on Portfolio
                            </div>
                            <div className="col-md-6">
                                Market Depth/ Asset summary/ Asset History
                            </div>
                        </div>
                    </div>

                </div>
            </Modal>
        )
    }
}

export default OpenPostions;