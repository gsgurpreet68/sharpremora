import React from 'react';
import {strCapitalize} from "../../Helpers/Functions/HelpfulFunctions";
import Loader1 from "../../components/Loader/Loader1";

export default class EachExchange extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.disconnectExchange = this.disconnectExchange.bind(this);
        this.editConn = this.editConn.bind(this);
        this.getBalance = this.getBalance.bind(this);
    }

    disconnectExchange() {
        const confirm = window.confirm('Are you sure that you want to disconnect?');
        if (confirm) {
            this.props.disconnectExchange ? this.props.disconnectExchange(this.props.exchange.id) : null;
        }
    }

    editConn() {
        this.props.editConn ? this.props.editConn(this.props.exchange.name) : null;
    }

    getBalance() {
        this.props.getBalance ? this.props.getBalance(this.props.exchange.name) : null;
    }

    render() {
        return (
            <tr>
                <td>{strCapitalize(this.props.exchange.name)}</td>
                <td>
                    <button className="btn btn-info btn-sm"
                            onClick={this.getBalance}
                            disabled={this.props.modifyingExchangeId === this.props.exchange.id}>
                        Get Balance
                    </button>
                </td>
                <td>
                    <button className="btn btn-primary btn-sm"
                            onClick={this.editConn}
                            disabled={this.props.modifyingExchangeId === this.props.exchange.id}>
                        Edit
                    </button>
                </td>
                <td>
                    <button className="btn btn-danger btn-sm"
                            disabled={this.props.modifyingExchangeId === this.props.exchange.id}
                            onClick={this.disconnectExchange}>
                        Disconnect
                    </button>
                </td>
            </tr>
        )
    }
}