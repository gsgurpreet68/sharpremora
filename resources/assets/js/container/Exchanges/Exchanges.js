import React from 'react';
import Home from '../../components/Home/Home';
import './scss/exchanges.scss';
import Loader1 from "../../components/Loader/Loader1";
import EachExchange from "./EachExchange";
import toast from "../../Helpers/Toast/Toast";
import {ajaxErrorHandling} from "../../Helpers/ErrorHandling/AjaxErrorHandling";
import Sad from "../../Helpers/Sad/Sad";
import EditConnModal from "./EditConnModal";
import BalanceModal from "./BalanceModal";

class Exchanges extends React.Component {
    constructor() {
        super();
        this.state = {
            showModal: false,
            isConnected: true,
            addedExchanges: [],
            modifyingExchangeId: 0
        };
        this.disconnectExchange = this.disconnectExchange.bind(this);
        this.getExchanges = this.getExchanges.bind(this);
        this.closeExchangeModal = this.closeExchangeModal.bind(this);
        this.editConn = this.editConn.bind(this);
        this.getBalance = this.getBalance.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.exchangeConnModalClosed !== this.props.exchangeConnModalClosed) {
            this.getExchanges();
        }
    }

    componentDidMount() {
        this.getExchanges();
    }

    getExchanges() {
        this.setState({
            showLoader: true
        });
        axios.get('/api/v1/exchange/connected', {
            headers: {
                Authorization: "Bearer " + localStorage.getItem('token')
            }
        }).then(response => {
            const data = response.data;
            this.setState({
                addedExchanges: data.data,
                showLoader: false,
                exchangesLoaded: true
            });
        }).catch(error => {
            console.log(error.response.data);
        });
    }

    disconnectExchange(id) {
        this.setState({
            modifyingExchangeId: id,
            showLoader: true
        });
        axios.post('/api/v1/exchange/disconnect', {
            exchange_id: id
        }, {
            headers: {
                Authorization: "Bearer " + localStorage.getItem('token')
            }
        }).then(response => {
            const data = response.data;
            this.getExchanges();
            toast(response.data.msg);
            this.setState({
                modifyingExchangeId: 0,
                showLoader: false
            });
        }).catch(error => {
            console.log(error);
            toast(ajaxErrorHandling(error));
            this.setState({
                modifyingExchangeId: 0,
                showLoader: false
            });
        })
    }

    closeExchangeModal() {
        this.setState({
            showModal: false
        }, () => {
            this.getExchanges();
        });
    }

    editConn(exchangeName) {
        this.setState({
            currentExchangeEdit: exchangeName,
            showEditModal: true
        });
    }

    getBalance(exchangeName) {
        this.setState({
            currentExchangeBalance: exchangeName,
            showBalanceModal: true
        });
    }

    render() {
        let exchanges = this.state.addedExchanges;
        exchanges = exchanges.map((e, i) => {
            return <EachExchange key={i}
                                 exchange={e}
                                 modifyingExchangeId={this.state.modifyingExchangeId}
                                 editConn={this.editConn}
                                 getBalance={this.getBalance}
                                 disconnectExchange={this.disconnectExchange}/>
        });
        return (
            <div className="app-body" id="exchanges">
                {/* <SmallView /> */}
                {this.state.showModal ?
                    <Home exchangeExists={true}
                          triggerClose={this.closeExchangeModal}/> : ''}
                <div className="padding">
                    <div className="box">
                        <div className="box-header">
                            <h3 className="main-header">
                                Connected Exchanges
                            </h3>
                            <div className="pull-right">
                                <button onClick={() => this.setState({showModal: true})}
                                        className="btn btn-primary btn-sm">Add Exchange
                                </button>
                            </div>
                            {this.state.showLoader ? <Loader1/> : null}
                        </div>
                        <EditConnModal currentExchange={this.state.currentExchangeEdit}
                                       modalClosed={() => this.setState({showEditModal: false})}
                                       showModal={this.state.showEditModal}/>

                        <BalanceModal currentExchange={this.state.currentExchangeBalance}
                                      modalClosed={() => this.setState({showBalanceModal: false})}
                                      showModal={this.state.showBalanceModal}/>
                        {(() => {
                            if (this.state.exchangesLoaded && this.state.addedExchanges.length === 0) {
                                return <div className="text-center">
                                    <Sad text="Not Connected to Any Exchange"/>
                                </div>
                            } else {
                                return <div className="table-responsive">
                                    <table className="table table-striped table-hover table-bordered white b-a">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th title="get balance">
                                                Get Bal
                                            </th>
                                            <th>
                                                Edit
                                            </th>
                                            <th>Disconnect</th>
                                        </tr>
                                        </thead>
                                        <tbody>{exchanges}</tbody>
                                    </table>
                                </div>
                            }
                        })()}
                    </div>
                </div>
            </div>
        );
    }
}

export default Exchanges;