import React from 'react';
import BasicActionModal from "../../components/Modal/BasicActionModal";
import Loader1 from "../../components/Loader/Loader1";
import toast from "../../Helpers/Toast/Toast";
import {ajaxErrorHandling} from "../../Helpers/ErrorHandling/AjaxErrorHandling";

export default class EditConnModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.inputChange = this.inputChange.bind(this);
        this.editConn = this.editConn.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.currentExchange !== nextProps.currentExchange) {
            this.setState({
                exchangeName: nextProps.currentExchange
            }, () => {
                const exchangeName = this.state.exchangeName.toLowerCase();
                if (exchangeName === 'gdax' || exchangeName === 'bitstamp') {
                    this.setState({
                        showExtraField: true
                    });
                    if (exchangeName === 'gdax') {
                        this.setState({
                            extraFieldPlaceholder: 'Pass phrase'
                        });
                    } else if (exchangeName === 'bitstamp') {
                        this.setState({
                            extraFieldPlaceholder: 'User id'
                        });
                    }
                } else {
                    this.setState({
                        showExtraField: false
                    });
                }
            });
        }
    }

    inputChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    editConn() {
        const data = {
            exchange_name: this.state.exchangeName,
            api_key: this.state.apiKey,
            api_secret: this.state.apiSecret,
            extra_1: this.state.extraParam,
            revalidate: 1 // in php, 1 means true. Send true may result in validation error
        };
        this.setState({
            editing: true
        });
        axios.post('/api/v1/exchange/connect', data, {
            headers: {
                Authorization: "Bearer " + localStorage.getItem('token')
            }
        }).then(response => {
            const data = response.data;
            console.log(data);
            this.setState({
                editing: false
            });
            toast(data.msg);
            if (data.success) {
                this.props.modalClosed ? this.props.modalClosed() : null;
            }
        }).catch(error => {
            console.log(error.response.data);
            this.setState({
                editing: false
            });
            toast(ajaxErrorHandling(error));
        });
    }

    render() {
        return (
            <BasicActionModal isOpen={this.props.showModal}
                              header="Edit Conn"
                              hideCross={true}
                              actionBtn={true}
                              actionBtnName="Edit"
                              actionBtnDisabled={this.state.editing}
                              actionBtnClicked={this.editConn}
                              modalClosed={this.props.modalClosed}
                              actionBtnClass="btn btn-primary">
                <div className="form-group">
                    <input type="text"
                           className="form-control"
                           name="apiKey"
                           onChange={this.inputChange}
                           placeholder="API Key"/>
                </div>
                <div className="form-group">
                    <input type="text"
                           onChange={this.inputChange}
                           className="form-control"
                           name="apiSecret"
                           placeholder="API Secret"/>
                </div>
                {(() => {
                    if (this.state.showExtraField) {
                        return <div className="form-group">
                            <input type="text"
                                   className="form-control"
                                   onChange={this.inputChange}
                                   name="extraParam"
                                   placeholder={this.state.extraFieldPlaceholder}/>
                        </div>
                    } else {
                        return null;
                    }
                })()}
                {this.state.editing ? <Loader1/> : null}
            </BasicActionModal>
        )
    }
}