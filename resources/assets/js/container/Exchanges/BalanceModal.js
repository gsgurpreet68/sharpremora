import React from 'react';
import BasicActionModal from "../../components/Modal/BasicActionModal";
import Loader1 from "../../components/Loader/Loader1";
import toast from "../../Helpers/Toast/Toast";
import {ajaxErrorHandling} from "../../Helpers/ErrorHandling/AjaxErrorHandling";
import {strCapitalize} from "../../Helpers/Functions/HelpfulFunctions";

export default class BalanceModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            balance: []
        };
        this.getBalance = this.getBalance.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.currentExchange !== nextProps.currentExchange) {
            this.setState({
                exchangeName: nextProps.currentExchange,
                balance: []
            }, () => {
                this.getBalance();
            });
        }
    }

    componentDidMountMount() {
        this.setState({
            exchangeName: this.props.currentExchange
        });
    }

    getBalance() {
        this.setState({
            showLoader: true
        });
        const url = '/api/v1/exchange/get-balance?exchange_name=' + this.state.exchangeName;
        axios.get(url, {
            headers: {
                Authorization: "Bearer " + localStorage.getItem('token')
            }
        }).then(response => {
            const data = response.data;
            this.setState({
                showLoader: false,
                balance: data.data
            });
        }).catch(error => {
            this.setState({
                showLoader: false
            });
            toast(ajaxErrorHandling(error));
            console.log(error.response.data);
        });
    }

    render() {
        let balance = this.state.balance;
        balance = balance.map((e, i) => {
            return <tr key={i}>
                <td>{e.asset}</td>
                <td>{e.free}</td>
                <td>{e.used}</td>
                <td>{e.total}</td>
            </tr>
        });
        const header = 'Balance on ' + (!this.state.showLoader && this.state.exchangeName ?
            'on ' + strCapitalize(this.state.exchangeName) : '');
        return (
            <BasicActionModal isOpen={this.props.showModal}
                              minWidth="25rem"
                              modalClosed={this.props.modalClosed}
                              header={header}>
                {this.state.showLoader ? <Loader1/> : null}
                <table className="table table-striped">
                    <thead>
                    <tr>
                        <th>Asset</th>
                        <th>Free</th>
                        <th>Used</th>
                        <th>Total</th>
                    </tr>
                    </thead>
                    <tbody>{balance}</tbody>
                </table>
                <p className="text-right">
                    {this.state.balance.length > 0 ? '** Only showing assets that have balance.' : ''}
                </p>
            </BasicActionModal>
        )
    }
}