import React from 'react';
import EachCategory from "./EachCategory";
import './scss/categories.scss';
import CreateCategory from "./CreateCategory";
import Loader1 from "../../components/Loader/Loader1";
import EditCategory from "./EditCategory";
import Sad from "../../Helpers/Sad/Sad";

export default class PositionsCategories extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            categories: [],
            currentCategory: {}
        };
        this.getCategories = this.getCategories.bind(this);
        this.editCategory = this.editCategory.bind(this);
    }

    componentDidMount() {
        this.getCategories();
    }

    getCategories() {
        this.setState({
            loadingCategories: true
        });

        axios.get('/api/v1/dashboard/get-categories-names', {
            headers: {
                Authorization: "Bearer " + localStorage.getItem('token')
            }
        }).then(response => {
            let categories = response.data.data;
            this.setState({
                categories: categories,
                categoriesLoaded: true,
                loadingCategories: false
            });
        }).catch(error => {
            this.setState({
                'error': true,
                'errorMsg': 'Couldn\'t load categories'
            });
        });
    }

    editCategory(currentCategory) {
        this.setState({
            currentCategory: currentCategory,
            showEditModal: true
        });
    }

    render() {
        let categories = this.state.categories;
        categories = categories.map((e, i) => {
            return <EachCategory key={i}
                                 category={e}
                                 editCategory={this.editCategory}
                                 categoryDeleted={this.getCategories}/>
        });

        return (
            <div className="app-body" id="categories">
                <div className="padding">
                    <div className="box">
                        <div className="box-header">
                            Position Categories
                        </div>
                        <div className="row">
                            <div className="col-sm-12">
                                <CreateCategory categoryCreated={this.getCategories}/>
                            </div>
                        </div>
                        {/*  edit category modal */}
                        <EditCategory currentCategory={this.state.currentCategory}
                                      editModalClosed={() => this.setState({showEditModal: false})}
                                      showModal={this.state.showEditModal}
                                      categoryEdited={this.getCategories}/>

                        <div className="row">
                            <div className="col-sm-12">
                                <div className="categories-cont">
                                    {this.state.loadingCategories ? <Loader1/> : null}
                                    {(() => {
                                        if (!this.state.loadingCategories && this.state.categories.length === 0) {
                                            return <Sad text='No Category Found'/>
                                        } else {
                                            return <div className="table-responsive">
                                                <table
                                                    className="table table-striped table-hover table-bordered white b-a table-sm">
                                                    <thead>
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>Edit</th>
                                                        <th>Delete</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>{categories}</tbody>
                                                </table>
                                            </div>
                                        }
                                    })()}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}