import React from 'react';
import BasicActionModal from "../../components/Modal/BasicActionModal";
import Loader1 from "../../components/Loader/Loader1";
import {ajaxErrorHandling} from "../../Helpers/ErrorHandling/AjaxErrorHandling";
import Sad from "../../Helpers/Sad/Sad";
import toast from "../../Helpers/Toast/Toast";

export default class CreateCategory extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.openModal = this.openModal.bind(this);
        this.modalActionBtnClicked = this.modalActionBtnClicked.bind(this);
        this.formElemChange = this.formElemChange.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    openModal() {
        this.setState({
            showModal: true
        });
    }

    closeModal() {
        this.setState({
            showModal: false
        });
    }

    modalActionBtnClicked() {
        const data = {
            name: this.state.categoryName
        };
        axios.post('/api/v1/dashboard/create-position-category', data, {
            headers: {
                Authorization: "Bearer " + localStorage.getItem('token')
            },
            onUploadProgress: (progress) => {
                this.setState({
                    creating: true
                });
            }
        }).then(response => {
            this.setState({
                commit_success: true,
                creating: false,
                post_commit_message: response.data.msg
            });
            toast(response.data.msg);
            if (response.data.success) {
                this.props.categoryCreated ? this.props.categoryCreated() : null;
                this.setState({
                    showModal: false
                });
            }
        }).catch(error => {
            console.log(error);
            this.setState({
                show_commit_status: true,
                commit_success: false,
                creating: false,
                post_commit_message: ajaxErrorHandling(error)
            }, () => {
                toast(ajaxErrorHandling(error));
            });
        });
    }

    formElemChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    render() {
        return (
            <div id="create-category">
                <div className="pull-right">
                    <button className="btn btn-primary btn-sm"
                            id="create-category-btn"
                            onClick={this.openModal}>
                        Create Category
                    </button>
                </div>
                <BasicActionModal isOpen={this.state.showModal}
                                  header='Create Position Category'
                                  modalClosed={this.closeModal}
                                  actionBtn={true}
                                  actionBtnName='Create'
                                  actionBtnClass='btn-primary'
                                  disabledActionBtn={this.state.creating}
                                  actionBtnClicked={this.modalActionBtnClicked}>
                    <form>
                        <div className="form-group">
                            <input type="text"
                                   name="categoryName"
                                   onChange={this.formElemChange}
                                   placeholder="Category Name"
                                   className="form-control"/>
                        </div>
                    </form>
                    {this.state.creating ? <Loader1/> : null}
                </BasicActionModal>
            </div>
        )
    }
}