import React from 'react';
import toast from "../../Helpers/Toast/Toast";
import {ajaxErrorHandling} from "../../Helpers/ErrorHandling/AjaxErrorHandling";
import Loader1 from "../../components/Loader/Loader1";

export default class EachCategory extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.editCategory = this.editCategory.bind(this);
        this.deleteCategory = this.deleteCategory.bind(this);
    }

    editCategory() {
        this.props.editCategory ? this.props.editCategory(this.props.category) : null;
    }

    deleteCategory() {
        const confirm = window.confirm(`Are you sure that you want to delete? Note: this can't be undone.`);
        if (confirm) {
            const data = {
                position_category_id: this.props.category.id
            };
            axios.post('/api/v1/dashboard/delete-position-category', data, {
                headers: {
                    Authorization: "Bearer " + localStorage.getItem('token')
                },
                onUploadProgress: (progress) => {
                    this.setState({
                        deleting: true
                    });
                }
            }).then(response => {
                this.setState({
                    commit_success: true,
                    deleting: false,
                    post_commit_message: response.data.msg
                });
                toast(response.data.msg);
                if (response.data.success) {
                    this.props.categoryDeleted ? this.props.categoryDeleted() : null;
                }
            }).catch(error => {
                console.log(error);
                this.setState({
                    show_commit_status: true,
                    commit_success: false,
                    deleting: false,
                    post_commit_message: ajaxErrorHandling(error)
                }, () => {
                    toast(ajaxErrorHandling(error));
                });
            });
        }
    }

    render() {
        return (
            <tr>
                <td>{this.props.category.name}</td>
                <td className="pointer">
                    <button className="btn btn-primary btn-sm"
                            onClick={this.editCategory}>
                        <i className="fa fa-edit"/> Edit
                    </button>
                </td>
                <td className="pointer">
                    <button className="btn btn-danger btn-sm"
                            onClick={this.deleteCategory}
                            disabled={this.state.deleting}>
                        {this.state.deleting ? <Loader1/> : <span>
                            <i className="fa fa-trash"/> Delete
                        </span>}
                    </button>
                </td>
            </tr>
        )
    }
}