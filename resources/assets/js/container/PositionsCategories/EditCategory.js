import React from 'react';
import BasicActionModal from "../../components/Modal/BasicActionModal";
import toast from "../../Helpers/Toast/Toast";
import {ajaxErrorHandling} from "../../Helpers/ErrorHandling/AjaxErrorHandling";
import Loader1 from "../../components/Loader/Loader1";

export default class EditCategory extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.changeName = this.changeName.bind(this);
        this.modalAction = this.modalAction.bind(this);
        this.editCategory = this.editCategory.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.currentCategory !== this.props.currentCategory) {
            this.setState({
                categoryName: nextProps.currentCategory.name
            });
        }

        if (nextProps.showModal !== this.props.showModal) {
            this.setState({
                showModal: nextProps.showModal
            });
        }
    }

    componentDidMount() {
        this.setState({
            categoryName: this.props.currentCategory.name,
            showModal: this.props.showModal
        });
    }

    changeName(e) {
        this.setState({
            categoryName: e.target.value,
            disabledActionBtn: false
        });
    }

    modalAction(open) {
        this.setState({
            showModal: open
        });

        if (!open) {
            this.props.editModalClosed ? this.props.editModalClosed() : null;
        }
    }

    editCategory() {
        const data = {
            name: this.state.categoryName,
            position_category_id: this.props.currentCategory.id
        };
        axios.post('/api/v1/dashboard/edit-position-category', data, {
            headers: {
                Authorization: "Bearer " + localStorage.getItem('token')
            },
            onUploadProgress: (progress) => {
                this.setState({
                    editing: true
                });
            }
        }).then(response => {
            this.setState({
                commit_success: true,
                editing: false,
                post_commit_message: response.data.msg
            });
            toast(response.data.msg);
            if (response.data.success) {
                this.props.categoryEdited ? this.props.categoryEdited() : null;
                this.props.editModalClosed ? this.props.editModalClosed() : null;
                this.setState({
                    showModal: false
                });
            }
        }).catch(error => {
            console.log(error);
            this.setState({
                show_commit_status: true,
                commit_success: false,
                editing: false,
                post_commit_message: ajaxErrorHandling(error)
            }, () => {
                toast(ajaxErrorHandling(error));
            });
        });
    }

    render() {
        return (
            <BasicActionModal isOpen={this.state.showModal}
                              header='Edit Category'
                              modalClosed={() => this.modalAction(false)}
                              actionBtn={true}
                              actionBtnName='Save'
                              disabledActionBtn={this.state.editing}
                              actionBtnClass='btn-primary'
                              actionBtnClicked={this.editCategory}>
                <form>
                    <div className="form-group">
                        <input type="text"
                               className="form-control"
                               name="categoryName"
                               onChange={this.changeName}
                               value={this.state.categoryName}
                               placeholder='New name'/>
                    </div>
                </form>
                {this.state.editing ? <Loader1/> : null}
            </BasicActionModal>
        )
    }
}