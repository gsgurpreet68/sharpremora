import React from 'react';
import axios from 'axios';

class Settings extends React.Component{

    constructor(){
        super();
        this.state = {
            first_name: '',
            last_name: '',
            failed: false,
            ack: ''
        };
        this.handleUpdate = this.handleUpdate.bind(this);
    }

    async handleUpdate() {
        const { first_name, last_name } = this.state;
        const res = await axios.post('api/v1/update/profile', {
            first_name,
            last_name
        }, {
            headers: {
                Authorization: "Bearer " + localStorage.getItem('token')
            }
        });
        const data = await res.data;
        console.log(data);
    }

    render(){
        return (
            <div className="app-body">
                <div className="row no-gutter">
                    <div className="col-sm-3 col-lg-2">
                        <div className="p-y">
                        <div className="nav-active-border left b-primary">
                            <ul className="nav nav-sm flex-column">
                                <li className="nav-item">
                                    <a className="nav-link block active" data-toggle="tab" data-target="#tab-1">Profile</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link block" data-toggle="tab" data-target="#tab-2">Account Settings</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link block" data-toggle="tab" data-target="#tab-3">Emails</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link block" data-toggle="tab" data-target="#tab-4">Notifications</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link block" data-toggle="tab" data-target="#tab-5">Security</a>
                                </li>
                            </ul>
                        </div>
                        </div>
                    </div>
                    <div className="col-sm-9 col-lg-10 light lt">
                        <div className="tab-content pos-rlt">
                        <div className="tab-pane active" id="tab-1">
                            {/* <div className="p-a-md dker _600">Public profile</div> */}
                            <form className="p-a-md col-md-6">
                            {/* <div className="form-group">
                                <label>Profile picture</label>
                                <div className="form-file">
                                <input type="file" />
                                <button className="btn white">Upload new picture</button>
                                </div>
                            </div> */}
                            <div className="form-group">
                                <label>First Name</label>
                                <input type="text" className="form-control" />
                            </div>
                            <div className="form-group">
                                <label>Last Name</label>
                                <input type="text" className="form-control" />
                            </div>
                            {/* <div className="form-group">
                                <label>URL</label>
                                <input type="text" className="form-control" />
                            </div>
                            <div className="form-group">
                                <label>Company</label>
                                <input type="text" className="form-control" />
                            </div>
                            <div className="form-group">
                                <label>Location</label>
                                <input type="text" className="form-control" />
                            </div>
                            <div className="checkbox">
                                <label className="ui-check">
                                <input type="checkbox" /><i className="dark-white"></i> Available for hire
                                </label>
                            </div> */}
                            <button onClick={() => this.handleUpdate()} type="button" className="btn btn-info m-t">Update</button>
                            </form>
                        </div>
                        <div className="tab-pane" id="tab-2">
                            <div className="p-a-md dker _600">Account settings</div>
                            <form role="form" className="p-a-md col-md-6">
                            <div className="form-group">
                                <label>Client ID</label>
                                <input type="text" disabled className="form-control" value="d6386c0651d6380745846efe300b9869" />
                            </div>
                            <div className="form-group">
                                <label>Secret Key</label>
                                <input type="text" disabled className="form-control" value="3f9573e88f65787d86d8a685aeb4bd13" />
                            </div>
                            <div className="form-group">
                                <label>App Name</label>
                                <input type="text" className="form-control" />
                            </div>
                            <div className="form-group">
                                <label>App URL</label>
                                <input type="text" className="form-control" />
                            </div>
                            <button type="submit" className="btn btn-info m-t">Update</button>
                            </form>
                        </div>
                        <div className="tab-pane" id="tab-3">
                            <div className="p-a-md dker _600">Emails</div>
                            <form role="form" className="p-a-md col-md-6">
                            <p>E-mail me whenever</p>
                            <div className="checkbox">
                                <label className="ui-check">
                                <input type="checkbox" /><i className="dark-white"></i> Anyone posts a comment
                                </label>
                            </div>
                            <div className="checkbox">
                                <label className="ui-check">
                                <input type="checkbox" /><i className="dark-white"></i> Anyone follow me
                                </label>
                            </div>
                            <div className="checkbox">
                                <label className="ui-check">
                                <input type="checkbox" /><i className="dark-white"></i> Anyone send me a message
                                </label>
                            </div>
                            <div className="checkbox">
                                <label className="ui-check">
                                <input type="checkbox" /><i className="dark-white"></i> Anyone invite me to group
                                </label>
                            </div>
                            <button type="submit" className="btn btn-info m-t">Update</button>
                            </form>
                        </div>
                        <div className="tab-pane" id="tab-4">
                            <div className="p-a-md dker _600">Notifications</div>
                            <form role="form" className="p-a-md col-md-6">
                            <p>Notice me whenever</p>
                            <div className="checkbox">
                                <label className="ui-check">
                                <input type="checkbox" /><i className="dark-white"></i> Anyone seeing my profile page
                                </label>
                            </div>
                            <div className="checkbox">
                                <label className="ui-check">
                                <input type="checkbox" /><i className="dark-white"></i> Anyone follow me
                                </label>
                            </div>
                            <div className="checkbox">
                                <label className="ui-check">
                                <input type="checkbox" /><i className="dark-white"></i> Anyone send me a message
                                </label>
                            </div>
                            <div className="checkbox">
                                <label className="ui-check">
                                <input type="checkbox" /><i className="dark-white"></i> Anyone invite me to group
                                </label>
                            </div>
                            <button type="submit" className="btn btn-info m-t">Update</button>
                            </form>
                        </div>
                        <div className="tab-pane" id="tab-5">
                            <div className="p-a-md dker _600">Security</div>
                            <div className="p-a-md">
                            <div className="clearfix m-b-lg">
                                <form role="form" className="col-md-6 p-a-0">
                                <div className="form-group">
                                    <label>Old Password</label>
                                    <input type="password" className="form-control" />
                                </div>
                                <div className="form-group">
                                    <label>New Password</label>
                                    <input type="password" className="form-control" />
                                </div>
                                <div className="form-group">
                                    <label>New Password Again</label>
                                    <input type="password" className="form-control" />
                                </div>
                                <button type="submit" className="btn btn-info m-t">Update</button>
                                </form>
                            </div>

                            <p><strong>Delete account?</strong></p>
                            <button type="submit" className="btn btn-danger m-t" data-toggle="modal" data-target="#modal">Delete Account</button>

                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Settings;