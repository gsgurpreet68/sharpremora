import React from 'react';

export default class EachPosition extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.addPositionIntoCategory = this.addPositionIntoCategory.bind(this);
        this.closePosition = this.closePosition.bind(this);
    }

    addPositionIntoCategory() {
        this.props.addPositionIntoCategory ? this.props.addPositionIntoCategory(this.props.position) : null;
    }

    closePosition() {
        this.props.closePosition ? this.props.closePosition(this.props.position) : null;
    }

    render() {
        return (
            <tr>
                <td>{this.props.position.symbol}</td>
                <td className="actions" nowrap="nowrap">
                    <i className="fa fa-cogs pointer text-info"
                       data-toggle="tooltip"
                       onClick={this.addPositionIntoCategory}
                       title="Add position into categories"/>&nbsp;
                    <i className="fa fa-asterisk pointer text-primary"
                       data-toggle="tooltip"
                       title="Risk Management"/>&nbsp;
                    <i className="fa fa-times-circle text-danger pointer"
                       onClick={this.closePosition}
                       data-toggle="tooltip" title="Close Position"/>
                </td>
                <td>
                    {this.props.position.entry} {this.props.position.price_currency}
                </td>
                <td className="text-capitalize">{this.props.position.exchange_name}</td>
                <td>{this.props.position.quantity}</td>
                <td>{}</td>
                <td></td>
                <td></td>
            </tr>
        )
    }
}