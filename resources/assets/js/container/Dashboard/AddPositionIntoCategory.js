import React from 'react';
import BasicActionModal from "../../components/Modal/BasicActionModal";
import toast from "../../Helpers/Toast/Toast";
import {ajaxErrorHandling} from "../../Helpers/ErrorHandling/AjaxErrorHandling";
import Sad from "../../Helpers/Sad/Sad";
import Loader1 from "../../components/Loader/Loader1";

export default class AddPositionIntoCategory extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            categories: [],
            showModal: false,
            currentPosition: {},
            positionCategoryId: 0
        };
        this.getCategories = this.getCategories.bind(this);
        this.modalClosed = this.modalClosed.bind(this);
        this.changeCategory = this.changeCategory.bind(this);
        this.addIntoCategory = this.addIntoCategory.bind(this);
    }

    getCategories() {
        axios.get('/api/v1/dashboard/get-categories-names', {
            headers: {
                Authorization: "Bearer " + localStorage.getItem('token')
            },
            onDownloadProgress: () => {
                this.setState({
                    loadingCategories: true
                });
            }
        }).then(response => {
            let categories = response.data.data;
            this.setState({
                categories: categories,
                categoriesLoaded: true,
                loadingCategories: false
            });
        }).catch(error => {
            toast(ajaxErrorHandling(error));
        });
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.currentPosition !== this.props.currentPosition) {
            this.setState({
                currentPosition: nextProps.currentPosition
            });
        }

        if (nextProps.showCategoryModal !== this.props.showCategoryModal) {
            this.setState({
                showModal: nextProps.showCategoryModal
            });
        }
    }

    componentDidMount() {
        this.getCategories();
        this.setState({
            currentPosition: this.props.currentPosition,
            showModal: this.props.showCategoryModal
        });
    }

    modalClosed() {
        this.setState({showModal: false});
        this.props.modalClosed ? this.props.modalClosed() : null;
    }

    changeCategory(e) {
        this.setState({
            positionCategoryId: e.target.value
        });
    }

    addIntoCategory() {
        const data = {
            position_id: this.state.currentPosition.user_position_id,
            position_category_id: this.state.positionCategoryId
        };
        axios.post('/api/v1/dashboard/add-position-into-category', data, {
            headers: {
                Authorization: "Bearer " + localStorage.getItem('token')
            },
            onUploadProgress: (progress) => {
                this.setState({
                    adding: true
                });
            }
        }).then(response => {
            this.setState({
                commit_success: true,
                adding: false,
                post_commit_message: response.data.msg
            });
            toast(response.data.msg);
            if (response.data.success) {
                this.props.addedIntoCategory ? this.props.addedIntoCategory() : null;
                this.props.modalClosed ? this.props.modalClosed() : null;
                this.setState({
                    showModal: false
                });
            }
        }).catch(error => {
            console.log(error);
            this.setState({
                adding: false
            }, () => {
                toast(ajaxErrorHandling(error));
            });
        });
    }

    render() {
        let categories = this.state.categories;
        categories = categories.map((e, i) => {
            return <option value={e.id}
                           key={i}>{e.name}</option>
        });
        return (
            <BasicActionModal isOpen={this.state.showModal}
                              header='Add into category'
                              actionBtn={true}
                              actionBtnName='Add'
                              actionBtnClass='btn-primary'
                              actionBtnClicked={this.addIntoCategory}
                              disabledActionBtn={this.state.positionCategoryId === 0 || this.state.adding}
                              modalClosed={this.modalClosed}>
                <form>
                    <div className="form-group">
                        {(() => {
                            if (this.state.categoriesLoaded && this.state.categories.length === 0) {
                                return <Sad text="No category found"/>
                            } else {
                                return <select name="category"
                                               id="category"
                                               onChange={this.changeCategory}
                                               className="form-control">
                                    <option value="0">Select a category</option>
                                    {categories}
                                </select>
                            }
                        })()}
                        {this.state.loadingCategories || this.state.adding ? <Loader1/> : null}
                    </div>
                </form>
            </BasicActionModal>
        )
    }
}