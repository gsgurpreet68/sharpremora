import React from 'react';
import BasicActionModal from "../../components/Modal/BasicActionModal";
import {ajaxErrorHandling} from "../../Helpers/ErrorHandling/AjaxErrorHandling";
import toast from "../../Helpers/Toast/Toast";
import Loader1 from "../../components/Loader/Loader1";

export default class ClosePositionModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            exit: '100'
        };
        this.closePosition = this.closePosition.bind(this);
        this.changeExitSize = this.changeExitSize.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.showModal !== this.props.showModal) {
            this.setState({
                showModal: nextProps.showModal
            });
        }

        if (nextProps.currentPosition !== this.props.currentPosition) {
            this.setState({
                currentPosition: nextProps.currentPosition,
                exit: '100'
            });
        }
    }

    componentDidMount() {
        this.setState({
            showModal: this.props.showModal,
            currentPosition: this.props.currentPosition
        });
    }

    changeExitSize(e) {
        this.setState({
            exit: e.target.value
        });
    }

    closePosition() {
        const data = {
            user_position_id: this.state.currentPosition.user_position_id,
            exit_size: this.state.exit
        };
        this.setState({
            closing: true
        });
        axios.post('/api/v1/position/close', data, {
            headers: {
                Authorization: "Bearer " + localStorage.getItem('token')
            }
        }).then(response => {
            this.setState({
                closing: false
            });
            toast(response.data.msg);
            if (response.data.success) {
                this.props.positionClosed ? this.props.positionClosed() : null;
                this.props.modalClosed ? this.props.modalClosed() : null;
                this.setState({
                    showModal: false
                });
            }
        }).catch(error => {
            console.log(error);
            this.setState({
                closing: false
            }, () => {
                toast(ajaxErrorHandling(error));
            });
        });
    }

    render() {
        return (
            <BasicActionModal isOpen={this.state.showModal}
                              minWidth="30%"
                              hideCross={true}
                              actionBtn={true}
                              actionBtnName="Close"
                              actionBtnClicked={this.closePosition}
                              actionBtnDisabled={this.state.closing}
                              actionBtnClass="btn-danger"
                              modalClosed={this.props.modalClosed}
                              header="Close Position">
                <label className="md-check close-label">
                    <input type="radio"
                           name="exit"
                           onChange={this.changeExitSize}
                           value="25"/>
                    <i className="blue"/> 25%
                </label>
                <label className="md-check close-label">
                    <input type="radio"
                           name="exit"
                           onChange={this.changeExitSize}
                           value="50"/>
                    <i className="blue"/> 50%
                </label>
                <label className="md-check close-label">
                    <input type="radio"
                           name="exit"
                           onChange={this.changeExitSize}
                           value="75"/>
                    <i className="blue"/> 75%
                </label>
                <label className="md-check close-label">
                    <input type="radio"
                           name="exit"
                           checked={this.state.exit === '100'}
                           onChange={this.changeExitSize}
                           value="100"/>
                    <i className="blue"/> 100%
                </label> <br/> <br/>
                {this.state.closing ? <Loader1/> : null}
            </BasicActionModal>
        )
    }
}