import React from 'react';
import {Tab, Tabs, TabList, TabPanel} from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import './Dashboard.css';
import './scss/dashboard.scss';
import Positions from "./Positions";
import Sad from "../../Helpers/Sad/Sad";
import toast from "../../Helpers/Toast/Toast";
import {ajaxErrorHandling} from "../../Helpers/ErrorHandling/AjaxErrorHandling";
import Loader1 from "../../components/Loader/Loader1";
import RemoraHubModal from "../../components/Modal/RemoraHubModal";

export default class Dashboard extends React.Component {
    constructor() {
        super();
        this.state = {
            positions: [],
            categories: [],
            showingPositions: [],
            categoryName: 'all'
        };
        this.tabSelected = this.tabSelected.bind(this);
        this.modalClosed = this.modalClosed.bind(this);
    }

    componentDidMount() {
        this.getPositions();
        this.getCategoriesNames();
    }

    getCategoriesNames() {
        this.setState({
            loadingCategories: true
        });

        axios.get('/api/v1/dashboard/get-categories-names', {
            headers: {
                Authorization: "Bearer " + localStorage.getItem('token')
            }
        }).then(response => {
            let categories = response.data.data;
            categories.unshift({'name': 'All'});
            categories.push({'name': 'Uncategorized'});
            this.setState({
                categories: categories,
                categoriesLoaded: true,
                loadingCategories: false
            }, () => {
                this.getPositions();
            });
        }).catch(error => {
            this.setState({
                'error': true,
                'errorMsg': 'Couldn\'t load categories'
            });
        });
    }

    getPositions() {
        this.setState({
            loadingPositions: true
        });
        const url = '/api/v1/dashboard/get-open-positions?category_name=' + this.state.categoryName;
        axios.get(url, {
            headers: {
                Authorization: "Bearer " + localStorage.getItem('token')
            }
        }).then(response => {
            const data = response.data.data;
            if (response.data.success) {
                this.setState({
                    positions: data,
                    showingPositions: data,
                    loadingPositions: false
                });
            } else {
                this.setState({positions: []});
            }
        }).catch(error => {
            this.setState({
                'error': true,
                'errorMsg': 'Couldn\'t load positions',
                loadingPositions: false
            });
            toast(ajaxErrorHandling(error));
        })
    }

    tabSelected(index) {
        const categories = this.state.categories;
        let categoryName;
        if (index === 0) {
            categoryName = 'all';
        } else {
            categoryName = categories[index]['name'].toLowerCase();
        }
        this.setState({
            categoryName: categoryName
        }, () => {
            this.getPositions();
        });
    }

    modalClosed() {
        this.getPositions();
    }

    render() {
        let tabs = this.state.categories;
        tabs = tabs.map((e, i) => {
            return <Tab key={i}>{e.name}</Tab>
        });

        let tabPanels = this.state.categories;
        tabPanels = tabPanels.map((e, i) => {
            return <TabPanel key={i}>
                <Positions showingPositions={this.state.positions}
                           addedIntoCategory={this.getPositions}
                           modalClosed={this.modalClosed}
                           loadingPositions={this.state.loadingPositions}/>
            </TabPanel>
        });
        return (
            <div className="app-body" id="view">
                <div className="padding">
                    <div className="box">
                        <div className="box-header container">
                            <div className="row">
                                <div className="col-sm-6">
                                    <h2 className="mb-0 _300">Open Positions</h2>
                                    <small className="text-muted">Current performance of executed orders</small>
                                </div>
                                <div className="col-sm-6 grey-200">
                                    <div className="row padding">
                                        <div className="col-sm-4">
                                            <h4>Portfolio Size</h4>
                                            <small className="text-muted">$258,386</small>
                                        </div>
                                        <div className="col-sm-4">
                                            <h4>Change</h4>
                                            <small className="text-muted">+10% (24 hours) -5% (1 Week)</small>
                                        </div>
                                        <div className="col-sm-4">
                                            <h4>Distribution</h4>
                                            <p>
                                                <small className="text-muted">25% Crypto</small>
                                                <small className="text-muted">30% Stocks</small>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-12">
                            <div id="positions-cont">
                                {this.state.loadingCategories ? <Loader1/> : null}
                                {(() => {
                                    if (this.state.categoriesLoaded) {
                                        return <Tabs onSelect={this.tabSelected}>
                                            <TabList>
                                                {tabs}
                                            </TabList>
                                            {tabPanels}
                                        </Tabs>
                                    } else {
                                        return null;
                                    }
                                })()}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}