import React from 'react';
import EachPosition from "./EachPosition";
import Sad from "../../Helpers/Sad/Sad";
import AddPositionIntoCategory from "./AddPositionIntoCategory";
import Loader1 from "../../components/Loader/Loader1";
import ClosePositionModal from "./ClosePositionModal";

export default class Positions extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            positions: [],
            currentPosition: {}
        };
        this.addPositionIntoCategory = this.addPositionIntoCategory.bind(this);
        this.positionCategoryModalClosed = this.positionCategoryModalClosed.bind(this);
        this.closePosition = this.closePosition.bind(this);
        this.closeModalClosed = this.closeModalClosed.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.showingPositions !== nextProps.showingPositions) {
            this.setState({
                positions: nextProps.showingPositions
            });
        }
        if (this.state.loadingPositions !== nextProps.loadingPositions) {
            this.setState({
                loadingPositions: nextProps.loadingPositions
            });
        }
    }

    componentDidMount() {
        this.setState({
            positions: this.props.showingPositions,
            loadingPositions: this.props.loadingPositions
        });
    }

    addPositionIntoCategory(position) {
        this.setState({
            currentPosition: position,
            showCategoryModal: true
        });
    }

    positionCategoryModalClosed() {
        this.setState({showCategoryModal: false});
        this.props.modalClosed ? this.props.modalClosed() : null;
    }

    closePosition(position) {
        this.setState({
            currentPositionClose: position,
            showCloseModal: true
        });
    }

    closeModalClosed() {
        this.setState({
            showCloseModal: false
        });
        this.props.modalClosed ? this.props.modalClosed() : null;
    }

    render() {
        let positions = this.state.positions;
        positions = positions.map((e, i) => {
            return <EachPosition key={i}
                                 position={e}
                                 closePosition={this.closePosition}
                                 addPositionIntoCategory={this.addPositionIntoCategory}/>
        });

        return (
            <div id="positions">
                {this.state.loadingPositions ? <Loader1/> : null}
                {(() => {
                    if (this.state.positions.length === 0 && !this.state.loadingPositions) {
                        return <Sad text={'No position found'}/>
                    } else {
                        return <div>
                            <AddPositionIntoCategory currentPosition={this.state.currentPosition}
                                                     modalClosed={this.positionCategoryModalClosed}
                                                     showCategoryModal={this.state.showCategoryModal}/>

                            <ClosePositionModal showModal={this.state.showCloseModal}
                                                modalClosed={this.closeModalClosed}
                                                currentPosition={this.state.currentPositionClose}/>
                            <div className="table-responsive">
                                <table className="table table-striped table-hover table-bordered white b-a table-sm">
                                    <thead>
                                    <tr>
                                        <th scope="col" width="20%">Asset</th>
                                        <th scope="col">Actions</th>
                                        <th scope="col">Entry</th>
                                        <th scope="col">Exchange</th>
                                        <th scope="col">Size</th>
                                        <th scope="col">Capital</th>
                                        <th scope="col">Position</th>
                                        <th scope="col">P/L</th>
                                        <th scope="col">Fees</th>
                                        <th scope="col">ROI</th>
                                    </tr>
                                    </thead>
                                    <tbody>{positions}</tbody>
                                </table>
                            </div>
                        </div>
                    }
                })()}
            </div>
        )
    }
}