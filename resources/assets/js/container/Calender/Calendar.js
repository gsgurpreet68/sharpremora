import React from 'react';
import toast from "../../Helpers/Toast/Toast";
import {ajaxErrorHandling} from "../../Helpers/ErrorHandling/AjaxErrorHandling";
import Loader1 from "../../components/Loader/Loader1";
import './scss/calendar.scss';
import {strCapitalize} from "../../Helpers/Functions/HelpfulFunctions";
import Sad from "../../Helpers/Sad/Sad";

export default class Calendar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            orders: [],
            openedPositions: [],
            closedPositions: []
        };
        this.getOrders = this.getOrders.bind(this);
        this.getPositions = this.getPositions.bind(this);
    }

    getOrders() {
        this.setState({
            gettingOrders: true
        });
        const url = '/api/v1/orders?status=all&&order_time_flow=desc&page=1&from_date=' +
            this.state.currentDate + '&to_date=' + this.state.currentDate;
        axios.get(url, {
            headers: {
                Authorization: "Bearer " + localStorage.getItem('token')
            }
        }).then(response => {
            let orders = response.data.data;
            this.setState({
                orders: orders,
                ordersLoaded: true,
                gettingOrders: false
            });
        }).catch(error => {
            this.setState({
                gettingOrders: false
            });
            toast(ajaxErrorHandling(error, '\n'));
        });
    }

    getPositions() {
        const url = '/api/v1/calendar/positions?date=' + this.state.currentDate;
        this.setState({
            gettingPositions: true
        });
        axios.get(url, {
            headers: {
                Authorization: "Bearer " + localStorage.getItem('token')
            }
        }).then(response => {
            let positions = response.data.data;
            this.setState({
                closedPositions: positions.closedPositions,
                openedPositions: positions.openedPositions,
                positionsLoaded: true,
                gettingPositions: false
            });
        }).catch(error => {
            this.setState({
                gettingPositions: false
            });
            toast(ajaxErrorHandling(error, '\n'));
        });
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.match.params.value !== this.props.match.params.value) {
            this.setState({
                currentDate: nextProps.match.params.value,
                ordersLoaded: false,
                positionsLoaded: false
            }, () => {
                this.getOrders();
                this.getPositions();
            });
        }
    }

    componentDidMount() {
        this.setState({
            currentDate: this.props.match.params.value
        }, () => {
            this.getOrders();
            this.getPositions();
        });
    }

    render() {
        let orders = this.state.orders;
        orders = orders.map((e, i) => {
            return <tr key={i}>
                <td>{e.symbol}</td>
                <td>{e.exchange_name}</td>
                <td>{e.status}</td>
                <td>{e.side}</td>
                <td>{e.type}</td>
                <td>{e.size}</td>
                <td>{e.price}</td>
                <td>{e.ordered_at}</td>
            </tr>
        });

        let openedPositions = this.state.openedPositions;
        openedPositions = openedPositions.map((e, i) => {
            return <li key={i}>
                {e.quantity} of {e.ccxt_symbol} on {strCapitalize(e.exchange_name)}
            </li>
        });

        let closedPositions = this.state.closedPositions;
        closedPositions = closedPositions.map((e, i) => {
            return <li key={i}>
                {e.quantity} of {e.ccxt_symbol} on {strCapitalize(e.exchange_name)}
            </li>
        });
        return (
            <div id="calendar" className="app-body">
                <div className="padding">
                    <div className="box">
                        <div className="box-header">
                            <h2 className="main-header">
                                {this.state.currentDate}
                            </h2>
                        </div>
                        {this.state.gettingOrders || this.state.gettingPositions ? <Loader1/> : null}
                        <div className="row">
                            {(() => {
                                if (this.state.ordersLoaded && this.state.orders.length !== 0) {
                                    return <div className="col-md-12">
                                        <div className="container">
                                            <h3 className="h3">Orders</h3>
                                        </div>
                                        <div className="table-responsive">
                                            <table
                                                className="table table-striped table-hover table-bordered white b-a table-sm">
                                                <thead>
                                                <tr>
                                                    <th>Symbol</th>
                                                    <th>Exchange</th>
                                                    <th>Status</th>
                                                    <th>Side</th>
                                                    <th>Type</th>
                                                    <th>Size</th>
                                                    <th>Price</th>
                                                    <th>Time</th>
                                                </tr>
                                                </thead>
                                                <tbody>{orders}</tbody>
                                            </table>
                                        </div>
                                    </div>
                                } else {
                                    return null;
                                }
                            })()}

                            {(() => {
                                if (this.state.positionsLoaded && this.state.openedPositions.length !== 0) {
                                    return <div className="col-md-3">
                                        <div className="container">
                                            <ul className="open-positions-cont">
                                                <li>Opened</li>
                                                {openedPositions}
                                            </ul>
                                        </div>
                                    </div>
                                } else {
                                    return null;
                                }
                            })()}

                            {(() => {
                                if (this.state.positionsLoaded && this.state.closedPositions.length !== 0) {
                                    return <div className="col-md-3">
                                        <div className="container">
                                            <ul className="open-positions-cont">
                                                <li>Closed</li>
                                                {closedPositions}
                                            </ul>
                                        </div>
                                    </div>
                                } else {
                                    return null;
                                }
                            })()}
                            {(() => {
                                if (this.state.orders.length === 0
                                    && this.state.openedPositions.length === 0
                                    && !this.state.gettingPositions
                                    && this.state.closedPositions) {
                                    return <div className="col-md-12">
                                        <div className="container">
                                            <Sad text="No activity on this day"/>
                                        </div>
                                    </div>
                                } else {
                                    return null;
                                }
                            })()}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}