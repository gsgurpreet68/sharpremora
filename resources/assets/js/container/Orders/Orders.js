import React from 'react';
import './scss/orders.scss';
import toast from "../../Helpers/Toast/Toast";
import {ajaxErrorHandling} from "../../Helpers/ErrorHandling/AjaxErrorHandling";
import EachOrder from "./EachOrder";
import Loader1 from "../../components/Loader/Loader1";
import Sad from "../../Helpers/Sad/Sad";
import Pagination from "../../Helpers/Pagination/Pagination";
import ReactDatePicker from 'react-datetime';
import 'react-datetime/css/react-datetime.css';

export default class Orders extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            orders: [],
            currentStatus: 'all',
            currentOrderTimeFlow: 'desc',
            currentPage: 1,
            showDateRange: false,
            ordersFromDate: '',
            ordersToDate: ''
        };
        this.getOrders = this.getOrders.bind(this);
        this.changeOrderStatus = this.changeOrderStatus.bind(this);
        this.changeOrderTimeFlow = this.changeOrderTimeFlow.bind(this);
        this.pageChanged = this.pageChanged.bind(this);
        this.toggleDateRange = this.toggleDateRange.bind(this);
        this.pickFromDate = this.pickFromDate.bind(this);
        this.pickToDate = this.pickToDate.bind(this);
    }

    getOrders() {
        this.setState({
            gettingOrders: true
        });
        const url = '/api/v1/orders?status=' + this.state.currentStatus +
            '&order_time_flow=' + this.state.currentOrderTimeFlow +
            '&page=' + this.state.currentPage + '&from_date=' + this.state.ordersFromDate +
            '&to_date=' + this.state.ordersToDate;
        axios.get(url, {
            headers: {
                Authorization: "Bearer " + localStorage.getItem('token')
            }
        }).then(response => {
            let orders = response.data.data;
            this.setState({
                orders: orders,
                ordersLoaded: true,
                pageCount: response.data.pageCount,
                gettingOrders: false
            });
        }).catch(error => {
            this.setState({
                gettingOrders: false
            });
            toast(ajaxErrorHandling(error, '\n'));
        });
    }

    componentDidMount() {
        this.getOrders();
    }

    changeOrderStatus(status) {
        this.setState({
            currentStatus: status,
            currentPage: 1
        }, () => {
            this.getOrders();
        });
    }

    changeOrderTimeFlow(e) {
        const flow = e.target.value;
        if (this.state.currentOrderTimeFlow !== flow) {
            this.setState({
                currentOrderTimeFlow: e.target.value
            }, () => {
                this.getOrders();
            });
        }
    }

    pageChanged(page) {
        this.setState({
            currentPage: page
        }, () => {
            this.getOrders();
        });
    }

    toggleDateRange() {
        this.setState({
            showDateRange: !this.state.showDateRange
        }, () => {
            if (!this.state.showDateRange) {
                this.setState({
                    ordersFromDate: '',
                    ordersToDate: ''
                }, () => {
                    this.getOrders();
                });
            }
        });
    }

    pickFromDate(date) {
        this.setState({
            ordersFromDate: date.format('YYYY-MM-DD')
        }, () => {
            if (this.state.ordersToDate) {
                this.getOrders();
            }
        });
    }

    pickToDate(date) {
        this.setState({
            ordersToDate: date.format('YYYY-MM-DD')
        }, () => {
            if (this.state.ordersFromDate) {
                this.getOrders();
            }
        });
    }

    render() {
        let orders = this.state.orders;
        orders = orders.map((e, i) => {
            return <EachOrder key={i}
                              currentStatus={this.state.currentStatus}
                              orderCancelled={this.getOrders}
                              order={e}/>
        });
        return (
            <div className="app-body" id="orders">
                <div className="padding">
                    <div className="box">
                        <div className="box-header">
                            <h2 className='mb-0 _300'>Orders</h2>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="filter-cont">
                                <div className="row">
                                    <div className="col-md-5">
                                        <ul className="filter-list">
                                            <li className='status-txt'>Status:</li>
                                            <li>
                                            <span
                                                className={'label label-default ' + (this.state.currentStatus === 'all' ? 'active' : '')}
                                                onClick={() => this.changeOrderStatus('all')}>
                                                All
                                            </span>
                                            </li>
                                            <li>
                                            <span
                                                className={'label label-default ' + (this.state.currentStatus === 'open' ? 'active' : '')}
                                                onClick={() => this.changeOrderStatus('open')}>
                                                Open
                                            </span>
                                            </li>
                                            <li>
                                            <span
                                                className={'label label-default ' + (this.state.currentStatus === 'filled' ? 'active' : '')}
                                                onClick={() => this.changeOrderStatus('filled')}>
                                                Filled
                                            </span>
                                            </li>
                                            <li>
                                            <span
                                                className={'label label-default ' + (this.state.currentStatus === 'cancelled' ? 'active' : '')}
                                                onClick={() => this.changeOrderStatus('cancelled')}>
                                                Cancelled
                                            </span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="col-md-5">
                                        <ul className="time-filter">
                                            <li>Filter By:</li>
                                            <li>
                                                Time
                                            </li>
                                            <li>
                                                <div className="form-group">
                                                    <select name="time-filter"
                                                            id="time-filter"
                                                            onClick={this.changeOrderTimeFlow}
                                                            className="form-control time-filter-select">
                                                        <option value="desc">Desc</option>
                                                        <option value="asc">Asc</option>
                                                    </select>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="col-md-2">
                                        <button className='btn btn-primary btn-sm'
                                                onClick={this.toggleDateRange}>
                                            Previous Orders
                                        </button>
                                    </div>
                                </div>
                                {(() => {
                                    if (this.state.showDateRange) {
                                        return <div className="row">
                                            <div className="col-md-1">
                                                From:
                                            </div>
                                            <div className="col-md-4">
                                                <ReactDatePicker inputProps={{'placeholder': 'Pick a date'}}
                                                                 timeFormat={false}
                                                                 dateFormat='YYYY-MM-DD'
                                                                 onChange={this.pickFromDate}
                                                                 closeOnSelect={true}/>
                                            </div>
                                            <div className="col-md-1">
                                                To:
                                            </div>
                                            <div className="col-md-4">
                                                <ReactDatePicker inputProps={{'placeholder': 'Pick a date'}}
                                                                 onChange={this.pickToDate}
                                                                 timeFormat={false}
                                                                 dateFormat='YYYY-MM-DD'
                                                                 closeOnSelect={true}/>
                                            </div>
                                        </div>
                                    } else {
                                        return null;
                                    }
                                })()}
                            </div>
                            <div className="orders-cont">
                                {this.state.gettingOrders ? <Loader1/> : null}
                                {(() => {
                                    if (this.state.orders.length === 0 && !this.state.gettingOrders) {
                                        return <Sad text={'No Order Found'}/>
                                    } else {
                                        return <div className="table-responsive">
                                            <table
                                                className="table table-striped table-hover table-bordered white b-a table-sm">
                                                <thead>
                                                <tr>
                                                    <th>Symbol</th>
                                                    <th>Exchange</th>
                                                    <th>Status</th>
                                                    <th>Side</th>
                                                    <th>Type</th>
                                                    <th>Size</th>
                                                    <th>Price</th>
                                                    <th>Time</th>
                                                    {this.state.currentStatus === 'open' ? <th>Action</th> : null}
                                                </tr>
                                                </thead>
                                                <tbody>
                                                {orders}
                                                </tbody>
                                            </table>
                                            {(() => {
                                                if (this.state.pageCount > 1) {
                                                    return <div className="pagination">
                                                        <Pagination pageCount={this.state.pageCount}
                                                                    pageChanged={this.pageChanged}/>
                                                    </div>
                                                } else {
                                                    return null;
                                                }
                                            })()}
                                        </div>
                                    }
                                })()}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}