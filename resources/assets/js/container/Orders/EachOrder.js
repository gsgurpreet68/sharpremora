import React from 'react';
import Loader1 from "../../components/Loader/Loader1";
import {ajaxErrorHandling} from "../../Helpers/ErrorHandling/AjaxErrorHandling";
import toast from "../../Helpers/Toast/Toast";

export default class EachOrder extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.cancelOrder = this.cancelOrder.bind(this);
    }

    cancelOrder() {
        const confirm = window.confirm('Are you sure that you want to cancel?');
        if (confirm) {
            if (!this.props.order.exchange_order_id) {
                alert('It\'s a dummy order. So, it can\'t be cancelled');
                throw Error('error');
            }
            this.setState({
                cancelling: true
            });
            const data = {
                order_id: this.props.order.order_id
            };
            axios.post('/api/v1/orders/cancel-order', data, {
                headers: {
                    Authorization: "Bearer " + localStorage.getItem('token')
                },
            }).then(response => {
                this.setState({
                    cancelling: false,
                });
                toast(response.data.msg);
                if (response.data.success) {
                    this.props.orderCancelled ? this.props.orderCancelled() : null;
                }
            }).catch(error => {
                console.log(error);
                this.setState({
                    cancelling: false,
                }, () => {
                    toast(ajaxErrorHandling(error));
                });
            });
        }
    }

    render() {
        return (
            <tr>
                <td>{this.props.order.symbol}</td>
                <td>{this.props.order.exchange_name}</td>
                <td>{this.props.order.status}</td>
                <td>{this.props.order.side}</td>
                <td>{this.props.order.type}</td>
                <td>{this.props.order.size}</td>
                <td>
                    {this.props.order.price} <span className="label label-primary font-color-black">
                    {this.props.order.price_currency}
                </span>
                </td>
                <td>{this.props.order.ordered_at}</td>
                {(() => {
                    if (this.props.currentStatus === 'open') {
                        return <td>
                            <button className="btn btn-danger btn-sm"
                                    disabled={this.state.cancelling}
                                    onClick={this.cancelOrder}>
                                {this.state.cancelling ? <Loader1/> : 'Cancel'}
                            </button>
                        </td>
                    } else {
                        return null;
                    }
                })()}
            </tr>
        )
    }
}