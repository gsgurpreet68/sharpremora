import React from 'react';
import './scss/openPositionModal.scss';
import RemoraHubModal from "../../components/Modal/RemoraHubModal";
import {ajaxErrorHandling} from "../../Helpers/ErrorHandling/AjaxErrorHandling";
import toast from "../../Helpers/Toast/Toast";
import './scss/openPositionModal.scss';
import {strCapitalize} from "../../Helpers/Functions/HelpfulFunctions";
import ReactSelect from 'react-select';
import 'react-select/dist/react-select.css';
import Loader1 from "../../components/Loader/Loader1";
import {Link} from 'react-router-dom';
import Dropdown from 'react-dropdown'
import 'react-dropdown/style.css'

export default class OpenPositionModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            addedExchanges: [],
            exchangeCapabilities: {},
            assets: [],
            orderType: 'limit',
            currentExchange: 'nothing',
            currentAsset: {'label': 'Loading...'}
        };
        this.getExchanges = this.getExchanges.bind(this);
        this.changeExchange = this.changeExchange.bind(this);
        this.loadExchangeCapabilities = this.loadExchangeCapabilities.bind(this);
        this.setOrderType = this.setOrderType.bind(this);
        this.loadAssets = this.loadAssets.bind(this);
        this.switchAsset = this.switchAsset.bind(this);
        this.changeInput = this.changeInput.bind(this);
        this.sendOrder = this.sendOrder.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.getBalance = this.getBalance.bind(this);
        this.uiclass = this.uiclass.bind(this);
        this.exchangeOptions = this.exchangeOptions.bind(this);
    }

    getExchanges() {
        this.setState({gettingExchanges: true});
        axios.get('/api/v1/exchange/connected', {
            headers: {
                Authorization: "Bearer " + localStorage.getItem('token')
            }
        }).then(response => {
            const data = response.data;
            this.setState({addedExchanges: data.data, gettingExchanges: false});
        }).catch(error => {
            toast(ajaxErrorHandling(error));
            console.log(error.response.data);
        });
    }

    componentDidMount() {
        this.getExchanges();
    }

    changeExchange(e) {
        const exchange = e.value;
        this.setState({
            balance:[],
            currentExchange: exchange,
            assets: [],
            exchangeCapabilities: {},
            currentAsset: {'label': 'Loading...'}
        }, () => {
            if (this.state.currentExchange !== 'nothing') {
                this.loadAssets();
                this.loadExchangeCapabilities();
            }
        });
    }

    loadAssets() {
        const url = 'api/v1/exchange/markets?exchange_name=' + this.state.currentExchange;
        this.setState({
            reactSelectPlaceholder: 'Loading...',
            reactSelectNoResultsTxt: 'Loading...'
        });
        axios.get(url, {
            headers: {
                Authorization: "Bearer " + localStorage.getItem('token')
            }
        }).then(response => {
            const data = response.data;
            this.setState({
                assets: data.data,
                currentAsset: data.data.length > 0 ? data.data[0] : {},
                reactSelectNoResultsTxt: 'Nothing found',
                reactSelectPlaceholder: 'Symbol'
            }, () => {
                if (this.state.currentAsset.label) {
                    this.getBalance();
                }
            });
        }).catch(error => {
            toast(ajaxErrorHandling(error));
            console.log(error.response.data);
        });
    }

    loadExchangeCapabilities() {
        const url = 'api/v1/exchange/capabilities?exchange_name=' + this.state.currentExchange;
        axios.get(url, {
            headers: {
                Authorization: "Bearer " + localStorage.getItem('token')
            }
        }).then(response => {
            const data = response.data;
            this.setState({
                exchangeCapabilities: data.data
            });
        }).catch(error => {
            toast(ajaxErrorHandling(error));
            console.log(error.response.data);
        });
    }

    setOrderType(type) {
        this.setState({
            orderType: type
        });
    }

    switchAsset(value) {
        this.setState({
            currentAsset: value
        }, () => {
            this.getBalance();
        });
    }

    changeInput(e) {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    getBalance() {
        const currentExchange = this.state.currentExchange;
        const currentAsset = this.state.currentAsset;
        if (currentAsset) {
            const url = '/api/v1/exchange/get-balance?exchange_name=' + currentExchange +
                '&asset[0]=' + currentAsset.base + '&asset[1]=' + currentAsset.quote;
            this.setState({
                loadingBalance: true,
                balanceLoaded: false
            });
            axios.get(url, {
                headers: {
                    Authorization: "Bearer " + localStorage.getItem('token')
                }
            }).then(response => {
                const data = response.data;
                this.setState({
                    loadingBalance: false,
                });
                if (data.success) {
                    this.setState({
                        balanceLoaded: true,
                        balance: data.data
                    });
                }
            }).catch(error => {
                toast(ajaxErrorHandling(error));
                console.log(error.response.data);
                this.setState({
                    loadingBalance: false,
                    balanceLoaded: false
                });
            });
        }
    }

    sendOrder(side) {
        let currentSide;
        if (side === 'buy') {
            currentSide = 'buy';
        } else if (side === 'sell') {
            currentSide = 'sell';
        }
        this.setState({
            side: currentSide
        }, () => {
            this.setState({
                sendingOrder: true
            });
            let data = {
                exchange_name: this.state.currentExchange,
                symbol: this.state.currentAsset.ccxt_symbol,
                type: this.state.orderType,
                side: this.state.side,
                quantity: this.state.quantity,
                price: this.state.price,
                time_in_force: 'GTC'
            };

            axios.post('/api/v1/position/open', data, {
                headers: {
                    Authorization: "Bearer " + localStorage.getItem('token')
                }
            }).then(response => {
                this.setState({
                    sendingOrder: false,
                });
                toast(response.data.msg);
                if (response.data.success) {
                    this.props.orderPlaced ? this.props.orderPlaced() : null;
                }
            }).catch(error => {
                console.log(error);
                this.setState({
                    sendingOrder: false,
                }, () => {
                    const msg = ajaxErrorHandling(error, '<br/>');
                    toast(msg, msg.length > 100 ? 5000 : 3000);
                });
            });
        });
    }

    closeModal() {
        this.props.modalClosed ? this.props.modalClosed() : null;
    }

    uiclass()
    {
        if(this.state.currentExchange == 'nothing'){ 
            return 'blue-grey-50';
        }

        return this.state.exchangeCapabilities.exchange_type == 'C' ? 'dark-900' : 'blue-grey-50';
    }

    exchangeOptions()
    {
        return this.state.addedExchanges.map((e, i) => {
            return { value: e.name, label: strCapitalize(e.name) };
        });
    }

    render() {
        
    
        return (
            <div id="open-position-modal">
                <RemoraHubModal customClass={`model-width ${this.uiclass()}`} showModal={true}
                                modalClosed={this.props.modalClosed}>
                                <div className={`container w-xxl zero-padding text-xs ${this.uiclass()}`}>
                                    <div className="row no-gutter text-u-c text-center clearfix">
                                    <div className="col-sm-2 row no-gutter border-right-0">
                                        <div className="col-xs p-a-xs b-l mover">&nbsp;</div>
                                        <div className="col-xs p-a-xs b-l b-r pointer">
                                        
                                        <Link to="/open-new-position">
                                            <i className="fa fa-square-o"
                                            onClick={this.closeModal}/>
                                        </Link>
                                        </div>
                                    </div>
                                    <div className="col-sm-4 p-a-xs border-left-0 text-ellipsis" style={{overflow:'visible'}}>
                                    
                                    <Dropdown options={this.exchangeOptions()} onChange={this.changeExchange} value={{ value: "", label: this.state.gettingExchanges ? 'Loading...' : 'Exchange' }} placeholder= {this.state.gettingExchanges ? 'Loading...' : 'Exchange'} />
                                    </div>
                                    <div className="col-sm-6 p-a-xs b-l">
                                    <b className={"label rounded label-md m-l-sm pointer " +
                                        (this.state.orderType === 'limit' ? "info" : '')}
                                        onClick={() => this.setOrderType('limit')}
                                        data-toggle="tooltip"
                                        title="Limit Order">L</b>
                                        {(() => {
                                            if (this.state.exchangeCapabilities.market_order) {
                                                return <b className={"label rounded label-md m-l-sm pointer " +
                                                (this.state.orderType === 'market' ? "info" : '')}
                                                        data-toggle="tooltip"
                                                        onClick={() => this.setOrderType('market')}
                                                        title="Market Order">M</b>
                                            } else {
                                                return null;
                                            }
                                        })()}
                                        {(() => {
                                            if (this.state.exchangeCapabilities.stop_loss_order) {
                                                return <b className={"label rounded label-md m-l-sm pointer " +
                                                (this.state.orderType === 'stop_loss' ? "info" : '')}
                                                        data-toggle="tooltip"
                                                        onClick={() => this.setOrderType('stop_loss')}
                                                        title="Stop loss order">SL</b>
                                            } else {
                                                return null;
                                            }
                                        })()}
                                    </div>
                                    </div>
                                    <div className="row no-gutter text-center b-t b-b clearfix">
                                    <div className="col-sm p-a-xs b-l">
                                        {(() => {
                                            if (this.state.currentExchange !== 'nothing') {
                                                return <ReactSelect placeholder={this.state.reactSelectPlaceholder}
                                                                    noResultsText={this.state.reactSelectNoResultsTxt}
                                                                    value={this.state.currentAsset ? this.state.currentAsset : null}
                                                                    onChange={this.switchAsset}
                                                                    options={this.state.assets}/>
                                            } else {
                                                return null;
                                            }
                                        })()}
                                    </div>
                                    
                                    <div className="col-sm p-a-xs b-l"><input className="form-control form-control-sm form-control-custom" type="text" placeholder="Size" /></div>
                                    
                                    </div>
                                    <div className="row no-gutter text-center clearfix">
                                        <div className="col-sm text-u-c danger pointer align-middle zero-padding ">
                                            <div className={`p-a-xs b-l ${this.uiclass()}`}>
                                            {(() => {
                                                    if (this.state.balanceLoaded) {
                                                        let e = this.state.balance[0];
                                                            if(!e) return <span className="text-muted">-</span>;
                                                            return <span title={'Balance available in ' + e.asset} className="text-muted">{e.asset} - {e.free}</span>
                                                           
                                                    } else {
                                                        return <span className="text-muted">-</span>;
                                                    }
                                                })()}
                                            
                                            
                                            </div>
                                            <div className="p-a-xs"><span
                                             disabled={this.state.sendingOrder}
                                             onClick={() => this.sendOrder('sell')}
                                            className="text-white align-middle _600 custom-btn">Sell/Short</span></div>
                                        </div>
                                        <div className="col-sm b-l b-r">
                                            <div className="text-muted text-center align-middle p-a-xs">
                                            <small>
                                                {this.state.loadingBalance ?  `Loading Balance...` : `/`}
                                            </small>
                                            </div>
                                            <div className="p-a-xs">
                                              <input className="form-control form-control-sm form-control-custom" type="text" 
                                                        onChange={this.changeInput}
                                                        name="price"
                                                        placeholder="Price"
                                                        disabled={this.state.orderType == 'market' ? true : false}
                                                        />
                                            
                                                

                                            </div>
                    
                                        </div>
                                        <div className="col-sm text-u-c green align-middle zero-padding">
                                            <div className={`p-a-xs b-r ${this.uiclass()}`}>
                
                                                {(() => {
                                                    if (this.state.balanceLoaded) {
                                                        let e = this.state.balance[1];
                                                            if(!e) return <span className="text-muted">-</span>;
                                                            return <span title={'Balance available in ' + e.asset} className="text-muted">{e.asset} - {e.free}</span>
                                                           
                                                    } else {
                                                        return <span className="text-muted">-</span>;
                                                    }
                                                })()}
                                                </div>
                                             <div className="p-a-xs">
                                                <span
                                                disabled={this.state.sendingOrder}
                                                onClick={() => this.sendOrder('buy')}
                                                className="text-white align-middle _600 custom-btn">Buy/Long</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                    {this.state.sendingOrder ? <Loader1/> : null}
                </RemoraHubModal>
            </div>
        )
    }
}