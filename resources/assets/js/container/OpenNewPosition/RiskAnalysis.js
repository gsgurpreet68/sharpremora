import React from 'react';

export default class RiskAnalysis extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div className="col-md-4 col-sm-4">
                <div style={{'marginLeft': '10px'}}>
                    <div className="box">
                        <div className="box-header">
                            <h3 className="text-u-c">Risk/Reward Analysis</h3>
                        </div>
                        <div className="box-body">
                            You did not set a Stop Loss. You are at risk of losing 100% of your capital. Ignore
                            this message if this is part of your strategy. Otherwise, consider setting a stop
                            loss for this position or setting a default stop loss for this asset in Settings.
                        </div>

                        <div className="box-body">
                            You have set a take profit of 20%, this translates to a potential reward of $357 on
                            this asset if the price hits your Take Profit level. Consider setting a Boomerang
                            Take Profit to keep the position open even after your profit is guaranteed.
                        </div>

                        <div className="box-body">
                            More than 2% of your total investment capita will be at risk on this position.
                            Consider setting a smaller stop loss or reducing your position size.
                        </div>
                    </div>

                    <div className="box">
                        <div className="box-header">
                            <h3 className="text-u-c">Impact on Portfolio</h3>
                        </div>
                        <div className="box-body">
                            <h5>Before</h5>
                        </div>
                        <div className="box-body">
                            <h5>After</h5>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}