import React from 'react';
import toast from "../../Helpers/Toast/Toast";
import {ajaxErrorHandling} from "../../Helpers/ErrorHandling/AjaxErrorHandling";
import {strCapitalize} from "../../Helpers/Functions/HelpfulFunctions";
import Loader1 from "../../components/Loader/Loader1";
import ReactSelect from 'react-select';
import 'react-select/dist/react-select.css';
import OrderBook from "./OrderBook";
import RiskAnalysis from "./RiskAnalysis";
import Order from "./Order";
import './scss/openNewPosition.scss';
import openSocket from 'socket.io-client';

export default class OpenNewPosition extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            addedExchanges: [],
            currentExchange: 'nothing',
            assets: [],
            currentAsset: {},
            balance: [],
            asks:[],
            bids:[],
        };
        this.getExchanges = this.getExchanges.bind(this);
        this.changeExchange = this.changeExchange.bind(this);
        this.switchAsset = this.switchAsset.bind(this);
        this.typingAssetName = this.typingAssetName.bind(this);
        this.loadExchangeCapabilities = this.loadExchangeCapabilities.bind(this);
        this.getBalance = this.getBalance.bind(this);
        this.subscribeToExchangeMarket = this.subscribeToExchangeMarket.bind(this);
    }

    subscribeToExchangeMarket(cb) {
        const  socket = openSocket('http://localhost:8888');
        console.log('Opening connection');
        socket.on('exchange_market_depth', data => {
            console.log('testing socket response', data);
            //console.log(data.orders);
            if(data.asks){
                this.setState({asks:data.asks})
            }
            if(data.bids){
                this.setState({bids:data.bids})
            }
            
        });
        socket.emit('market_details', {'exchange': 'binance', 'market':'LTCBTC'});
      }

    getExchanges() {
        this.setState({gettingExchanges: true});
        axios.get('/api/v1/exchange/connected', {
            headers: {
                Authorization: "Bearer " + localStorage.getItem('token')
            }
        }).then(response => {
            const data = response.data;
            this.setState({addedExchanges: data.data, gettingExchanges: false});
        }).catch(error => {
            toast(ajaxErrorHandling(error));
            console.log(error.response.data);
        });
    }

    componentDidMount() {
        this.getExchanges();
        this.subscribeToExchangeMarket();
        const screenSize = window.screen.width;
        if (screenSize >= 610) {
            this.setState({
                smallView: false,

            });
        } else {
            this.setState({
                smallView: true
            });
        }
    }

    loadExchangeCapabilities() {
        const url = 'api/v1/exchange/capabilities?exchange_name=' + this.state.currentExchange;
        axios.get(url, {
            headers: {
                Authorization: "Bearer " + localStorage.getItem('token')
            }
        }).then(response => {
            const data = response.data;
            this.setState({
                exchangeCapabilities: data.data
            });
        }).catch(error => {
            toast(ajaxErrorHandling(error));
            console.log(error.response.data);
        });
    }

    getBalance() {
        const currentExchange = this.state.currentExchange;
        const currentAsset = this.state.currentAsset;
        if (currentAsset) {
            const url = '/api/v1/exchange/get-balance?exchange_name=' + currentExchange +
                '&asset[0]=' + currentAsset.base + '&asset[1]=' + currentAsset.quote;
            this.setState({
                loadingBalance: true,
                balanceLoaded: false
            });
            axios.get(url, {
                headers: {
                    Authorization: "Bearer " + localStorage.getItem('token')
                }
            }).then(response => {
                const data = response.data;
                this.setState({
                    loadingBalance: false,
                });
                if (data.success) {
                    this.setState({
                        balanceLoaded: true,
                        balance: data.data
                    });
                }
            }).catch(error => {
                toast(ajaxErrorHandling(error));
                console.log(error.response.data);
                this.setState({
                    loadingBalance: false,
                    balanceLoaded: false
                });
            });
        }
    }

    loadAssets() {
        const url = 'api/v1/exchange/markets?exchange_name=' + this.state.currentExchange;
        this.setState({
            reactSelectPlaceholder: 'Loading...',
            reactSelectNoResultsTxt: 'Loading...'
        });
        axios.get(url, {
            headers: {
                Authorization: "Bearer " + localStorage.getItem('token')
            }
        }).then(response => {
            const data = response.data;
            this.setState({
                assets: data.data,
                currentAsset: data.data.length > 0 ? data.data[0] : null,
                reactSelectPlaceholder: 'Symbol',
                reactSelectNoResultsTxt: 'Nothing found'
            }, () => {
                this.getBalance();
            });
        }).catch(error => {
            toast(ajaxErrorHandling(error));
            console.log(error.response.data);
        });
    }

    changeExchange(e) {
        const exchange = e.target.value;
        this.setState({
            currentExchange: exchange,
            assets: [],
            currentAsset: {'label': 'Loading assets...'}
        }, () => {
            if (this.state.currentExchange !== 'nothing') {
                this.loadAssets();
                this.loadExchangeCapabilities();
            }
        });
    }

    switchAsset(value) {
        this.setState({
            currentAsset: value
        }, () => {
            this.getBalance();
        });
    }

    typingAssetName(str) {
        if (this.state.currentExchange === 'nothing') {
            toast('Kindly select an exchange first', 2000);
        }
    }

    render() {
        let exchanges = this.state.addedExchanges;
        exchanges = exchanges.map((e, i) => {
            return <option value={e.name}
                           key={i}>{strCapitalize(e.name)}</option>
        });
        let balance = this.state.balance;
        balance = balance.map((e, i) => {
            return <li key={i}
                       className="balance"
                       title={'Balance available in ' + e.asset}>
                {e.asset} - {e.free}
            </li>
        });
        return (
            <div className="app-body" id="open-new-position">
                <div className="m-a-md p-a-sm">
                    <div className="row-col no-gutters">
                        <div className="box row no-gutters m-b-sm p-y w-100 m-auto">
                            <div className="col-md-4">
                                <div className="col-8 p-l-md">
                                    <span className="_600">
                                        {this.state.currentAsset.label}
                                        </span>
                                    {(() => {
                                        if (this.state.side) {
                                            if (this.state.side === 'sell') {
                                                return '{' +
                                                    (this.state.currentAsset.base ? this.state.currentAsset.base : '')
                                                    + ' ' + (this.state.price ? this.state.price : '')
                                                    + '}';
                                            } else if (this.state.side === 'buy') {
                                                return '{' +
                                                    (this.state.currentAsset.quote ? this.state.currentAsset.quote : '')
                                                    + ' ' + (this.state.price ? this.state.price : '')
                                                    + '}';
                                            }
                                        }
                                    })()}
                                    {(() => {
                                        if (this.state.currentAsset.type) {
                                            return <span
                                                className="label accent label-sm"
                                                style={{'marginLeft': '7px'}}>
                                                {this.state.currentAsset.type}
                                                </span>
                                        } else {
                                            return null;
                                        }
                                    })()}
                                    <br/>
                                    {this.state.loadingBalance ? <span className="small-font">
                                        Loading Balance...
                                    </span> : null}
                                    {this.state.balanceLoaded ? (
                                        <ul className="balance-list">{balance}</ul>
                                    ) : null}
                                </div>
                                <div className="col">
                                    {this.state.leverage ? (
                                        <span className="label label-sm danger"
                                              title="Leverage: Margin - $1750, Fees - 1% of position">x4 Leverage</span>
                                    ) : null}
                                    &nbsp;
                                    {this.state.side ? (
                                        <span className="label label-md grey-400"
                                              title="Interactive Brokers">{this.state.side}</span>
                                    ) : null}
                                </div>
                            </div>
                            <div className="col-md-4">
                                <span className="_600">Broker</span><br/>
                                <select name="exchanges"
                                        onChange={this.changeExchange}
                                        id="added-exchanges">
                                    <option value="nothing">Select an exchange</option>
                                    {exchanges}
                                </select>
                                {this.state.getExchanges ? <Loader1/> : null}
                            </div>
                            <div className="col-md-4">
                                <div className="form-group">
                                    {(() => {
                                        if (this.state.currentExchange !== 'nothing') {
                                            return <ReactSelect placeholder="Switch Asset"
                                                                onInputChange={this.typingAssetName}
                                                                options={this.state.assets}
                                                                value={this.state.currentAsset}
                                                                onChange={this.switchAsset}/>
                                        } else {
                                            return null;
                                        }
                                    })()}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row no-gutter">
                        <OrderBook asks={this.state.asks} bids={this.state.bids}/>
                        <Order exchangeCapabilities={this.state.exchangeCapabilities}
                               currentAsset={this.state.currentAsset}
                               priceChanged={(price) => this.setState({price: price})}
                               exchange={this.state.currentExchange}
                               sideChanged={(side) => this.setState({side: side})}/>
                        <RiskAnalysis/>
                    </div>
                </div>
            </div>
        )
    }
}