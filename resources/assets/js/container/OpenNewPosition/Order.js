import React from 'react';
import Loader1 from "../../components/Loader/Loader1";
import {ajaxErrorHandling} from "../../Helpers/ErrorHandling/AjaxErrorHandling";
import toast from "../../Helpers/Toast/Toast";

export default class Order extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            exchangeCapabilities: {},
            type: 'limit',
            currentAsset: {}
        };
        this.setSide = this.setSide.bind(this);
        this.setType = this.setType.bind(this);
        this.changeInput = this.changeInput.bind(this);
        this.changePrice = this.changePrice.bind(this);
        this.changeSize = this.changeSize.bind(this);
        this.changeTimeInForce = this.changeTimeInForce.bind(this);
        this.sendOrder = this.sendOrder.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.exchange !== nextProps.exchange) {
            this.setState({
                exchange: nextProps.exchange
            });
        }

        if (this.props.currentAsset !== nextProps.currentAsset && nextProps.currentAsset) {
            this.setState({
                currentAsset: nextProps.currentAsset
            });
        }

        if (this.props.exchangeCapabilities !== nextProps.exchangeCapabilities) {
            if (nextProps.exchangeCapabilities) {
                this.setState({
                    exchangeCapabilities: nextProps.exchangeCapabilities
                });
            }
        }
    }

    componentDidMount() {
        this.setState({
            exchange: this.props.exchange,
            currentAsset: this.props.currentAsset ? this.props.currentAsset : {},
            exchangeCapabilities: this.props.exchangeCapabilities ? this.props.exchangeCapabilities : {}
        });
    }

    setSide(side) {
        this.setState({
            side: side
        }, () => {
            this.props.sideChanged ? this.props.sideChanged(side) : null;
        });
    }

    setType(type) {
        this.setState({
            type: type
        });
        if (type === 'market') {
            this.setState({
                price: ''
            });
        }
    }

    changeTimeInForce(timeInForce) {
        this.setState({
            timeInForce: timeInForce
        });
    }

    changeInput(e) {
        this.setState({
            [e.target.name]: e.target.value
        });
        if (e.target.name === 'price') {
            this.props.priceChanged ? this.props.priceChanged(e.target.value) : null;
        }
    }

    changePrice(direction) {
        let price = this.state.price;
        const orderType = this.state.type;
        if (price && orderType !== 'market') {
            price = parseFloat(price);
            if (direction === 'plus') {
                if (price >= 1) {
                    price = price + 1;
                } else {
                    price = price + 0.02;
                }
            } else if (direction === 'minus') {
                if (price >= 2) {
                    price = price - 1;
                } else {
                    price = price - 0.02;
                }
            }
        }
        this.setState({
            price: price
        }, () => {
            this.props.priceChanged ? this.props.priceChanged(this.state.price) : null;
        });
    }

    changeSize(direction) {
        let quantity = this.state.quantity;
        if (quantity) {
            quantity = parseFloat(quantity);
            if (direction === 'plus') {
                quantity = quantity + 1;
            } else if (direction === 'minus') {
                quantity = quantity - 1;
            }
        }
        this.setState({
            quantity: quantity
        });
    }

    sendOrder() {
        this.setState({
            sendingOrder: true
        });
        let data = {
            exchange_name: this.state.exchange,
            symbol: this.state.currentAsset.ccxt_symbol,
            type: this.state.type,
            side: this.state.side,
            quantity: this.state.quantity,
            price: this.state.price,
            time_in_force: this.state.timeInForce
        };

        axios.post('/api/v1/position/open', data, {
            headers: {
                Authorization: "Bearer " + localStorage.getItem('token')
            }
        }).then(response => {
            this.setState({
                sendingOrder: false,
            });
            toast(response.data.msg);
            if (response.data.success) {
                this.props.orderPlaced ? this.props.orderPlaced() : null;
            }
        }).catch(error => {
            console.log(error);
            this.setState({
                sendingOrder: false,
            }, () => {
                const msg = ajaxErrorHandling(error, '<br/>');
                toast(msg, msg.length > 100 ? 5000 : 3000);
            });
        });
    }

    render() {
        return (
            <div className="col-md-4 col-sm-4">
                <div className="box p-a-sm">
                    <div className="box-body">
                        <form action="#" className="open-position-large">
                            <div className="row">
                                <div className="m-b-sm b-b w-100 nav-active-bg">
                                    <ul className="nav nav-tabs">
                                        {(() => {
                                            if (this.state.exchangeCapabilities.limit_order) {
                                                return <li className="nav-item">
                                                    <a href="javascript:"
                                                       onClick={() => this.setType('limit')}
                                                       className={"nav-link " +
                                                       (this.state.type === 'limit' ? '_600active active' : '')}
                                                       data-toggle="tab">
                                                <span
                                                    className="label label-sm  m-r-xs info">L</span>
                                                        {this.state.type === 'limit' ? 'Limit' : null}
                                                    </a>
                                                </li>
                                            } else {
                                                return null;
                                            }
                                        })()}
                                        {(() => {
                                            if (this.state.exchangeCapabilities.market_order) {
                                                return <li className="nav-item">
                                                    <a href="javascript:"
                                                       onClick={() => this.setType('market')}
                                                       className={"nav-link _600" +
                                                       (this.state.type === 'market' ? '_600active active' : '')}
                                                       data-toggle="tab">
                                                <span
                                                    className="label label-sm  m-r-xs info">M</span>
                                                        {this.state.type === 'market' ? 'Market' : null}
                                                    </a>
                                                </li>
                                            }
                                        })()}
                                        {(() => {
                                            if (this.state.exchangeCapabilities.stop_loss_order) {
                                                return <li className="nav-item">
                                                    <a href="javascript:"
                                                       onClick={() => this.setType('stop_loss')}
                                                       className={"nav-link _600" +
                                                       (this.state.type === 'stop_loss' ? '_600active active' : '')}
                                                       data-toggle="tab">
                                                <span
                                                    className="label label-sm  m-r-xs info">SL</span>
                                                        {this.state.type === 'stop_loss' ? 'Stop Loss' : null}
                                                    </a>
                                                </li>
                                            }
                                        })()}
                                    </ul>
                                </div>
                            </div>
                            <div className="row m-b-md  m-auto w-100">
                                <button
                                    type="button"
                                    className={"btn btn-secondary btn-md no-radius " +
                                    (this.state.side === 'sell' ? 'btn-danger' : 'grey-100')}
                                    onClick={() => this.setSide('sell')}
                                    style={{'marginRight': '7px'}}>
                                    Sell/Short
                                </button>
                                <button
                                    type="button"
                                    className={"btn btn-secondary btn-md no-radius " +
                                    (this.state.side === 'buy' ? 'btn-danger' : 'grey-100')}
                                    onClick={() => this.setSide('buy')}>
                                    Buy/Long
                                </button>
                            </div>

                            <div className="row">
                                <div className="form-group w-100">
                                    {(() => {
                                        if (this.state.exchangeCapabilities.time_in_force) {
                                            return <div className="pull-right">
                                                    <span className={"label label-sm " +
                                                    (this.state.timeInForce === 'gtc' ? 'info' : '')}
                                                          data-toggle="tooltip"
                                                          onClick={() => this.changeTimeInForce('gtc')}
                                                          title="Good Til Cancel">GTC</span>
                                                <span className={"label label-sm " +
                                                (this.state.timeInForce === 'day' ? 'info' : '')}
                                                      data-toggle="tooltip"
                                                      onClick={() => this.changeTimeInForce('day')}
                                                      title="Good Til End of Day">DAY</span>
                                                <span className={"label label-sm " +
                                                (this.state.timeInForce === 'fok' ? 'info' : '')}
                                                      data-toggle="tooltip"
                                                      onClick={() => this.changeTimeInForce('fok')}
                                                      title="Fill or Kill">FOK</span>
                                                <span className={"label label-sm " +
                                                (this.state.timeInForce === 'ioc' ? 'info' : '')}
                                                      data-toggle="tooltip"
                                                      onClick={() => this.changeTimeInForce('ioc')}
                                                      title="Immediate or Cancel">IOC</span>
                                            </div>
                                        } else {
                                            return null;
                                        }
                                    })()}
                                    <div className="clear"><label htmlFor="size"
                                                                  className="m-r-sm text-u-c">Price</label>
                                    </div>
                                    <div className="input-group input-group-sm">
                                        <span className="input-group-addon dker text-muted _600">
                                            {this.state.currentAsset.symbol ? (
                                                this.state.side === 'buy' ? this.state.currentAsset.quote :
                                                    this.state.side === 'sell' ?
                                                        this.state.currentAsset.base : null
                                            ) : null}
                                        </span>
                                        <input type="text" className="form-control"
                                               name="price"
                                               readOnly={this.state.type === 'market'}
                                               value={this.state.price}
                                               onChange={this.changeInput}
                                               id="price"
                                               placeholder="Price"/>
                                        <span className="input-group-addon dker text-muted">
                                            <i className="fa fa-plus"
                                               onClick={() => this.changePrice('plus')}/>
                                        </span>
                                        <span className="input-group-addon dker text-muted">
                                            <i className="fa fa-minus"
                                               onClick={() => this.changePrice('minus')}/>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div className="row">
                                <div className="form-group">
                                    <label htmlFor="size" className="m-r-sm text-u-c">Size</label>
                                    <div className="input-group input-group-sm">
                                        {/*<input type="text" className="col form-control m-r-xs" id="size"
                                               placeholder="Total Value"/>*/}
                                        <input type="text" className="col form-control"
                                               id="quantity"
                                               name="quantity"
                                               value={this.state.quantity}
                                               onChange={this.changeInput}
                                               placeholder="Quantity"/>
                                        <span className="input-group-addon dker text-muted"
                                              onClick={() => this.changeSize('plus')}>
                                            <i className="fa fa-plus"/>
                                        </span>
                                        <span className="input-group-addon dker text-muted"
                                              onClick={() => this.changeSize('minus')}>
                                            <i className="fa fa-minus"/>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            {/*<div className="row b-b m-b-sm">
                                Hide if the exchange/broker has not approved the account for leverage
                                <div className="form-group b-r p-r-sm">
                                                <label htmlFor="leverage" className="m-r-md text-u-c">
                                                    <span className="m-r-xs">Leverage</span>
                                                </label>
                                                <div className="">
                                                    <select name="" id="leverage" className="custom-select">
                                                        <option value=""></option>
                                                        <option value="">2x</option>
                                                        <option value="">3x</option>
                                                        <option value="">4x</option>
                                                        <option value="">1000x</option>
                                                    </select>
                                                </div>
                                            </div>
                                <div className="col text-u-c m-auto _600">
                                    <div>Capital: $5,600</div>
                                    "Capital" should be changed to "Margin" if Leverage is used
                                    <div>Fees: $25</div>
                                </div>
                            </div>


                            <div className="row">
                                <div className="form-group">
                                    <label htmlFor="stop-loss" className="m-r-sm text-u-c">
                                        <span className="m-r-xs">Stop Loss</span>
                                        <span className="label label-sm" data-toggle="tooltip"
                                              title="Set as Trailing Stop Loss">T</span>
                                    </label>
                                    <div className=" input-group input-group-sm">
                                        <input type="text" className="col form-control m-r-xs"
                                               id="stop-loss"
                                               placeholder="Absolute"/>
                                        <input type="text" className="col form-control" id="stop-loss"
                                               placeholder="%"/>
                                        <span className="input-group-addon dker text-muted"><i
                                            className="fa fa-plus"/></span>
                                        <span className="input-group-addon dker text-muted"><i
                                            className="fa fa-minus"/></span>
                                    </div>
                                </div>
                            </div>

                            <div className="row">
                                <div className="form-group">
                                    <label htmlFor="take-profit" className="m-r-sm text-u-c">
                                        <span className="m-r-xs">Take Profit</span>
                                        <span className="label label-sm" data-toggle="tooltip"
                                              title="Set as Boomerang Take Profit">B</span>
                                    </label>
                                    <div className=" input-group input-group-sm">
                                        <input type="text" className="col form-control m-r-xs"
                                               id="take-profit"
                                               placeholder="Absolute"/>
                                        <input type="text" className="col form-control" id="size"
                                               placeholder="%"/>
                                        <span className="input-group-addon dker text-muted"><i
                                            className="fa fa-plus"/></span>
                                        <span className="input-group-addon dker text-muted"><i
                                            className="fa fa-minus"/></span>
                                    </div>
                                </div>
                            </div>*/}

                            {/* hidden until we can make rules for this */}
                            {/*<div className="row b-b m-b-md">
                                            <div className="form-group">
                                                <label htmlFor="rule" className="m-r-sm text-u-c">
                                                    <span className="m-r-xs">Risk Management Rule</span>
                                                </label>
                                                <div className="">
                                                    <select name="" id="" className="custom-select">
                                                        <option value="">Dow Crash Protection</option>
                                                        <option value="">Crypto Profits Locker</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>*/}

                            <div className="row ">
                                <button className="btn btn-lg info m-auto"
                                        onClick={this.sendOrder}
                                        disabled={this.state.sendingOrder}>
                                    {this.state.sendingOrder ? <Loader1/> : 'Commit'}
                                </button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        )
    }
}