import React from 'react';
import ReactTable from "react-table";
import 'react-table/react-table.css';

export default class OrderBook extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        
          const columns = [{
            Header: 'Price',
            accessor: 'price' // String-based value accessors!
          }, {
            Header: 'Size',
            accessor: 'size',
            //Cell: props => <span className='number'>{props.value}</span> // Custom cell components!
          }, {
            //id: 'friendName', // Required because our accessor is not a string
            Header: 'Cumulative Total',
            accessor: 'cumulative' // Custom value accessors!
          }]
        return (
            <div className="col-md-4 col-sm-4">
                <ReactTable
                    data={this.props.asks}
                    columns={columns}
                />
                <ReactTable
                    data={this.props.bids}
                    columns={columns}
                />

            </div>
        )
    }
}