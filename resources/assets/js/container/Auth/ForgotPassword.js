import React from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

class ForgotPassword extends React.Component{

    constructor(){
        super();
        this.state = {
            email: '',
            failed: false,
            success: false,
            ack: ''
        };
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit() {
        this.setState({failed: false, success: false, ack: ''});
        if(this.state.email==''){
            this.setState({failed:true,ack:'Please enter a valid email address.'});
            return false;
        }

        axios.post('/api/password/reset', {
            username: this.state.email
        })
        .then(response=>{
            let data = response.data;
            if(data.success){
                this.setState({success: true, ack: data.msg});
                
            }else{
                this.setState({failed: true, ack: data.msg});
            }
        })
        .catch(error=>{
            let errorText = error.response.data.msg;
            if(error.response.data.msg == 'Validation Error') {
                errorText = "Please enter a valid email address.";
            }
            this.setState({failed: true, ack: errorText});
        })
    }

    render(){
        // console.log(this.props.location)
        return (
            <div className="center-block w-xxl w-auto-xs p-y-md">
                <div className="p-a-md box-color r box-shadow-z1 text-color m-a">
                <div className="m-b">
                    Forgot your password?
                    <p className="text-xs m-t">Enter your email address below and we will send you instructions on how to change your password.</p>
                </div>

                { this.state.failed ? <div className="alert alert-danger">{this.state.ack}</div> : '' }

                { this.state.success ? <div className="alert alert-success">{this.state.ack}</div> : '' }

                <form name="reset">
                    <div className="md-form-group">
                    <input type="email" className="md-input" value={this.state.email} onChange={(e)=>this.setState({email:e.target.value})} required />
                    <label>Your Email</label>
                    </div>
                    <button type="button" onClick={()=>this.handleSubmit()} className="btn primary btn-block p-x-md">Send</button>
                </form>
                </div>
                <p id="alerts-container"></p>
                <div className="p-v-lg text-center">Return to <Link to="/signin" className="text-primary _600">Sign in</Link></div>    
            </div>
        );
    }
}

export default ForgotPassword;