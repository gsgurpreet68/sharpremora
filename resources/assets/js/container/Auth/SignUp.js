import React from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

class SignUp extends React.Component{

    constructor(){
        super();
        this.state = {
            first_name: '',
            last_name: '',
            email: '',
            password: '',
            c_password: '',
            failed: false,
            ack: ''
        };
        this.handleRegister = this.handleRegister.bind(this);
    }

    handleRegister(event) {
        event.preventDefault();
        this.setState({failed: false, ack: ''});
        const { first_name, last_name, email, password, c_password } = this.state;
        if(first_name=='' || last_name=='' || email=='' || password=='' || c_password==''){
            this.setState({failed: true, ack: 'Please fill all the required fields.'});
            return false;
        }
        if(!email.match(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/gi)){
            this.setState({failed: true, ack: 'Invalid email'});
            return false;
        }
        if(password.length<6){
            this.setState({failed: true, ack: 'Password should consist of 6 characters or more.'});
            return false;
        }
        if(password!=c_password){
            this.setState({failed: true, ack: 'Passwords don\'t match'});
            return false;
        }
        axios.post('/api/register', {
           first_name,
           last_name,
           email,
           password,
           c_password
        })
        .then(data=>{
            var response = data.data;
            if(response.success){
                const { id, user_id } = response.newToken.token;
                localStorage.setItem('id', id);
                localStorage.setItem('user_id', user_id);
                localStorage.setItem('token', response.newToken.accessToken);
                this.props.revalidate(()=>{
                    this.props.history.push('/dashboard');
                });
            }else{
                this.setState({failed: true, ack: data.msg});
            }
        })
        .catch(error=>{
            this.setState({failed: true, ack: error.response.data.msg});
        })
    }

    render(){
        // console.log(this.props.location)
        return (
            <div className="center-block w-xxl w-auto-xs p-y-md">

                <div className="p-a-md box-color r box-shadow-z1 text-color m-a">
                    <div className="m-b text-sm">
                        Sign up
                    </div>

                    { this.state.failed ? <div className="alert alert-danger">{this.state.ack}</div> : '' }

                    <form onSubmit={(event) => this.handleRegister(event)}>
                        <div className="md-form-group">
                            <input type="text" className="md-input" value={this.state.first_name} onChange={(e) => this.setState({first_name: e.target.value}) } required />
                            <label>First Name <span style={{color:'red'}}>*</span></label>
                        </div>
                        <div className="md-form-group">
                            <input type="text" className="md-input" value={this.state.last_name} onChange={(e) => this.setState({last_name: e.target.value})} required />
                            <label>Last Name <span style={{color:'red'}}>*</span></label>
                        </div>
                        <div className="md-form-group">
                            <input type="email" className="md-input" value={this.state.email} onChange={(e) => this.setState({email: e.target.value})} required />
                            <label>Email <span style={{color:'red'}}>*</span></label>
                        </div>
                        <div className="md-form-group">
                            <input type="password" className="md-input" password={this.state.password} onChange={(e) => this.setState({password: e.target.value})} required />
                            <label>Password <span style={{color:'red'}}>*</span></label>
                        </div>
                        <div className="md-form-group">
                            <input type="password" className="md-input" password={this.state.c_password} onChange={(e) => this.setState({c_password: e.target.value})} required />
                            <label>Confirm Password <span style={{color:'red'}}>*</span></label>
                        </div>
                        <button type="submit" className="btn primary btn-block p-x-md">Register and Sign in</button>
                    </form>
                </div>

                <div className="p-v-lg text-center">
                    <div>Already have an account? <Link to="/signin" className="text-primary _600">Sign in</Link></div>
                </div>
            </div>
        );
    }
}

export default SignUp;