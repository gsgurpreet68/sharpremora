import React from 'react';
import axios from 'axios';
import { Redirect, Route, Link } from 'react-router-dom';

class SignIn extends React.Component{

    constructor(){
        super();
        this.state = {
            email: '',
            password: '',
            failed: false,
            ack: ''
        };
        this.handleLogin = this.handleLogin.bind(this)
    }

    handleLogin(event) {
        event.preventDefault(); 
        this.setState({failed: false, ack: ''});
        // console.log('clicked');
        const { email, password } = this.state;
        if(email=='' || password==''){
            this.setState({failed: true, ack: 'Please fill all the required fields.'});
            return false;
        }
        axios.post('/api/login', {
            username: email,
            password
        })
        .then(response=>{
            var data = response.data;
            if(data.success){
                // console.log(data);
                const { id, user_id } = data.newToken.token;
                localStorage.setItem('id', id);
                localStorage.setItem('user_id', user_id);
                localStorage.setItem('token', data.newToken.accessToken);
                this.props.revalidate(()=>{
                    this.props.history.push('/dashboard');
                });
                
            }else{
                this.setState({failed: true, ack: data.msg});
            }
        })
        .catch(error=>{
            this.setState({failed: true, ack: error.response.data.msg});
        })
        
    }

    render(){
        // console.log('a')
        return (
            <div className="center-block w-xxl w-auto-xs p-y-md">

                <div className="p-a-md box-color r box-shadow-z1 text-color m-a">
                    <div className="m-b text-sm">
                        Sign in
                    </div>

                    { this.state.failed ? <div className="alert alert-danger">{this.state.ack}</div> : '' }
                    <form name="reset" onSubmit={(event) => this.handleLogin(event)}>
                        <div className="md-form-group">
                            <input type="email" value={this.state.email} onChange={(e) => this.setState({email: e.target.value})} className="md-input" required />
                            <label>Email <span style={{color:'red'}}>*</span></label>
                        </div>
                        <div className="md-form-group">
                            <input type="password" value={this.state.password} onChange={(e) => this.setState({password: e.target.value})} className="md-input" required />
                            <label>Password <span style={{color:'red'}}>*</span></label>
                        </div>      
                        {/* <div className="m-b-md">        
                        <label className="md-check">
                            <input type="checkbox" /><i className="primary"></i> Keep me signed in
                        </label>
                        </div> */}
                        <button type="submit" className="btn primary btn-block p-x-md">Sign in</button>
                    </form>

                    <div style={{marginTop: 10}} ><Link to="/forgotpassword" className="text-primary _600">Forgot Password?</Link></div>

                    <div style={{marginTop: 10}} >New user? <Link to="/signup" className="text-primary _600">SignUp Now</Link></div>
                    
                </div>

            </div>
        );
    }
}

export default SignIn;