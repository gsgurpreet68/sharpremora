import { combineReducers } from 'redux';
import componentReducers from './componentReducers';


export default combineReducers({
    componentReducers
});