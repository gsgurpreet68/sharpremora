import $ from 'jquery';
import './toast.css';

export default function toast($msg, $time = 3000) {
    // create toast DIV
    $('<div />')
        .attr('id', 'toast')
        .html($msg)
        .appendTo($('body'))
        // Add the "show" class to DIV
        .addClass("show")
        // After 3 seconds, remove the show class from DIV
        .delay($time).queue(function (next) {
        $(this).removeClass('show');
        next();
    })
    // remove the element after 100 ms
        .delay(100).queue(function (next) {
        $(this).remove();
        next();
    })
}