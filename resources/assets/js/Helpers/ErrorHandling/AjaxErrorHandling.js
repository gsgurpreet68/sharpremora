function ajaxErrorHandling(error, joinMsgUsing = '<br/>') {
    let errorMsg = '';
    if (error.hasOwnProperty('response')) {
        let response = error.response;
        const responseData = response.data;
        if (responseData.hasOwnProperty('errors')) {
            const errors = responseData.errors;
            let errorMsgs = [];
            for (let msg in errors) {
                if (errors.hasOwnProperty(msg)) {
                    let msgs = errors[msg];
                    for (let i = 0; i < msgs.length; i++) {
                        errorMsgs.push(msgs[i]);
                    }
                }
            }
            errorMsg = errorMsgs.join(joinMsgUsing);
        } else {
            errorMsg = responseData.msg;
        }
    } else {
        errorMsg = 'Something went wrong';
    }
    return errorMsg;
}

export {ajaxErrorHandling};