import React from 'react';
import './sad.scss';

export default class Sad extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div id="sad-face-cont">
                <div className="text-center">
                    <div>
                        <img
                            src="https://cdn3.iconfinder.com/data/icons/smileys-people-smiley-essential/48/v-44-256.png"
                            alt="sad face image"/>
                    </div>
                    <div className="sad-txt">
                        {this.props.text}
                    </div>
                </div>
            </div>
        )
    }
}