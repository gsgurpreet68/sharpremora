import React from 'react';
import ReactPaginate from 'react-paginate';
import './scss/pagination.scss';

export default class Pagination extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.pageChange = this.pageChange.bind(this);
    }

    pageChange(page) {
        // page.selected is 0 based
        this.props.pageChanged ? this.props.pageChanged(page.selected + 1) : null;
    }

    render() {
        return (
            <div className="pagination-cont">
                <ReactPaginate pageCount={this.props.pageCount}
                               onPageChange={this.pageChange}
                               pageRangeDisplayed={4}
                               containerClassName='react-paginate'
                               marginPagesDisplayed={4}/>
            </div>
        )
    }
}