import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import { Provider, connect } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import reduxThunk from 'redux-thunk';

import '../../../public/assets/animate.css/animate.min.css';
import '../../../public/assets/glyphicons/glyphicons.css';
import '../../../public/assets/font-awesome/css/font-awesome.min.css';
import '../../../public/assets/material-design-icons/material-design-icons.css';
import '../../../public/assets/bootstrap/dist/css/bootstrap.min.css';
import '../../../public/assets/styles/app.css';
import '../../../public/assets/styles/font.css';
import '../../../public/assets/custom/Styles.css';

import reducers from './reducers';

import Main from './Main';

const store = createStore(reducers, applyMiddleware(reduxThunk));

require('./bootstrap');

class App extends Component {
  render() {
    return (
      <Main />
    );
  }
}

// export default App;

ReactDOM.render(<Provider store={store} ><App /></Provider>, document.getElementById('root'));
// registerServiceWorker();