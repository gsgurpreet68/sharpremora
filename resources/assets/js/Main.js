import React, {Component} from 'react';
import {BrowserRouter, Route, Switch, Redirect} from 'react-router-dom';
import {connect} from 'react-redux';

import SignIn from './container/Auth/SignIn';
import SignUp from './container/Auth/SignUp';
import ForgotPassword from './container/Auth/ForgotPassword';

import SideBar from './components/Nav/Sidebar/SideBar';
import Header from './components/Nav/Header/Header';

import Home from './components/Home/Home';
import Dashboard from './container/Dashboard/Dashboard';
import RiskManagement from './container/RiskManagement/RiskManagement';
import Syndicates from './container/Syndicates/Syndicates';
import Portfolio from './container/Portfolio/Portfolio';
import Settings from './container/Profile/Settings';
import Exchanges from './container/Exchanges/Exchanges';
import axios from 'axios';

import {Auth} from './components/Misc/Auth';
import PositionsCategories from "./container/PositionsCategories/PositionsCategories";
import Orders from "./container/Orders/Orders";
import OpenNewPosition from "./container/OpenNewPosition/OpenNewPosition";
import Calendar from "./container/Calender/Calendar";

const CustomRoute = ({component: Component, ...rest}) => {
    return <Route {...rest} render={(props) => <Component {...rest} {...props}/>}/>
};
const PrivateRoute = ({component: Component, auth: Auth, isConnected: isConnected, ...rest}) => {
    return <Route {...rest} render={(props) => Auth ?
        <React.Fragment>{isConnected ? '' : null}<Component {...props}
                                                            isConnected={true}/></React.Fragment> :
        <Redirect to="/signin"/>}/>
};

class Main extends Component {

    constructor() {
        super();
        this.state = {
            isSessionActive: true,
            id: localStorage.getItem('id'),
            user_id: localStorage.getItem('user_id'),
            token: localStorage.getItem('token'),
            isConnected: true
        };
        this.triggerClose = this.triggerClose.bind(this);
    }

    componentDidMount() {
        this.validateSession();
    }

    validateSession(callback) {
        Auth.validateSession(data => {
            this.setState({isSessionActive: data.status}, () => {
                if (callback !== undefined) {
                    callback();
                }
                axios.get('/api/v1/exchange/connected', {
                    headers: {
                        Authorization: "Bearer " + localStorage.getItem('token')
                    }
                })
                    .then(response => {
                        const data = response.data;
                        this.setState({
                            isConnected: data.data.length > 0
                        });
                    })
                    .catch(error => {
                        console.log(error.response.data);
                    })
            });
        })
    }


    getAuthActive() {
        return this.state.isSessionActive;
    }

    triggerClose() {
        this.setState({
            isConnected: true,
            exchangeConnModalClosed: true
        });
    }

    render() {
        const {id, user_id} = this.state;
        return (
            <BrowserRouter>
                <Switch>
                    <CustomRoute exact path="/signin" component={SignIn} revalidate={(cb) => this.validateSession(cb)}/>
                    <CustomRoute exact path="/signup" component={SignUp} revalidate={(cb) => this.validateSession(cb)}/>
                    <CustomRoute exact path="/forgotpassword" component={ForgotPassword}/>
                    <div>
                        {!this.state.isConnected ? <Home triggerClose={this.triggerClose}/> : null}
                        <SideBar/>
                        <div id="content" className="app-content box-shadow-z0" role="main">
                            <Header revalidate={() => this.validateSession()}/>
                            <PrivateRoute exact path="/" component={Dashboard} auth={this.getAuthActive()}
                                          isConnected={!this.state.isConnected} postConnect={() => {
                                this.setState({isConnected: true})
                            }}/>
                            <PrivateRoute exact path="/open-new-position" component={OpenNewPosition}
                                          auth={this.getAuthActive()} isConnected={!this.state.isConnected}
                                          postConnect={() => {
                                              this.setState({isConnected: true})
                                          }}/>
                            <PrivateRoute exact path="/dashboard" component={Dashboard} auth={this.getAuthActive()}
                                          isConnected={!this.state.isConnected} postConnect={() => {
                                this.setState({isConnected: true})
                            }}/>
                            <Route exact path="/exchanges" render={(props) =>
                                <Exchanges {...props}
                                           exchangeConnModalClosed={this.state.exchangeConnModalClosed}/>
                            }/>
                            {/*<PrivateRoute exact path="/exchanges"
                                          component={Exchanges} auth={this.getAuthActive()}
                                          isConnected={!this.state.isConnected} postConnect={() => {
                                this.setState({isConnected: true})
                            }}/>*/}
                            <PrivateRoute exact path="/risk-management" component={RiskManagement}
                                          auth={this.getAuthActive()} isConnected={!this.state.isConnected}
                                          postConnect={() => {
                                              this.setState({isConnected: true})
                                          }}/>
                            <PrivateRoute exact path="/syndicates" component={Syndicates} auth={this.getAuthActive()}
                                          isConnected={!this.state.isConnected} postConnect={() => {
                                this.setState({isConnected: true})
                            }}/>
                            <PrivateRoute exact path="/positions-categories" component={PositionsCategories}
                                          auth={this.getAuthActive()} isConnected={!this.state.isConnected}
                                          postConnect={() => {
                                              this.setState({isConnected: true})
                                          }}/>
                            <PrivateRoute exact path="/orders" component={Orders} auth={this.getAuthActive()}
                                          isConnected={!this.state.isConnected} postConnect={() => {
                                this.setState({isConnected: true})
                            }}/>
                            <PrivateRoute exact path="/portfolio" component={Portfolio} auth={this.getAuthActive()}
                                          isConnected={!this.state.isConnected} postConnect={() => {
                                this.setState({isConnected: true})
                            }}/>
                            <PrivateRoute exact path="/settings" component={Settings} auth={this.getAuthActive()}
                                          isConnected={!this.state.isConnected} postConnect={() => {
                                this.setState({isConnected: true})
                            }}/>
                            <Route exact path="/calendar/:value" render={(props) =>
                                <Calendar {...props}/>
                            }/>
                            {/*<PrivateRoute exact path="/calendar"
                                          component={Calendar} auth={this.getAuthActive()}
                                          isConnected={!this.state.isConnected} postConnect={() => {
                                this.setState({isConnected: true})
                            }}/>*/}
                        </div>
                    </div>
                </Switch>
            </BrowserRouter>
        );
    }
}

function mapStateToProps(state) {
    return {
        currentComponent: state.componentReducers.currentComponent
    }
}

export default connect(mapStateToProps)(Main);