import React from 'react';
import Loader1 from "../components/Loader/Loader1";

export class ApiSubmitSection extends React.Component {
    constructor(props) {
        super(props);
        this.submitForm = this.submitForm.bind(this);
        this.state = {
            api_key: '',
            api_secret: this.props.secretFieldName ? '' : 'dummy',
            extras: []
        };
        this.cancelAddingExchange = this.cancelAddingExchange.bind(this);
    }

    submitForm() {
        if (this.props.extraFields !== undefined && this.props.extraFields.length !== this.state.extras.length) {
            for (let i = 0; i < this.props.extraFields.length; i++) {
                if (this.state.extras[i] !== undefined) {
                    continue;
                }
                this.state.extras[i] = '';
            }
            this.setState({extras: this.state.extras}, () => {
                this.props.postSubmit(this.state);
            })
        } else {
            this.props.postSubmit(this.state);
        }
    }

    cancelAddingExchange() {
        this.props.cancelAddingExchange ? this.props.cancelAddingExchange() : null;
    }

    render() {
        return (
            <div style={{width: 450, marginTop: 10, marginBottom: 10, display: 'flex', flexDirection: 'column'}}>
                <div style={{marginBottom: 10}}><input
                    style={{border: '1px solid #ccc', outline: 'none', padding: 10, width: '100%', borderRadius: 4}}
                    type="text" placeholder={this.props.keyFieldName} value={this.state.api_key}
                    onChange={(e) => this.setState({api_key: e.target.value})}/></div>
                {
                    this.props.secretFieldName ? (
                        <div style={{marginBottom: 10}}>
                            <input style={{
                                border: '1px solid #ccc',
                                outline: 'none',
                                padding: 10,
                                width: '100%',
                                borderRadius: 4
                            }}
                                   type="text"
                                   placeholder={this.props.secretFieldName}
                                   value={this.state.api_secret}
                                   onChange={(e) => this.setState({api_secret: e.target.value})}/>
                        </div>
                    ) : null
                }
                {
                    this.props.extraFields !== undefined ? (
                        this.props.extraFields.map((e, i) => {
                            return <div style={{marginBottom: 10}} key={i}>
                                <input style={{
                                    border: '1px solid #ccc',
                                    outline: 'none',
                                    padding: 10,
                                    width: '100%',
                                    borderRadius: 4
                                }} type="text"
                                       placeholder={e}
                                       value={this.state.extras[i] !== undefined ? this.state.extras[i] : ''}
                                       onChange={(e) => {
                                           this.state.extras[i] = e.target.value;
                                           this.setState({extras: this.state.extras})
                                       }}/></div>
                        })
                    ) : null
                }
                <button className="btn btn-primary"
                        disabled={this.props.submitting}
                        onClick={this.submitForm}>
                    {this.props.submitting ? <Loader1/> : 'Submit'}
                </button>
                <button type="button"
                        onClick={this.cancelAddingExchange}
                        disabled={this.props.submitting}
                        className="btn btn-danger"
                        style={{'marginTop': '10px'}}>
                    Cancel
                </button>
            </div>
        )

    }
}

export const platforms = [
    {
        "platform": "Bitmex",
        "steps": [{
            title: 'Step 1',
            img: 'assets/images/Bitmex/BitMEX-Go-To-API-Settings.png',
            desc: 'Click on API in header of Bitmex. This will take you to API Overview Page'
        }, {
            title: 'Step 2',
            img: 'assets/images/Bitmex/BitMEX-API-Keys-Management-Page.png',
            desc: 'Once at API Overview page, click on API Key Management under API Keys'
        }, {
            title: 'Step 3',
            img: 'assets/images/Bitmex/BitMEX-Create-API-Key-Order.png',
            desc: 'Create an API Key wherein Key Permissions, Order and Order Cancel should be checked. NOTE: No not check Withdraw'
        }, {
            title: 'Step 4',
            img: 'assets/images/Bitmex/BitMEX-Copy-API-Key-Secret.png',
            desc: 'This will generate ID and Secret. Note this down. You will need it in next step. Also, don\'t forget to Enable the key'
        }],
    },
    {
        "platform": "Bittrex",
        "steps": [{
            title: 'Step 1',
            img: 'assets/images/Bittrex/Bittrex-Step-1-Click-Settings.png',
            desc: 'Click on API in header of Bittrex. This will take you to API Overview Page'
        }, {
            title: 'Step 2',
            img: 'assets/images/Bittrex/Bittrex-Step-1-Click-Settings.png',
            desc: 'Once at API Overview page, click on API Key Management under API Keys'
        }, {
            title: 'Step 3',
            img: 'assets/images/Bittrex/Bittrex-Step-3-Add-New-Key.png',
            desc: 'Create an API Key wherein Key Permissions, Order and Order Cancel should be checked. NOTE: No not check Withdraw'
        }, {
            title: 'Step 4',
            img: 'assets/images/Bittrex/Bittrex-Step-4-Copy-API-Key-and-Secret.png',
            desc: 'This will generate ID and Secret. Note this down. You will need it in next step. Also, don\'t forget to Enable the key'
        }],
    },
    {
        "platform": "Bitfinex",
        "steps": [{
            title: 'Step 1',
            img: 'assets/images/Bitfinex/Bitfinex-Step-1-Click-API.png',
            desc: 'Click on API in header of Bittrex. This will take you to API Overview Page'
        }, {
            title: 'Step 2',
            img: 'assets/images/Bitfinex/Bitfinex-Step-2-Create-New-Key.png',
            desc: 'Once at API Overview page, click on API Key Management under API Keys'
        }, {
            title: 'Step 3',
            img: 'assets/images/Bitfinex/Bitfinex-Step-3-Generate-API-Key.png',
            desc: 'Create an API Key wherein Key Permissions, Order and Order Cancel should be checked. NOTE: No not check Withdraw'
        }, {
            title: 'Step 4',
            img: 'assets/images/Bitfinex/Bitfinex-Step-4-2FA.png',
            desc: 'This will generate ID and Secret. Note this down. You will need it in next step. Also, don\'t forget to Enable the key'
        }, {
            title: 'Step 5',
            img: 'assets/images/Bitfinex/Bitfinex-Step-5-Email-Auth.png',
            desc: 'This will generate ID and Secret. Note this down. You will need it in next step. Also, don\'t forget to Enable the key'
        }, {
            title: 'Step 6',
            img: 'assets/images/Bitfinex/Bitfinex-Step-6-Copy-API-Details.png',
            desc: 'This will generate ID and Secret. Note this down. You will need it in next step. Also, don\'t forget to Enable the key'
        }],
    },
    {
        "platform": "Binance",
        "steps": [{
            title: 'Step 1',
            img: 'assets/images/Binance/Binance-Step-1-Click-Account.png',
            desc: 'Click on API in header of Bittrex. This will take you to API Overview Page'
        }, {
            title: 'Step 2',
            img: 'assets/images/Binance/Binance-Step-2-Click-API-Setting.png',
            desc: 'Once at API Overview page, click on API Key Management under API Keys'
        }, {
            title: 'Step 3',
            img: 'assets/images/Binance/Binance-Step-3-Create-API-Key.png',
            desc: 'Create an API Key wherein Key Permissions, Order and Order Cancel should be checked. NOTE: No not check Withdraw'
        }, {
            title: 'Step 4',
            img: 'assets/images/Binance/Binance-Step-4-Provide-2FA.png',
            desc: 'This will generate ID and Secret. Note this down. You will need it in next step. Also, don\'t forget to Enable the key'
        }, {
            title: 'Step 5',
            img: 'assets/images/Binance/Binance-Step-5-Copy-API-Keys.png',
            desc: 'This will generate ID and Secret. Note this down. You will need it in next step. Also, don\'t forget to Enable the key'
        }],
    },
    {
        "platform": "Cryptopia",
        "steps": []
    },
    {
        "platform": "Kucoin",
        "steps": []
    },
    {
        "platform": "Gdax",
        "steps": [] // Requires password as well
    },
    {
        "platform": "Bitstamp",
        "steps": []
    },
    {
        "platform": "Robinhood",
        "steps": []
    },
    {
        "platform": "TradeIt",
        "steps": []
    },
    {
        "platform": "WhaleClub-BTC",
        "steps": []
    },
    {
        "platform": "WhaleClub-ETH",
        "steps": []
    },
    {
        "platform": "WhaleClub-LTC",
        "steps": []
    },
    {
        "platform": "WhaleClub-DASH",
        "steps": []
    }
]