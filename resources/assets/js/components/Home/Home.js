import React from 'react';
import {platforms, ApiSubmitSection} from '../../data/PlatformSteps';
import toast from "../../Helpers/Toast/Toast";
import {ajaxErrorHandling} from "../../Helpers/ErrorHandling/AjaxErrorHandling";

class Home extends React.Component {
    constructor() {
        super();
        this.state = {
            showSteps: false,
            platforms,
            result: platforms[0].steps,
            currentStep: 0,
            showSuggestion: false,
            search: '',
            showAddResult: false,
            success: false,
            alertMsg: ''
        };
        this.connectAPI = this.connectAPI.bind(this);
        this.cancelAddingExchange = this.cancelAddingExchange.bind(this);
    }


    handleSubmit() {
        for (let i = 0; i < platforms.length; i++) {
            if (this.state.search === platforms[i].platform) {
                this.setState({result: platforms[i].steps, showSteps: true});
                return true;
            }
        }
    }

    connectAPI(d) {
        let par = {
            exchange_name: this.state.search.toLowerCase(),
            api_key: d.api_key,
            api_secret: d.api_secret
        };
        for (let i = 0; i < d.extras.length; i++) {
            par['extra_' + (i + 1)] = d.extras[i];
        }
        this.setState({
            submitting: true
        });
        axios.post('api/v1/exchange/connect', par,
            {
                headers: {
                    Authorization: "Bearer " + localStorage.getItem('token')
                }
            }).then(response => {
            if (response.data.success) {
                if (this.props.postConnect !== undefined) {
                    this.props.postConnect();
                }
                this.setState({
                    showAddResult: true,
                    success: true,
                    alertMsg: 'Successfully added'
                }, () => {
                    setTimeout(() => {
                        if (this.props.exchangeExists) {
                            this.props.triggerClose();
                        } else {
                            this.props.history.push('/exchanges');
                        }
                    }, 500);
                    this.props.triggerClose ? this.props.triggerClose() : null;
                });
            } else {
                this.setState({showAddResult: true, success: false, alertMsg: response.data.msg});
            }
            this.setState({
                submitting: false
            });
        }).catch(error => {
            console.log(error);
            this.setState({
                submitting: false,
                success: false,
            });
            toast(ajaxErrorHandling(error));
        });
    }

    cancelAddingExchange() {
        this.props.triggerClose ? this.props.triggerClose() : null;
    }

    render() {
        let key_field_name = 'Enter API Key';
        let secret_field_name = 'Enter API Secret';
        let extra_fields = [];
        const broker_name = this.state.search.toLowerCase();
        if (broker_name === 'gdax') {
            extra_fields = ['Passphrase'];
        }
        if (broker_name === 'bitstamp') {
            extra_fields = ['UID'];
        }
        if (broker_name === 'robinhood') {
            key_field_name = 'Username';
            secret_field_name = 'Password';
        }
        if (broker_name.match(/whaleclub/i)) {
            key_field_name = 'API Token';
            secret_field_name = false;
        }
        return (
            <div style={{position: 'absolute', height: '100vh', width: '100vw', zIndex: 2000, top: 0, left: 0}}>
                <div style={{
                    width: '100vw',
                    height: '100vh',
                    position: 'absolute',
                    backgroundColor: '#333',
                    opacity: .8,
                    zIndex: 2001
                }}/>
                <div style={{
                    width: '100vw',
                    height: '100vh',
                    position: 'absolute',
                    zIndex: 2002,
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                    <div style={{
                        display: 'flex',
                        width: 500,
                        padding: 10,
                        borderRadius: 4,
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: '#fff',
                        position: 'relative'
                    }}>
                        {
                            this.props.exchangeExists ? (
                                <i className="fa fa-times"
                                   style={{position: 'absolute', right: 5, top: 5, cursor: 'pointer'}}
                                   onClick={this.cancelAddingExchange}/>
                            ) : null
                        }
                        {this.state.showAddResult ?
                            <div className={this.state.success ? "alert alert-success" : "alert alert-danger"}
                                 style={{width: '100%'}}>{this.state.alertMsg}</div> :
                            ''
                        }

                        {!this.state.showSteps ?
                            <div>
                                <div>
                                    <span style={{fontSize: 25}}>
                                    {this.props.exchangeExists === undefined || !this.props.exchangeExists ? 'To use Sharp Remora, you need to connect to exchanges and/or brokers using API codes. Use the search box below to start and follow on-screen instructions.' : 'Use the search box below to start and follow on-screen instructions to find the API keys.'}
                                    </span>
                                </div>
                                <div style={{
                                    display: 'flex',
                                    marginTop: 20,
                                    justifyContent: 'center',
                                    position: 'relative'
                                }}>
                                    <input style={{flex: 1}} className="sr-overlay-search-input"
                                           placeholder="Type Binance or Robin Hood" value={this.state.search} autoFocus
                                           onChange={(e) => this.setState({
                                               showSuggestion: e.target.value !== '',
                                               search: e.target.value
                                           })}/>
                                    <button onClick={() => this.handleSubmit()} className="sr-overlay-search-btn">
                                        <i className="fa fa-chevron-right"/>
                                    </button>
                                </div>
                                {this.state.showSuggestion ? (
                                    <div style={{width: '100%', marginTop: 10, overflow: 'auto', maxHeight: 100}}>
                                        {platforms.map((e, i) => {
                                            let patt = new RegExp(' ' + this.state.search, 'gi');
                                            if (patt.test(' ' + e.platform)) {
                                                return (<div style={{
                                                    padding: 10,
                                                    cursor: 'pointer',
                                                    borderBottom: '1px solid #ccc'
                                                }} key={i} onClick={() => this.setState({
                                                    showSuggestion: false,
                                                    search: e.platform
                                                })}>{e.platform}</div>);
                                            }
                                        })}


                                    </div>) : ''}
                            </div> :
                            (

                                <div>
                                    {
                                        this.state.currentStep < this.state.result.length ?
                                            (
                                                <div>
                                                    <div style={{
                                                        borderBottomColor: '#333',
                                                        borderBottomWidth: 1,
                                                        paddingLeft: 10,
                                                        paddingRight: 10
                                                    }}>
                                                        <h3>{this.state.result[this.state.currentStep].title}</h3>
                                                    </div>
                                                    <div style={{marginBottom: 10, paddingLeft: 10, paddingRight: 10}}>
                                                        <p>{this.state.result[this.state.currentStep].desc}</p>
                                                    </div>
                                                    {
                                                        this.state.result[this.state.currentStep].img !== undefined ? (
                                                            <div style={{
                                                                marginBottom: 10,
                                                                paddingLeft: 10,
                                                                paddingRight: 10
                                                            }}>
                                                                <img src={this.state.result[this.state.currentStep].img}
                                                                     style={{width: 450}} alt="Steps"/>
                                                            </div>
                                                        ) : ''
                                                    }
                                                </div>

                                            ) :
                                            (
                                                <ApiSubmitSection
                                                    postSubmit={(data) => this.connectAPI(data)}
                                                    keyFieldName={key_field_name}
                                                    secretFieldName={secret_field_name}
                                                    submitting={this.state.submitting}
                                                    extraFields={extra_fields}
                                                    cancelAddingExchange={this.cancelAddingExchange}
                                                />
                                            )
                                    }


                                    <div style={{display: 'flex', justifyContent: 'space-between'}}>
                                        {this.state.currentStep > 0 ? '' :
                                            <button onClick={() => this.setState({showSteps: false})}
                                                    className="btn btn-default">Back</button>}
                                        {this.state.currentStep === 0 ? '' : <button
                                            onClick={() => this.setState({currentStep: this.state.currentStep - 1})}
                                            className="btn btn-default">Previous</button>}
                                        {this.state.currentStep < this.state.result.length ? <button
                                            onClick={() => this.setState({currentStep: this.state.currentStep + 1})}
                                            className="btn btn-primary">Next</button> : <div/>}
                                    </div>
                                </div>
                            )
                        }
                    </div>
                </div>
            </div>
        )
    }
}

export default Home;