import React from 'react';
import {Link} from 'react-router-dom';
import ReactDateTime from 'react-datetime';
import 'react-datetime/css/react-datetime.css';
import "./scss/sidebar.scss";
import {ajaxErrorHandling} from "../../../Helpers/ErrorHandling/AjaxErrorHandling";
import toast from "../../../Helpers/Toast/Toast";
import Loader1 from "../../Loader/Loader1";

export default class SideBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            orderDates: []
        };
        this.renderDay = this.renderDay.bind(this);
        this.getOrderDates = this.getOrderDates.bind(this);
        this.dateClicked = this.dateClicked.bind(this);
    }

    getOrderDates() {
        this.setState({
            loading: true
        });
        const url = '/api/v1/orders/calendar-dates-list';
        axios.get(url, {
            headers: {
                Authorization: "Bearer " + localStorage.getItem('token')
            }
        }).then(response => {
            this.setState({
                loading: false,
                orderDates: response.data.data
            }, () => {
                this.setState({
                    datesLoaded: true
                });
            });
        }).catch(error => {
            this.setState({
                loading: false
            });
            toast(ajaxErrorHandling(error));
        });
    }

    componentDidMount() {
        this.getOrderDates();
    }

    renderDay(props, currentDate, selectedDate) {
        const date = currentDate.format('YYYY-MM-DD');
        const ordered = this.state.orderDates.findIndex((e) => {
            return e.date === date;
        });
        return <td {...props}
                   title={ordered !== -1 ? 'Did some activity on this day' : ''}>
            <Link to={'/calendar/' + date} {...props}>
                <span {...props}
                      data-class={ordered !== -1 ? '1' : '0'}>
                    {currentDate.date()}
                </span>
            </Link>
        </td>;
    }

    dateClicked() {

    }

    render() {
        return (
            <div id="aside" className="app-aside modal nav-dropdown">
                <div className="left navside dark dk" data-layout="column">
                    <div className="navbar no-radius">
                        <a className="navbar-brand">
                            <div ui-include="'../assets/images/logo.svg'"/>
                            <img src="../assets/images/logo.png" alt="." className="hide"/>
                            <span className="hidden-folded inline">Remora Hub</span>
                        </a>
                    </div>
                    <div className="hide-scroll" data-flex>
                        <nav className="scroll nav-light">

                            <ul className="nav">
                                <li className="nav-header hidden-folded">
                                    <small className="text-muted">Main</small>
                                </li>

                                <li>
                                    <Link to="/open-new-position">
                                <span className="nav-icon">
                                <i className="material-icons">&#xe3fc;
                                    <span ui-include="'../assets/images/i_0.svg'"/>
                                </i>
                                </span>
                                        <span className="nav-text">Open Position</span>
                                    </Link>

                                    <Link to="/dashboard">
                                <span className="nav-icon">
                                <i className="material-icons">&#xe3fc;
                                    <span ui-include="'../assets/images/i_0.svg'"/>
                                </i>
                                </span>
                                        <span className="nav-text">Dashboard</span>
                                    </Link>
                                    <Link to="/exchanges">
                                <span className="nav-icon">
                                <i className="material-icons">&#xe3fc;
                                    <span ui-include="'../assets/images/i_0.svg'"/>
                                </i>
                                </span>
                                        <span className="nav-text">Exchanges</span>
                                    </Link>
                                    <Link to="/portfolio">
                                <span className="nav-icon">
                                <i className="material-icons">&#xe3fc;
                                    <span ui-include="'../assets/images/i_0.svg'"/>
                                </i>
                                </span>
                                        <span className="nav-text">Portfolio Analytics</span>
                                    </Link>
                                    <Link to="/syndicates">
                                <span className="nav-icon">
                                <i className="material-icons">&#xe3fc;
                                    <span ui-include="'../assets/images/i_0.svg'"/>
                                </i>
                                </span>
                                        <span className="nav-text">Syndicates</span>
                                    </Link>
                                    <Link to="/risk-management">
                                <span className="nav-icon">
                                <i className="material-icons">&#xe3fc;
                                    <span ui-include="'../assets/images/i_0.svg'"/>
                                </i>
                                </span>
                                        <span className="nav-text">Risk Management</span>
                                    </Link>
                                    <Link to="/positions-categories">
                                <span className="nav-icon">
                                <i className="material-icons">&#xe3fc;
                                    <span ui-include="'../assets/images/i_0.svg'"/>
                                </i>
                                </span>
                                        <span className="nav-text">Position Categories</span>
                                    </Link>

                                    <Link to="/orders">
                                <span className="nav-icon">
                                <i className="material-icons">&#xe3fc;
                                    <span ui-include="'../assets/images/i_0.svg'"/>
                                </i>
                                </span>
                                        <span className="nav-text">Orders</span>
                                    </Link>
                                </li>
                            </ul>
                        </nav>
                        {this.state.loading ? <Loader1/> : null}
                        {(() => {
                            if (this.state.datesLoaded) {
                                return <div id="navbar-calendar-cont">
                                    <ReactDateTime input={false}
                                                   closeOnSelect={false}
                                                   timeFormat={false}
                                                   renderDay={this.renderDay}
                                                   className="navbar-calendar"/>
                                </div>
                            } else {
                                return null;
                            }
                        })()}
                    </div>
                    <div className="b-t">
                        <div className="nav-fold">
                            <a href="profile.html">
                            <span className="pull-left">
                            <img src="../assets/images/a0.jpg" alt="..." className="w-40 img-circle"/>
                            </span>
                                <span className="clear hidden-folded p-x">
                            <span className="block _500">Jean Reyes</span>
                            <small className="block text-muted"><i className="fa fa-circle text-success m-r-sm"/>online</small>
                            </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}