import React from 'react';
import {Link} from 'react-router-dom';
import Modal from '../../Modal/Modal';
import OpenPositions from '../../../container/OpenPositions/OpenPositions';
import OpenPositionModal from "../../../container/OpenNewPosition/OpenPositionModal";
import RemoraHubModal from "../../Modal/RemoraHubModal";

class Header extends React.Component {

    constructor() {
        super();
        this.state = {
            drop: true,
            showOpenPositionModal: false
        };
        this.closeModal = this.closeModal.bind(this);
    }

    closeModal() {
        this.setState({showOpenPositionModal: false});
        console.log('ca');
    }

    handleLogout() {
        localStorage.removeItem('id');
        localStorage.removeItem('user_id');
        localStorage.removeItem('token');
        this.props.revalidate();

    }

    render() {
        return (
            <React.Fragment>
                {this.state.showOpenPositionModal ?
                    <OpenPositionModal orderPlaced={this.closeModal}
                                       modalClosed={this.closeModal}/> : ''}
                <div className="app-header white box-shadow">
                    <div className="navbar navbar-toggleable-sm flex-row align-items-center">
                        <a data-toggle="modal" data-target="#aside" className="hidden-lg-up mr-3">
                            <i className="material-icons">&#xe5d2;</i>
                        </a>
                        <div className="mb-0 h5 no-wrap" ng-bind="$state.current.data.title" id="pageTitle"/>
                        <div className="collapse navbar-collapse" id="collapse">
                            <ul className="nav navbar-nav mr-auto">
                                <li>
                                    <button className="btn btn-primary"
                                            onClick={() => this.setState({showOpenPositionModal: true})}>
                                        <i className="fa fa-plus"/> Open a new position
                                    </button>
                                </li>
                            </ul>

                            {/* <div ui-include="'../views/blocks/navbar.form.html'"></div> */}
                        </div>
                        <ul className="nav navbar-nav ml-auto flex-row">
                            <li className="nav-item dropdown pos-stc-xs">
                                <button onClick={() => this.setState({drop: true})} className="nav-link mr-2"
                                        data-toggle="dropdown">
                                <span className="avatar w-32">
                                  <img src="../assets/images/a0.jpg" alt="..."/>
                                  <i className="on b-white bottom"/>
                                </span>
                                </button>
                                {/* <div ui-include="'../views/blocks/dropdown.notification.html'"></div> */}
                                {this.state.drop ? <div className="dropdown-menu dropdown-menu-overlay pull-right">
                                    <a className="dropdown-item">
                                        <span>Inbox</span>
                                        <span className="label warn m-l-xs">3</span>
                                    </a>
                                    <a className="dropdown-item">
                                        <span>Profile</span>
                                    </a>
                                    <Link to="/settings" className="dropdown-item">
                                        <span>Settings</span>
                                        <span className="label primary m-l-xs">3/9</span>
                                    </Link>
                                    <div className="dropdown-divider"/>
                                    <a className="dropdown-item">
                                        Need help?
                                    </a>
                                    <a className="dropdown-item" onClick={() => this.handleLogout()}>Sign out</a>
                                </div> : ''}
                            </li>
                            {/* <li className="nav-item dropdown">
                  <button onClick={() => this.setState({ drop: true })} className="nav-link p-0 clear">
                    <span className="avatar w-32">
                      <img src="../assets/images/a0.jpg" alt="..." />
                      <i className="on b-white bottom"></i>
                    </span>
                  </button>
                </li> */}
                            <li className="nav-item hidden-md-up">
                                <a className="nav-link pl-2" data-toggle="collapse" data-target="#collapse">
                                    <i className="material-icons">&#xe5d4;</i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default Header;