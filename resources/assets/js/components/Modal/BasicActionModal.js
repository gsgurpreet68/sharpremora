import React from 'react';
import ReactModal from 'react-modal';
import './scss/basicActionModal.scss';

export default class BasicActionModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false
        };
        this.closeModal = this.closeModal.bind(this);
        this.actionBtnClicked = this.actionBtnClicked.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.isOpen !== this.props.isOpen) {
            this.setState({
                isOpen: nextProps.isOpen
            });
        }
    }

    componentDidMount() {
        this.setState({
            isOpen: this.props.isOpen
        });
    }

    closeModal() {
        this.setState({
            isOpen: false
        });
        this.props.modalClosed ? this.props.modalClosed() : null;
    }

    actionBtnClicked() {
        this.props.actionBtnClicked ? this.props.actionBtnClicked() : null;
    }

    render() {
        return (
            <ReactModal isOpen={this.state.isOpen}
                        style={{
                            overlay: {
                                backgroundColor: 'rgba(236, 239, 241, 0.8)',
                                zIndex: '9999'
                            },
                            content: {
                                border: '0',
                                borderRadius: '4px',
                                bottom: 'auto',
                                minHeight: '2rem',
                                maxHeight: '90%',
                                left: '50%',
                                padding: '2rem',
                                position: 'fixed',
                                right: 'auto',
                                top: '50%',
                                transform: 'translate(-50%,-50%)',
                                minWidth: this.props.minWidth ? this.props.minWidth : '20rem',
                                maxWidth: '60rem',
                                overflow: 'auto'
                            }
                        }}
                        contentLabel="Minimal Modal Example">
                <div className="header-modal">
                    <h3 className="modal-headline">
                        {this.props.header}
                        {!this.props.hideCross ? (
                            <span className="cross-mark pull-right"
                                  onClick={this.closeModal}>
                                <i className="fa fa-times"/>
                            </span>) : null}
                    </h3>
                </div>
                {this.props.children}

                <div className="footer-modal">
                    <div className="pull-right">
                        {(() => {
                            if (this.props.actionBtn) {
                                return <button className={"btn btn-sm " + (this.props.actionBtnClass)}
                                               onClick={this.actionBtnClicked}
                                               disabled={this.props.disabledActionBtn || this.props.actionBtnDisabled}>
                                    {this.props.actionBtnName}
                                </button>
                            } else {
                                return null;
                            }
                        })()}
                        <button className="btn btn-sm" onClick={this.closeModal}>
                            Cancel
                        </button>
                    </div>
                </div>
            </ReactModal>
        )
    }
}