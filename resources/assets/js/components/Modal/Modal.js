import React from 'react'

const modal = (props) => {
    // console.log(props);
    return (
        <div style={{position: 'fixed', height: '100vh', width: '100vw', zIndex: 2000, top: 0, left: 0}} >
            <div onClick={() => props.closeModal()} style={{width: '100vw', height: '100vh', position: 'fixed', backgroundColor:'#333', opacity: .8, zIndex: 2001 }} ></div>
            <div style={{position: 'relative', zIndex: 2002, display: 'block', width: 800, margin: '50px auto' }} > 
                <div style={{display: 'flex', padding: 10,borderRadius: 4, flexDirection:'column', justifyContent:'center', alignItems:'center', backgroundColor:'#fff'}}>
                {props.children}
                </div>
            </div>
        </div>
    )
}

export default modal