import React from 'react';
import './scss/remoraHubModal.scss';

export default class RemoraHubModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: true
        };
        this.modalClosed = this.modalClosed.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.showModal !== this.props.showModal) {
            this.setState({
                showModal: nextProps.showModal
            });
        }
    }

    componentDidMount() {
        this.setState({
            showModal: this.props.showModal
        });
    }

    modalClosed() {
        this.setState({
            showModal: false
        }, () => {
            this.props.modalClosed ? this.props.modalClosed() : null;
        });
    }

    render() {
        return (
            <div
                className={"remorahub-modal " + (this.state.showModal ? "show-remorahub-modal" : '')}>
                <div className={ `remorahub-modal-content ${this.props.customClass}` }>
                    <span className="close-button" onClick={this.modalClosed}>&times;</span>
                    {this.props.children}
                </div>
            </div>
        )
    }
}