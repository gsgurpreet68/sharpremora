 export const Auth = {
    validateSession : (callback) => {
        if(localStorage.getItem('token')){
            axios.get('/api/v1/user/me',{
                headers: {
                    Authorization: "Bearer " + localStorage.getItem('token')
                }
            })
            .then(response=>{
                if(response.data.success){
                    callback({status:true, data: response.data.data });
                }else{
                    callback({status:false});
                }
            })
            .catch(error=>{
                callback({status:false});
            });
        }else{
            callback({status:false});
        }
        
    }
    
 }