import React from 'react';

class RangeSlider extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            markerPosition:0,
            sliderpoints:[]
        }
        this.getSliderPoints = this.getSliderPoints.bind(this);
        this.setMarker = this.setMarker.bind(this);
    }

    componentDidMount() {
        this.setState({sliderpoints:this.getSliderPoints()});
        this.setMarker(2);
    }
    dragStart(e) {
        e.preventDefault();
    }

    drag(e) {
        // e.preventDefault();
        console.log(e);
    }

    setMarker(e)  {
        var denom = this.state.sliderpoints.length-1;
        if(typeof e == 'number'){
            // this.setState({markerPosition:(100/denom*e)+'%'});
        }else{
            var width = e.currentTarget.getClientRects()[0].width;
            
            var each_width = width / denom;
            var click_point = e.clientX - e.currentTarget.getClientRects()[0].x;
            
            var lower_point = Math.floor(click_point/each_width);
            if(click_point-(lower_point*each_width)>((lower_point+1)*each_width)-click_point){
                var mark_position = ((lower_point+1)*each_width);
                var mark_point = lower_point+1;
            }else{
                var mark_position = lower_point*each_width;
                var mark_point = lower_point;
            }
            this.setState({markerPosition:mark_position},()=>{
                if(this.props.markerMoved!=undefined){
                    this.props.markerMoved(mark_point);
                }
            });
        }
    }

    getSliderPoints() {
        var arr = [];
        for(var i = this.props.rangeStart;i<=this.props.rangeEnd; i=i+this.props.interval){
            arr.push(i)
        }
        return arr;
    }
    render(){
        return (
            <div style={{height:10,width:'100%',border:'1px solid #000',position:'relative'}} onClick={e=>this.setMarker(e)}>
                <div style={{position:'absolute',top:-20,width:'100%',left:-5}}>
                    {
                        this.state.sliderpoints.map((e,i)=>{
                            var style={width:(100/(this.state.sliderpoints.length-1))+'%',display:'inline-block',fontSize:'10px',padding:'0 0 0 -5px'};
                            if(false&&i>=this.state.sliderpoints.length-1){
                                style.borderRight = '1px solid #000';
                            }
                            if(i>=this.state.sliderpoints.length-1){
                                return <div style={{width:0,height:0}}></div>
                            }
                            return (
                                <div style={style} key={i}>{(e)+'x'}</div>
                            )
                        })
                    }
                    
                </div>
                <div style={{height:'100%',width:this.state.markerPosition,backgroundColor:'red'}}></div>
                <div style={{height:'170%',width:'10px',backgroundColor:'green',position:'absolute',top:'-30%',left:(this.state.markerPosition-5)}}></div>
            </div>
        )
    }
    
}
    

export default RangeSlider;