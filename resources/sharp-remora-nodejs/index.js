import express from 'express' ;
import cors from 'cors';
import socket from 'socket.io';
import http from 'http';

import  markets  from './market'

// Application port number
const port = 9999;
const app = express();

// initialise server
const server = http.createServer(app);

// initialise socket
const io = socket(server);

app.use(cors());

/*
* Please add all exchanges endpoints here 
* easy to get the endpoints in functions 
*/
export const endPoints = {
	bittrex:"https://bittrex.com/api/v1.1/public/",
  binance: "https://www.binance.com/api/",
  bitmax : "https://www.bitmex.com/api/v1",
  cryptopia: "https://www.cryptopia.co.nz/api",
  kuCoin: "https://api.kucoin.com/v1/open/",
  gdax: "https://api.gdax.com/",
  bitstamp: "https://www.bitstamp.net/api/v2/",
  bitfinex :"https://api.bitfinex.com/v1/",
  robinhood : "https://api.robinhood.com/"
};

/*
 * start socket conection with front-end 
 * call to different exchanges based on exchange param value
 * exchange name and market name are required
*/
io.on("connection", (socket) => {
  socket.on("market_details", data => {
    if(data.market){
      
      switch(data.exchange) {
      
      case 'bittrex':
      	markets.getBittrexMarket(socket,data.market);  
        setInterval(() => {
            markets.getBittrexMarket(socket,data.market);
        }, 8000);    
    	break;
      case 'binance':
        markets.getBinanceMarket(socket,data.market);  
        setInterval(() => {
            markets.getBinanceMarket(socket,data.market);
        }, 8000);    
      break;
      case 'bitmax':
        markets.getBitmaxMarket(socket,data.market);  
        setInterval(() => {
            markets.getBitmaxMarket(socket,data.market);
        }, 8000);    
      break;
      case 'cryptopia':
        markets.getCryptopiaMarket(socket,data.market);  
        setInterval(() => {
            markets.getCryptopiaMarket(socket,data.market);
        }, 8000);    
      break;
      case 'kucoin':
        markets.getKuCoinMarket(socket,data.market);  
        setInterval(() => {
            markets.getKuCoinMarket(socket,data.market);
        }, 8000);    
      break;  
      case 'gdax':
        markets.getGdaxMarket(socket,data.market);  
        setInterval(() => {
            markets.getGdaxMarket(socket,data.market);
        }, 8000); 
      break;  
      case 'bitstamp':
        markets.getBitstampMarket(socket,data.market);  
        setInterval(() => {
            markets.getBitstampMarket(socket,data.market);
        }, 8000);        
      break;  
      case 'bitfinex':
        markets.getBitfinexMarket(socket,data.market);  
        setInterval(() => {
            markets.getBitfinexMarket(socket,data.market);
        }, 8000);        
      break;  
      case 'robinhood':
        markets.getRobinhoodMarket(socket,data.market);  
        setInterval(() => {
            markets.getRobinhoodMarket(socket,data.market);
        }, 8000);   
      break;
        
      }


    }else{
      socket.emit("exchange_market_depth", {error:"Market name is required."});  
    }  
  });


  /*
  * When socket connection disconnect.
  */  
  socket.on("disconnect", () => {
    
  });

});


// listen server on specific port
server.listen(port, function(err) {
   if (err) {
      console.log("Error in starting server", err);
   }
   console.log(`Your App is Running on Port : ${port}`);
});



