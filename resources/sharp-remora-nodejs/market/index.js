import { endPoints } from '../index.js';
import axios from 'axios';


function formatOrders(orders) {
    let newBids = orders.bids.map((bid) =>{
        let newBid = {};
        newBid.price = bid[0];
        newBid.size  = bid[1];
        newBid.cumulative = bid[0] * bid[1];
        return newBid;
    });

    let newAsks = orders.asks.map((ask) =>{
        let newAsk = {};
        newAsk.price = ask[0];
        newAsk.size = ask[1];
        newAsk.cumulative = ask[0] * ask[1];
        return newAsk;
    });

    return {bids: newBids, asks: newAsks};
}

const markets = {
    
    /*
	* Get Bittrex market depth and summary
	* two arguments pass:  socket and market name
	*/
    getBittrexMarket : async (socket,market) => {

        
            let [orders, summary] = await Promise.all([
                 axios.get(`${endPoints.bittrex}getorderbook?market=${market}&type=both`)
                .then((orders) => { 
                    return orders = orders.data.result
                },
                (err) => {return {status:false,msg:err.message};}),
                 axios.get(`${endPoints.bittrex}getmarketsummary?market=${market}`)
                .then((summary) =>  {
                    return summary = summary.data.result
                },
                
                (err) => {return {status:false,msg:err.message};}),
            ]);
            

            
           
            socket.emit("exchange_market_depth", [orders,summary]); 
            
    },

    /*
    * Get Binance market depth and summary
    * two arguments pass:  socket and market name
    */
    getBinanceMarket : async (socket,market,cb) => {
        console.log('getting binance datas');
        try {
            const orders = await axios.get(`${endPoints.binance}v1/depth?symbol=${market}&limit=100`);
            const summary = await axios.get(`${endPoints.binance}v3/ticker/bookTicker?symbol=${market}`);
            var orders = orders.data;
            var summary = summary.data;
            let formattedOrders = formatOrders(orders);

             /* 
               *Emit the response inclusing orders and summary data
               *Emitting a new message. It will be consumed by the client
             */
            // console.log(orders.bids);
            socket.emit("exchange_market_depth", formattedOrders); 
        } catch (error) {
            console.log("error on server",error);
            socket.emit("exchange_market_depth", error);  
        }
    },



    /*
    * Get Bitmax market depth and summary
    * two arguments pass:  socket and market name
    * we can't get sell buy order in different object because
    * Bitmax return both sell and buy order list in single object
    */
    getBitmaxMarket : async (socket,market) => {

        
            let [orders, summary] = await Promise.all([
                 axios.get(`${endPoints.bitmax}/orderBook/L2?symbol=${market}&depth=50`)
                .then((orders) => { 
                   return orders = orders.data
                },
                (err) => {return {status:false,msg:err.message};}),
                 axios.get(`${endPoints.bitmax}/orderBook/L2?symbol=${market}&depth=50`)
                .then((summary) =>  {
                    return summary = summary.data
                },
                
                (err) => {return {status:false,msg:err.message};}),
            ]);
            

            
           
            socket.emit("exchange_market_depth", [orders,summary]); 
            
    },


     /*
    * Get Cryptopia market depth and summary
    * two arguments pass:  socket and market name
    * pass the pair or single market (BTC or BTC_DGB)
    */
    getCryptopiaMarket : async (socket,market) => {
        
            let [orders, summary] = await Promise.all([
                 axios.get(`${endPoints.cryptopia}/GetMarketOrders/${market}`)
                .then((orders) => { 
                   return  orders = orders.data.Data
                },
                (err) => {return {status:false,msg:err.message};}),
                 axios.get(`${endPoints.cryptopia}/GetMarket/${market}`)
                .then((summary) =>  {
                   return  summary = summary.data.Data
                },
                
                (err) => {return {status:false,msg:err.message};}),
            ]);
            

            
           
            socket.emit("exchange_market_depth", [orders,summary]); 
            
    },



     /*
    * Get Kucoin market depth and summary
    * two arguments pass:  socket and market name
    */
    getKuCoinMarket : async (socket,market) => {

         
            let [orders, summary] = await Promise.all([
                 axios.get(`${endPoints.kuCoin}orders?symbol=${market}&limit=50`)
                .then((orders) => { 
                   return  orders = orders.data.data
                },
                (err) => {return {status:false,msg:err.message};}),
                 axios.get(`${endPoints.kuCoin}/tick?symbol=${market}`)
                .then((summary) =>  {
                   return  summary = summary.data.data
                },
                
                 (err) => {return {status:false,msg:err.message};}),
            ]);
            
            
            socket.emit("exchange_market_depth", [orders,summary]); 
            
    },


    /*
    * Get Gdax market depth and summary
    * two arguments pass:  socket and market name
    */
    getGdaxMarket : async (socket,market) => {



            let [orders, summary] = await Promise.all([
                 axios.get(`${endPoints.gdax}products/${market}/trades`)
                .then((orders) => { 
                    return orders = orders.data;
                    
                },
                (err) => {return {status:false,msg:err.message};}),
                 axios.get(`${endPoints.gdax}/products/${market}/ticker`)
                .then((summary) =>  {
                    return summary = summary.data;                    
                },
                
                (err) => {return {status:false,msg:err.message};}),
            ]);
            

            
            socket.emit("exchange_market_depth", [orders,summary]); 
            
    },


    
     /*
    * Get Bitstamp market depth and summary
    * two arguments pass:  socket and market name
    */
    getBitstampMarket : async (socket,market) => {


        
            let [orders, summary] = await Promise.all([
                 axios.get(`${endPoints.bitstamp}order_book/${market}`)
                .then((orders) => { 
                   return  orders = orders.data
                },
                (err) => {return {status:false,msg:err.message};}),
                 axios.get(`${endPoints.bitstamp}ticker/${market}`)
                .then((summary) =>  {
                   return  summary = summary.data
                },
                
                (err) => {return {status:false,msg:err.message};}),
            ]);
            
           
            socket.emit("exchange_market_depth", [orders,summary]); 
            
    },


     /*
    * Get Bitfinex market depth and summary
    * two arguments pass:  socket and market name
    */
    getBitfinexMarket : async (socket,market) => {

            
            let [orders, summary] = await Promise.all([
                 axios.get(`${endPoints.bitfinex}book/${market}`)
                .then((orders) => { 
                   return  orders = orders.data
                },
                (err) => {return {status:false,msg:err.message};}),
                 axios.get(`${endPoints.bitfinex}pubticker/${market}`)
                .then((summary) =>  {
                    return summary = summary.data
                },
                
                (err) => {return {status:false,msg:err.message};}),
            ]);
            

            socket.emit("exchange_market_depth", [orders,summary]); 
            
    },


     /*
    * Get Robinhood market depth and summary
    * two arguments pass:  socket and market name
    */
    getRobinhoodMarket : async (socket,market) => {

            
            let [orders, summary] = await Promise.all([
                 axios.get(`${endPoints.robinhood}quotes/${market}/`)
                .then((orders) => { 
                    return orders = orders.data
                },
                (err) => {return {status:false,msg:err.message};}),
                 axios.get(`${endPoints.robinhood}quotes/${market}/`)
                .then((summary) =>  {
                    return summary = summary.data
                },
                
                 (err) => {return {status:false,msg:err.message};}),
            ]);
            

            socket.emit("exchange_market_depth", [orders,summary]); 
            
       
    }
    
}

export default  markets ;
